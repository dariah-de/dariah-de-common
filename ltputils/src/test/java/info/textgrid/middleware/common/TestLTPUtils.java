/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.common;

import static org.junit.Assert.assertTrue;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import jakarta.activation.MimeTypeParseException;
import javax.imageio.ImageIO;
import org.apache.tika.exception.TikaException;
import org.apache.tika.mime.MimeTypeException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2020-03-04 - Funk - Remove special header.
 * 
 * 2018-07-09 - Funk - Added EXIF metadata extraction test from AdaptorManager.
 * 
 * 2015-09-28 - Funk - First version.
 */

/**
 * <p>
 * JUnit class for testing the LTPUtil implementation.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-03-16
 * @since 2015-09-28
 */

public class TestLTPUtils {

  // **
  // STATIC FINALS
  // **

  private static final String XML_DATA = "<xml><urgl aua=\"aua\">argl</urgl></xml>";
  private static final String TEXT_DATA = "Urgl! Argl! Aua!";
  private static final String TEXT_PLAIN = "text/plain";
  private static final String APPLICATION_PDF = "application/pdf";
  private static final String PDF_FILENAME = "src/test/resources/Kartoffelsuppe.pdf";
  private static final String TEXT_XML = "text/xml";
  private static final String DOT_XML = ".xml";
  private static final String DOT_TEXT = ".txt";
  private static final int BIG_IMAGE_WIDTH = 29000;
  private static final int BIG_IMAGE_HEIGHT = 27000;


  // **
  // STATICS
  // **

  private static File xmlFile;
  private static File textFile;
  private static File pdfFile;

  // **
  // SET UPS
  // **

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {

    // Create XML file.
    xmlFile = new File("xmlfile" + DOT_XML);
    FileWriter fw = new FileWriter(xmlFile);
    fw.write(XML_DATA);
    fw.close();
    xmlFile.deleteOnExit();

    // Create plain text file.
    textFile = new File("textfile" + DOT_TEXT);
    fw = new FileWriter(textFile);
    fw.write(TEXT_DATA);
    fw.close();
    textFile.deleteOnExit();

    // Read PDF file.
    pdfFile = new File(PDF_FILENAME);
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  // **
  // TESTS
  // **

  /**
   * @throws IOException
   * @throws TikaException
   */
  @Test
  public void testExtractMimetype4xml() throws IOException, TikaException {
    String m = LTPUtils.extractMimetype(xmlFile);
    if (!m.equals(TEXT_XML)) {
      System.out.println("File is not expected XML mimetype " + TEXT_XML + ": " + m);
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws TikaException
   */
  @Test
  public void testExtractMimetype4pdf() throws IOException, TikaException {
    String m = LTPUtils.extractMimetype(pdfFile);
    if (!m.equals(APPLICATION_PDF)) {
      System.out.println("File is not expected XML mimetype " + APPLICATION_PDF + ": " + m);
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws TikaException
   */
  @Test
  public void testExtractFileExtension4xml() throws TikaException, IOException {
    String m = LTPUtils.extractFileExtension(xmlFile);
    if (!m.equals(DOT_XML)) {
      System.out.println("File has not expected XML extension +" + DOT_XML + ": " + m);
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws TikaException
   */
  @Test
  public void testExtractMimetype4txt() throws IOException, TikaException {
    String m = LTPUtils.extractMimetype(textFile);
    if (!m.equals(TEXT_PLAIN)) {
      System.out.println("File is not expected text mimetype " + TEXT_PLAIN + ": " + m);
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws TikaException
   */
  @Test
  public void testExtractFileExtension4txt() throws TikaException, IOException {
    String m = LTPUtils.extractFileExtension(textFile);
    if (!m.equals(DOT_TEXT)) {
      System.out.println("File has not expected text extension +" + DOT_TEXT + ": " + m);
      assertTrue(false);
    }
  }

  /**
   * @throws MimeTypeException
   */
  @Test
  public void testExtensionExtraction() throws MimeTypeException {
    String mimetype = "application/zip";
    String extension = LTPUtils.getFileExtension(mimetype);
    if (!extension.equals(".zip")) {
      assertTrue("No file extension for: " + mimetype, false);
    }
  }

  /**
   * 
   */
  @Test
  public void testOmitHdlPrefix() {

    String meth = "testOmitHdlPrefix()";
    String expected = "urgli";

    // With existing prefix.
    String handle = RDFConstants.HDL_PREFIX + ":" + expected;
    String res = LTPUtils.omitHdlPrefix(handle);
    if (!res.equals("urgli")) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR omitting the HDL prefix!", false);
    }
    // Without prefix.
    handle = "urgli";
    res = LTPUtils.omitHdlPrefix(handle);
    if (!LTPUtils.omitHdlPrefix(handle).equals("urgli")) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR omitting the HDL prefix!", false);
    }
  }

  /**
   * 
   */
  @Test
  public void testOmitHdlNamespace() {

    String meth = "testOmitHdlNamespace()";
    String expected = "urgli";

    // With already existing namespace.
    String handle = RDFConstants.HDL_NAMESPACE + expected;
    String res = LTPUtils.omitHdlNamespace(handle);
    if (!res.equals(expected)) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR omitting the HDL namespace!", false);
    }

    // Without namespace.
    handle = "urgli";
    res = LTPUtils.omitHdlNamespace(handle);
    if (!LTPUtils.omitHdlNamespace(handle).equals(expected)) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR omitting the HDL namespace!", false);
    }
  }

  /**
   * 
   */
  @Test
  public void testOmitDoiPrefix() {

    String meth = "testOmitDoiPrefix()";
    String expected = "urgli";

    // With existing prefix.
    String doi = RDFConstants.DOI_PREFIX + ":" + expected;
    String res = LTPUtils.omitDoiPrefix(doi);
    if (!res.equals(expected)) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR omitting the DOI prefix!", false);
    }

    // Without prefix.
    doi = "urgli";
    res = LTPUtils.omitDoiPrefix(doi);
    if (!res.equals(expected)) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR omitting the DOI prefix!", false);
    }
  }

  /**
   * 
   */
  @Test
  public void testOmitDoiNamespace() {

    String meth = "testOmitDoiNamespace()";
    String expected = "urgli";

    // With already existing namespace.
    String doi = RDFConstants.DOI_NAMESPACE + expected;
    String res = LTPUtils.omitDoiNamespace(doi);
    if (!res.equals(expected)) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR omitting the DOI namespace!", false);
    }

    // Without namespace.
    doi = "urgli";
    res = LTPUtils.omitDoiNamespace(doi);
    if (!LTPUtils.omitDoiNamespace(doi).equals(expected)) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR omitting the DOI namespace!", false);
    }
  }

  /**
   * 
   */
  @Test
  public void testResolveDoiPidString() {

    String meth = "testResolveDoiPidString()";
    String expected = RDFConstants.DOI_NAMESPACE + "urgli";

    // With already existing namespace.
    String doi = expected;
    String res = LTPUtils.resolveDoiPid(doi);
    if (!res.equals(expected)) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR resolving the DOI namespace!", false);
    }

    // Without namespace.
    doi = "urgli";
    res = LTPUtils.resolveDoiPid(doi);
    if (!res.equals(expected)) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR resolving the DOI namespace!", false);
    }
  }

  /**
   * 
   */
  @Test
  public void testResolveDoiPidURI() {

    String meth = "testResolveDoiPidURI()";
    String expected = RDFConstants.DOI_NAMESPACE + "urgli";

    // With already existing namespace.
    URI doi = URI.create(expected);
    String res = LTPUtils.resolveDoiPid(doi);
    if (!res.equals(expected)) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR resolving the DOI namespace!", false);
    }

    // Without namespace.
    doi = URI.create("urgli");
    res = LTPUtils.resolveDoiPid(doi);
    if (!res.equals(expected)) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR resolving the DOI namespace!", false);
    }
  }

  /**
   * 
   */
  @Test
  public void testResolveHandlePidString() {

    String meth = "testResolveHandlePidString()";
    String expected = RDFConstants.HDL_NAMESPACE + "urgliargli";

    // With already existing namespace.
    URI doi = URI.create(expected);
    String res = LTPUtils.resolveHandlePid(doi);
    if (!res.equals(expected)) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR resolving the HDL namespace!", false);
    }

    // Without namespace.
    doi = URI.create("urgliargli");
    res = LTPUtils.resolveHandlePid(doi);
    if (!res.equals(expected)) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR resolving the HDL namespace!", false);
    }
  }

  /**
   * 
   */
  @Test
  public void testResolveHandlePidURI() {

    String meth = "testResolveHandlePidURI()";
    String expected = RDFConstants.HDL_NAMESPACE + "urgliargli";

    // With already existing namespace.
    String hdl = expected;
    String res = LTPUtils.resolveHandlePid(hdl);
    if (!res.equals(expected)) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR resolving the HDL namespace!", false);
    }

    // Without namespace.
    hdl = "urgliargli";
    res = LTPUtils.resolveHandlePid(hdl);
    if (!res.equals(expected)) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR resolving the HDL namespace!", false);
    }
  }

  /**
   * 
   */
  @Test
  public void testOmitDariahNamespace() {

    String meth = "testOmitDariahNamespace()";
    String expected = "urgl/argl/aua";

    // With trailing "/" (a namespace always must end with a "/"!).
    String namespace = RDFConstants.DARIAH_COLLECTION + "/";
    String uri = namespace + expected;
    String res = LTPUtils.omitNamespace(uri, namespace);
    if (!res.equals(expected)) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR omitting URI namespace!", false);
    }

    // Without trailing "/".
    namespace = RDFConstants.DARIAH_COLLECTION;
    uri = namespace + "/" + expected;
    res = LTPUtils.omitNamespace(uri, namespace);
    if (!res.equals(expected)) {
      System.out.println(meth + ": " + res + " != " + expected);
      assertTrue("ERROR omitting URI namespace!", false);
    }
  }

  /**
   * <p>
   * Test EXIF extraction.
   * </p>
   * 
   * @throws IOException
   * @throws MimeTypeParseException
   * @throws URISyntaxException
   */
  @Test
  public void testExtractExifData() throws IOException, MimeTypeParseException, URISyntaxException {

    String[] imageNames =
        {"C04_004.tif.18wvk.0.tiff", "C04_003.tif.18wn0.0.tiff", "C04_002.tif.18wtd.0.tiff",
            "C04_001.tif.18wnh.0.tiff", "wanne.png", "T121-00096-DEF-45v.jpg",
            "T121-00096-MAX-45v.jpg", "T121-00096-MIN-45v.jpg", "T121-00096-THUMB-45v.jpg"};
    for (String name : imageNames) {
      System.out.println("Testing extract exif metadata from " + name);

      String format = "";
      if (name.endsWith(".tiff")) {
        format = TextGridMimetypes.TIFF;
      } else if (name.endsWith(".jpg")) {
        format = TextGridMimetypes.JPEG;
      } else if (name.endsWith(".png")) {
        format = TextGridMimetypes.PNG;
      } else {
        assertTrue("Only TIFF, PNG and JPEG configured here! :-P", false);
      }

      InputStream inputImage = new FileInputStream(new File("src/test/resources/" + name));

      try {
        for (String rel : LTPUtils.extractExifImageData(inputImage, new URI("textgrid:" + name),
            format)) {
          System.out.println(rel);

          if (!(rel.contains("#width") || rel.contains("#height"))) {
            assertTrue("missind correct width or height!", false);
          }
        }
      } finally {
        inputImage.close();
      }
    }
  }

  /**
   * <p>
   * Test EXIF extraction.
   * </p>
   * 
   * @throws IOException
   * @throws MimeTypeParseException
   * @throws URISyntaxException
   */
  @Test(expected = MimeTypeParseException.class)
  public void testExtractExifDataFAIL()
      throws IOException, MimeTypeParseException, URISyntaxException {

    String name = "wanne.png";
    System.out.println("Testing extract exif metadata from " + name);

    String format = "image/peng";

    InputStream inputImage = new FileInputStream(new File("src/test/resources/" + name));

    try {
      for (String rel : LTPUtils.extractExifImageData(inputImage, new URI("textgrid:" + name),
          format)) {
        System.out.println(rel);

        if (!(rel.contains("#width") && rel.contains(String.valueOf(BIG_IMAGE_WIDTH))
            || rel.contains("#height") && rel.contains(String.valueOf(BIG_IMAGE_HEIGHT)))) {
          assertTrue("missind correct width or height!", false);
        }
      }
    } finally {
      inputImage.close();
    }
  }


  /**
   * <p>
   * Test EXIF extraction with a LARGE TIFF.
   * </p>
   * 
   * NOTE Please ignore in CI builds...
   * 
   * @throws IOException
   * @throws MimeTypeParseException
   * @throws URISyntaxException
   */
  @Test
  @Ignore
  public void testExtractExifDataLargeTIFF()
      throws IOException, MimeTypeParseException, URISyntaxException {

    File bigtiff = createHugeTiff();

    System.out.println("Testing extract exif metadata from " + bigtiff.getCanonicalPath());

    long start = System.currentTimeMillis();

    InputStream inputImage = new BufferedInputStream(new FileInputStream(bigtiff));

    try {
      for (String rel : LTPUtils.extractExifImageData(inputImage,
          new URI("textgrid:" + bigtiff.getName()), "image/tiff")) {
        System.out.println(rel);

        if (!(rel.contains("#width") && rel.contains(String.valueOf(BIG_IMAGE_WIDTH))
            || rel.contains("#height") && rel.contains(String.valueOf(BIG_IMAGE_HEIGHT)))) {
          assertTrue("missind correct width or height!", false);
        }
      }
    } finally {
      inputImage.close();
    }

    System.out.println("Extraction time: " + (System.currentTimeMillis() - start) + " millis");
  }

  // **
  // PRIVASTE METHODS
  // **

  /**
   * @param theWidth
   * @param theHeight
   * @return
   * @throws IOException
   */
  private static File createHugeTiff() throws IOException {

    System.out
        .print("Creating big TIFF image (" + BIG_IMAGE_WIDTH + "*" + BIG_IMAGE_HEIGHT + ")...");

    File result = File.createTempFile("BIG-", "-BIG.tiff");
    // result.deleteOnExit();

    BufferedImage image =
        new BufferedImage(BIG_IMAGE_WIDTH, BIG_IMAGE_HEIGHT, BufferedImage.TYPE_INT_RGB);

    Graphics2D graphics = image.createGraphics();

    graphics.setBackground(Color.GREEN);
    graphics.clearRect(0, 0, BIG_IMAGE_WIDTH, BIG_IMAGE_HEIGHT);
    graphics.dispose();

    boolean written = ImageIO.write(image, "TIFF", result);

    if (!written) {
      System.out.println(" error");
      assertTrue("Huge test TIFF could NOT be written!", false);
    }

    System.out.println(" done");

    return result;
  }

}
