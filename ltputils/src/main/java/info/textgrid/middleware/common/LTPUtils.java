/**
 * This software is copyright (c) 2024 by
 *
 * TextGrid Consortium (https://textgrid.de)
 *
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.common;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import org.apache.cxf.helpers.IOUtils;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.detect.Detector;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.TikaMetadataKeys;
import org.apache.tika.mime.MediaType;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;
import jakarta.activation.MimeTypeParseException;
import jakarta.ws.rs.ProcessingException;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * TODOLOG
 *
 **
 * CHANGELOG
 *
 * 2021-07-01 - Funk - Add javadoc comment on x and y resolution EXIF metadata extraction.
 *
 * 2020-09-23 - Funk - Using Tika file and stream for media type extraction, method also changed.
 *
 * 2020-04-06 - Funk - Add simple and fast and not-memory-using method for getting image width and
 * height.
 *
 * 2018-03-02 - Funk - Using jaxrs client now for FITS service call.
 *
 * 2018-01-11 - Funk - Added TECHMD suffix.
 *
 * 2015-09-23 - Funk - Added extractMimetype method.
 *
 * 2015-08-12 - Funk - First version.
 */

/**
 * <p>
 * Long term preservation utilities.
 * </p>
 *
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-07-01
 * @since 2015-08-12
 */

public class LTPUtils {

  // **
  // STATIC FINALS
  // **

  private static final String TEXT_XML = "text/xml";
  private static final String FITS_PATH = "examine?file=";

  public static final String INT_NAMESPACE_URI = "http://www.w3.org/2001/XMLSchema#integer";
  public static final String EXIF_NAMESPACE_URI = "http://www.w3.org/2003/12/exif/ns#";
  public static final String TECHMD_SUFFIX = ".tech";

  // **
  // CLASS
  // **

  private static Client fitsClient = null;

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Gets a mimetype string for a given file. Gives "text/xml" for "application/xml".
   * </p>
   *
   * @param theFile
   * @return The mimetype string of the given file.
   * @throws TikaException
   * @throws IOException
   */
  public static String extractMimetype(final File theFile) throws TikaException, IOException {

    String result;

    MediaType m = getMediaType(theFile);
    result = m.getBaseType().toString();

    // Check for XML application mimetype.
    if (m.equals(MediaType.APPLICATION_XML)) {
      result = TEXT_XML;
    }

    return result;
  }

  /**
   * <p>
   * Gives the file extension for a given file, if extractable by Tika.
   * </p>
   *
   * @param theFile
   * @return The file extension string of the given file.
   * @throws TikaException
   * @throws IOException
   */
  public static String extractFileExtension(final File theFile) throws TikaException, IOException {

    String result;

    TikaConfig config = TikaConfig.getDefaultConfig();
    MediaType m = getMediaType(theFile);
    MimeType mt = config.getMimeRepository().forName(m.toString());

    result = mt.getExtension();

    return result;
  }

  /**
   * <p>
   * Extracts FITS technical metadata for a given File. Needs a configured FITS installation.
   * </p>
   *
   * @param theFitsLocation
   * @param theTimeout
   * @param theData
   * @return
   * @throws IOException
   */
  public static String extractTechMD(final String theFitsLocation, final long theTimeout,
      final File theData) throws IOException {

    String result = "";

    // Create FITS service client and set timeout.
    if (fitsClient == null) {
      ClientBuilder.newBuilder().readTimeout(theTimeout, TimeUnit.MILLISECONDS);
      fitsClient = ClientBuilder.newClient().property("http.receive.timeout", theTimeout)
          .property("thread.safe.client", "true");
    }

    String request =
        theFitsLocation + FITS_PATH + URLEncoder.encode(theData.getCanonicalPath(), "UTF-8");

    try {
      Response response = fitsClient.target(request).request().get();

      int statusCode = response.getStatus();
      String reasonPhrase = response.getStatusInfo().getReasonPhrase();

      if (statusCode != Status.OK.getStatusCode()) {
        throw new IOException("Failed to get TECH_MD from THE FITS [" + request + "]: " + statusCode
            + " " + reasonPhrase);
      }

      result = IOUtils.readStringFromStream(response.readEntity(InputStream.class));

    } catch (ProcessingException e) {
      throw new IOException("FITS service timeout!", e);
    }

    // Make a short check if we get a proper answer from THE FITS.
    if (!result.startsWith("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
        && !result.contains("<identification>")) {
      throw new IOException("The FITS is not working properly!");
    }

    return result;
  }

  /**
   * <p>
   * Removes the "hdl:" from the given PID, if existing.
   * </p>
   *
   * @param theHDL
   * @return
   */
  public static String omitHdlPrefix(final String theHDL) {
    return omitPrefix(theHDL, RDFConstants.HDL_PREFIX);
  }

  /**
   * <p>
   * Removes the HDL namespace from the given PID, if existing.
   * </p>
   *
   * @param theHDL
   * @return
   */
  public static String omitHdlNamespace(final String theHDL) {
    return omitNamespace(theHDL, RDFConstants.HDL_NAMESPACE);
  }

  /**
   * <p>
   * Removes the "doi:" from the given DOI, if existing.
   * </p>
   *
   * @param theDOI
   * @return
   */
  public static String omitDoiPrefix(final String theDOI) {
    return omitPrefix(theDOI, RDFConstants.DOI_PREFIX);
  }

  /**
   * <p>
   * Removes the DOI namespace from the given DOI, if existing.
   * </p>
   *
   * @param theDOI
   * @return
   */
  public static String omitDoiNamespace(final String theDOI) {
    return omitNamespace(theDOI, RDFConstants.DOI_NAMESPACE);
  }

  /**
   * <p>
   * Gets a file extension string (without the "."!) for a given mimetype string. Returns "ttl" for
   * DARIAH-DE collection mimetype.
   * </p>
   *
   * @param theMimetype
   * @return
   */
  public static String getFileExtension(final String theMimetype) {

    String result = "";

    if (theMimetype.equals(TextGridMimetypes.DARIAH_COLLECTION)) {
      result = "ttl";
    } else {

      try {
        MimeType mimeType = MimeTypes.getDefaultMimeTypes().forName(theMimetype);
        if (mimeType != null) {
          result = mimeType.getExtension();
        }
      } catch (MimeTypeException e) {
        // Do nothing here, just return empty string.
      }
    }

    return result;
  }

  /**
   * <p>
   * Adds the HDL namespace "http://hdl.handle.net/" to the given HDL, prefix "hdl:" is removed
   * first, if existing.
   * </p>
   *
   * @param theHDL
   * @return
   */
  public static String resolveHandlePid(final URI theHDL) {
    return resolveURI(theHDL.toString(), RDFConstants.HDL_PREFIX, RDFConstants.HDL_NAMESPACE);
  }

  /**
   * <p>
   * Adds the HDL namespace "http://hdl.handle.net/" to the given HDL, prefix "hdl:" is removed
   * first, if existing.
   * </p>
   *
   * @param theHDL
   * @return
   */
  public static String resolveHandlePid(final String theHDL) {
    return resolveURI(theHDL.toString(), RDFConstants.HDL_PREFIX, RDFConstants.HDL_NAMESPACE);
  }

  /**
   * <p>
   * Adds the DOI namespace "http://dx.doi.org/" to the given DOI, prefix "doi:" is removed first,
   * if existing.
   * </p>
   *
   * @param theDOI
   * @return
   */
  public static String resolveDoiPid(final URI theDOI) {
    return resolveURI(theDOI.toString(), RDFConstants.DOI_PREFIX, RDFConstants.DOI_NAMESPACE);
  }

  /**
   * <p>
   * Adds the DOI namespace "http://dx.doi.org/" to the given DOI, prefix "doi:" is removed first,
   * if existing.
   * </p>
   *
   * @param theDOI
   * @return
   */
  public static String resolveDoiPid(final String theDOI) {
    return resolveURI(theDOI.toString(), RDFConstants.DOI_PREFIX, RDFConstants.DOI_NAMESPACE);
  }

  /**
   * <p>
   * Removes a namespace from the beginning of a string.
   * </p>
   *
   * @param theURI
   * @param theNamespace
   * @return
   */
  public static String omitNamespace(final String theURI, final String theNamespace) {

    String result = theURI;

    if (theURI != null && theURI.startsWith(theNamespace)) {
      result = theURI.replaceFirst(theNamespace, "");
    }

    // Remove eventually existing "/" from namespaces not ending with a "/".
    if (theURI != null && result.startsWith("/")) {
      result = result.substring(1);
    }

    return result;
  }

  /**
   * <p>
   * Removes a prefix, including the ":" from the beginning of a string.
   * </p>
   *
   * @param theURI
   * @param thePrefix
   * @return
   */
  public static String omitPrefix(final String theURI, final String thePrefix) {

    String result = theURI;

    if (theURI != null && theURI.startsWith(thePrefix + ":")) {
      result = theURI.replaceFirst(thePrefix + ":", "");
    }

    return result;
  }

  /**
   * <p>
   * Resolves URIs by omitting prefixes and adding the namespace.
   * </p>
   *
   * @param theURI
   * @param thePrefix
   * @param theNamespace
   * @return
   */
  public static String resolveURI(final String theURI, final String thePrefix,
      final String theNamespace) {

    String result = theURI;

    if (theURI != null && !theURI.startsWith(theNamespace)) {
      result = theNamespace + omitPrefix(theURI, thePrefix);
    }

    return result;
  }

  /**
   * <p>
   * Extract EXIF metadata from image stream, such as width and height, they can be extracted from
   * ImageReader. Resolution can not, so we do not extract it anymore :-( If needed in TG metadata,
   * please write to <a href="mailto:support@de.dariah.eu">support@de.dariah.eu</a> and hope for the
   * best! :-)
   * </p>
   * 
   * @param theImageStream
   * @param theUri
   * @param theMimetype
   * @return
   * @throws IOException
   * @throws MimeTypeParseException
   */
  public static List<String> extractExifImageData(InputStream theImageStream, URI theUri,
      String theMimetype) throws IOException, MimeTypeParseException {

    ArrayList<String> result = new ArrayList<String>();

    // Correct mimetype if TIF or JPG!
    String mimetype = theMimetype;
    if (mimetype.equalsIgnoreCase(TextGridMimetypes.JPG)) {
      mimetype = TextGridMimetypes.JPEG;
    } else if (mimetype.equalsIgnoreCase(TextGridMimetypes.TIF)) {
      mimetype = TextGridMimetypes.TIFF;
    }

    Iterator<ImageReader> iter = ImageIO.getImageReadersByMIMEType(mimetype);
    if (iter.hasNext()) {
      ImageReader reader = iter.next();
      try {
        ImageInputStream iis = ImageIO.createImageInputStream(theImageStream);
        reader.setInput(iis);
        result.add(exifTriple(theUri, "width", reader.getWidth(reader.getMinIndex())));
        result.add(exifTriple(theUri, "height", reader.getHeight(reader.getMinIndex())));
      } finally {
        reader.dispose();
      }
    } else {
      throw new MimeTypeParseException("No ImageReader for mimetype " + mimetype + " found!");
    }

    return result;
  }

  // **
  // INTERNAL
  // **

  /**
   * <p>
   * Get MediaType for file.
   * </p>
   *
   * @param theFile
   * @return The media type of the given file.
   * @throws TikaException
   * @throws IOException
   */
  private static MediaType getMediaType(final File theFile) throws TikaException, IOException {

    MediaType result = null;

    TikaConfig config = TikaConfig.getDefaultConfig();
    Detector detector = config.getDetector();
    TikaInputStream stream = TikaInputStream.get(Paths.get(theFile.getAbsolutePath()));

    Metadata metadata = new Metadata();
    metadata.add(TikaMetadataKeys.RESOURCE_NAME_KEY, theFile.getAbsolutePath());
    result = detector.detect(stream, metadata);

    stream.close();

    return result;
  }

  /**
   * @param uri
   * @param exifProperty
   * @param value
   * @return
   */
  private static String exifTriple(URI uri, String exifProperty, int value) {
    return "<" + uri.toString() + "> <" + EXIF_NAMESPACE_URI + exifProperty
        + "> \"" + value + "\"^^<" + INT_NAMESPACE_URI + "> .";
  }

}
