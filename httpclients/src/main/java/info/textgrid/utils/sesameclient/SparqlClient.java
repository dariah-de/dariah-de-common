package info.textgrid.utils.sesameclient;

import java.io.IOException;
import java.io.InputStream;

public interface SparqlClient {

	public static final String MIME_SPARQL_RESULT = "application/sparql-results+xml";

	public abstract InputStream sparql(String queryString) throws IOException;

}