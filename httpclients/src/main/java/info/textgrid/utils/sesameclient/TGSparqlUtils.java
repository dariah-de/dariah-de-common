/*
 * #%L
 * TextGrid :: HTTPClients
 * %%
 * Copyright (C) 2011 - 2012 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen (http://www.sub.uni-goettingen.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package info.textgrid.utils.sesameclient;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TGSparqlUtils {

	private static Log	log	= LogFactory.getLog(TGSparqlUtils.class);

	/**
	 * Get all uris which are aggregated by the input uri, this is done
	 * recursive, so all uris in subaggregations(collections, etc) are returned
	 * 
	 * @param client
	 *            SesamClient to query
	 * @param uri
	 *            aggregation uri
	 * @return
	 * @throws IOException
	 */
	public static List<String> getAggregatedUrisRecursive(SparqlClient client,
			String uri) throws IOException {

		String query = "PREFIX ore:<http://www.openarchives.org/ore/terms/>\n"
				+ "PREFIX tg:<http://textgrid.info/relation-ns#>\n"
				+ "SELECT (max(?o) as ?uri)\n" + "WHERE {\n" + "<" + uri
				+ "> (ore:aggregates/tg:isBaseUriOf|ore:aggregates)* ?o .\n"
				+ "FILTER not exists { ?o tg:isBaseUriOf ?x } .\n"
				+ "FILTER not exists { ?o tg:isDeleted true } .\n"
				+ "OPTIONAL {?base tg:isBaseUriOf ?o}\n" + "} group by ?base";

		InputStream res = client.sparql(query);
		List<String> uris = urisFromSparqlResponse(res);
		return uris;
	}

	/**
	 * Get number of latest revision for a baseUri
	 * 
	 * @param client
	 *            SesamClient to query
	 * @param baseUri
	 *            URI latest revision should be found for
	 * 
	 * @return number of latest revision
	 * @throws IOException
	 *             if no matching URI was found
	 */
	public static int getLatestRevision(SparqlClient client, String baseUri)
			throws IOException {

		String latestUri = getLatestUri(client, baseUri);
		return revisionFromUri(latestUri);
	}

	/**
	 * Get URI of latest revision for a baseUri
	 * 
	 * @param client
	 *            SesamClient to query
	 * @param baseUri
	 *            URI latest revision should be found for
	 * 
	 * @return URI of latest revision
	 * 
	 * @throws IOException
	 *             if no matching URI was found
	 */
	public static String getLatestUri(SparqlClient client, String baseUri)
			throws IOException {

		if (baseUri.contains(".")) {
			String[] arr = baseUri.split("\\.");
			log.info("uri no baseUri:" + baseUri + " - trying with " + arr[0]);
			baseUri = arr[0];
		}

		StringBuffer sb = new StringBuffer();

		sb.append("PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
				+ "PREFIX ore:<http://www.openarchives.org/ore/terms/>\n"
				+ "PREFIX tg:<http://textgrid.info/relation-ns#>\n"
				+ "PREFIX tei:<http://www.tei-c.org/ns/1.0>\n"
				+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>" + "\n"
				+ "SELECT ?uri\n" + "WHERE {\n");
		sb.append("  <")
		.append(baseUri)
		.append("> tg:isBaseUriOf ?uri .\n"
				+ "  ?base tg:isBaseUriOf ?uri .\n"
				// results should be sorted revision, so revision needs
				// to be be cut from uri and converted to int
				+ "  bind(xsd:integer(strafter(xsd:string(?uri), \".\")) as ?rev) .\n"
				+ "	 FILTER NOT EXISTS { ?uri tg:isDeleted true } .\n"
				+ "} order by desc(?rev) limit 1");

		InputStream res = client.sparql(sb.toString());
		List<String> uris = urisFromSparqlResponse(res);

		if (uris.size() == 0) {
			throw new IOException("No uri found for " + baseUri);
		}

		return uris.get(0);

	}

	/**
	 * Get number of latest revision for a baseUri, deleted are not filtered, so
	 * the last used revision is returned
	 * 
	 * @param client
	 *            SesamClient to query
	 * @param baseUri
	 *            URI latest revision should be found for
	 * 
	 * @return number of latest revision
	 * 
	 * @throws IOException
	 *             if no matching URI was found
	 */
	public static int getLatestUnfilteredRevision(SparqlClient client,
			String baseUri) throws IOException {

		String latestUri = getLatestUnfilteredUri(client, baseUri);
		return revisionFromUri(latestUri);

	}

	/**
	 * Get URI of latest revision for a baseUri, deleted are not filteres, so
	 * the last ever used revision is returned
	 * 
	 * @param client
	 *            SesamClient to query
	 * @param baseUri
	 *            URI latest revision should be found for
	 * 
	 * @return URI of latest revision
	 * 
	 * @throws IOException
	 *             if no matching URI was found
	 */
	public static String getLatestUnfilteredUri(SparqlClient client,
			String baseUri) throws IOException {

		if (baseUri.contains(".")) {
			String[] arr = baseUri.split("\\.");
			log.info("uri no baseUri:" + baseUri + " - trying with " + arr[0]);
			baseUri = arr[0];
		}

		StringBuffer sb = new StringBuffer();

		sb.append("PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
				+ "PREFIX ore:<http://www.openarchives.org/ore/terms/>\n"
				+ "PREFIX tg:<http://textgrid.info/relation-ns#>\n"
				+ "PREFIX tei:<http://www.tei-c.org/ns/1.0>\n"
				+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>" + "\n"
				+ "SELECT ?uri\n" + "WHERE {\n");
		sb.append("  <")
		.append(baseUri)
		.append("> tg:isBaseUriOf ?uri .\n"
				+ "  ?base tg:isBaseUriOf ?uri .\n"
				+ "  bind(xsd:integer(strafter(xsd:string(?uri), \".\")) as ?rev) .\n"
				+ "} order by desc(?rev) limit 1");
		System.out.println(sb.toString());
		InputStream res = client.sparql(sb.toString());
		List<String> uris = urisFromSparqlResponse(res);

		if (uris.size() == 0) {
			throw new IOException("No uri found for " + baseUri);
		}

		return uris.get(0);

	}

	/**
	 * Check if succUri is a (direct or indirect) successor of predUri in the
	 * same project. Also returns true for identity.
	 * 
	 * This method is useful to prevent aggregations from being included by
	 * itself, because no predecessor should be moved into one of its successors
	 * or itself.
	 * 
	 * @param client
	 *            SesamClient to query
	 * @param predUri
	 *            URI of predecessor
	 * @param succUri
	 *            URI of successor
	 * @return
	 * @throws Exception
	 */
	public static boolean isSuccessor(SparqlClient client, String predUri,
			String succUri) throws Exception {

		String query = "PREFIX ore:<http://www.openarchives.org/ore/terms/>\n"
				+ "PREFIX tg:<http://textgrid.info/relation-ns#>\n" + "ASK {\n"
				+ "  <" + predUri
				+ "> (ore:aggregates/tg:isBaseUriOf|ore:aggregates)* <"
				+ succUri + ">. \n" + "  <" + predUri
				+ "> tg:inProject ?p1 .\n" + "  <" + succUri
				+ "> tg:inProject ?p2 .\n" + "  FILTER (?p1 = ?p2) \n" + "} ";

		InputStream res = client.sparql(query);
		return parseBoolean(res);

	}

	/**
	 * returns list of uris found in query, working for 1 bound variable, as every uri-xml-elements content is
	 * returned in list. not namespace aware.
	 *
	 * sample for incoming xml:
	 * <pre>{@code
	 *    <sparql xmlns="http://www.w3.org/2005/sparql-results#">
	 *    <head>
	 *        <variable name="pre" />
	 *    </head>
	 *    <results>
	 *        <result><binding name="pre"><uri>uri1</uri></binding></result>
	 *    </results>
	 *    </sparql>
	 * }</pre>
	 * 
	 * @param in
	 *            inputstream from sparql-query
	 * @return list of uris found
	 */
	public static List<String> urisFromSparqlResponse(InputStream in) {

		List<String> urilist = new ArrayList<String>();

		try {
			XMLStreamReader r = XMLInputFactory.newInstance()
					.createXMLStreamReader(in);
			while (r.hasNext()) {

				if (r.next() == XMLStreamConstants.START_ELEMENT
						&& r.getLocalName().equals("uri")) {
					String uri = r.getElementText();
					// log.debug("uri: " + uri);
					urilist.add(uri);
				}
			}
		} catch (XMLStreamException e) {
			log.error("error parsing sparql-result-stream", e);
		}

		return urilist;
	}

	/**
	 * split revsision from uri TODO: move to textgrid-utils
	 * 
	 * @param uri
	 * @return
	 * @throws FileNotFoundException
	 *             If no revisions are existing for the given URI
	 */
	public static int revisionFromUri(String uri) throws IOException {
		int revision = 0;
		String[] arr = uri.split("\\.");
		if (arr.length > 1) {
			revision = Integer.parseInt(arr[1]);
		} else {
			throw new IOException("No revisions found for URI: " + uri);
		}

		return revision;
	}

	/**
	 * parse a boolean sparql result from xml stream and return the found
	 * boolean
	 * 
	 * compare http://www.w3.org/TR/rdf-sparql-XMLres/#boolean-results
	 * 
	 * @param in
	 *            inputstream
	 * @return boolean
	 * @throws Exception
	 */
	public static boolean parseBoolean(InputStream in) throws Exception {

		boolean res = false;
		boolean found = false;

		XMLStreamReader r = XMLInputFactory.newInstance()
				.createXMLStreamReader(in);
		while (r.hasNext()) {
			if (r.next() == XMLStreamConstants.START_ELEMENT
					&& r.getLocalName().equals("boolean")) {
				res = Boolean.valueOf(r.getElementText());
				found = true;
			}
		}

		if (!found) {
			throw new Exception("no parseable boolean sparql result found");
		}

		return res;

	}

}
