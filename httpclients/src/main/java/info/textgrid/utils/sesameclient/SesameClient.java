/*
 * #%L
 * TextGrid :: HTTPClients
 * %%
 * Copyright (C) 2011 - 2012 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen (http://www.sub.uni-goettingen.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package info.textgrid.utils.sesameclient;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.xml.bind.DatatypeConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class SesameClient implements SparqlClient{


	public static final String MIME_N3 = "text/rdf+n3";
	public static final String MIME_RDFXML = "application/rdf+xml";
	public static final String MIME_TURTLE = "application/x-turtle";
	public static final String MIME_NTRIPLES = "text/plain";
	public static final String MIME_SPARQL_RESULT = "application/sparql-results+xml";
	public static final String MIME_TRANSACTION = "application/x-rdftransaction";
	private static final String MIME_SPARQL_JSON = "application/sparql-results+json";
	private static final String MIME_SPARQL_BINARY = "application/x-binary-rdf-results-table";


	private Client client;
	private String target;
	private String basePath;
	private String username;
	private String password;

	private String charset = "UTF-8"; // TODO: deprecate
	private Log log = LogFactory.getLog(SesameClient.class);
	private boolean READONLY = false;	// use only get for querying if set


	/**
	 * Constructor for querying public instances without user and password
	 * 
	 * @param endpoint the sesame repository, e.G. http://textgridlab.org/openrdf-sesame/repositories/textgrid
	 * @throws MalformedURLException 
	 */
	public SesameClient(String endpoint) throws MalformedURLException {

		URL url = new URL(endpoint);
		int port = url.getPort() == -1 ? url.getDefaultPort() : url.getPort();
		this.target = url.getProtocol() + "://" + url.getHost() + ":" + port;
		this.basePath = url.getPath();

		this.client = ClientBuilder
				.newClient()
				.property("thread.safe.client", "true");

		this.READONLY = true;

	}

	/**
	 * Constructor
	 * 
	 * @param endpoint the sesame repository, e.G. http://textgridlab.org/openrdf-sesame/repositories/textgrid
	 * @param username
	 * @param password
	 * @throws MalformedURLException 
	 */
	public SesameClient(String endpoint, String username, String password) throws MalformedURLException {

		this(endpoint);
		this.username = username;
		this.password = password;

	}

	public int uploadN3(InputStream in) throws IOException {

		return uploadRDF(in, MIME_N3);

	}

	public int uploadRDFXML(InputStream in) throws IOException {

		return uploadRDF(in, MIME_RDFXML);

	}

	public int uploadTurtle(InputStream in) throws IOException {

		return uploadRDF(in, MIME_TURTLE);

	}

	public int uploadNTriples(InputStream in) throws IOException {

		return uploadRDF(in, MIME_NTRIPLES);

	}

	public int uploadRDF(InputStream data, String mime) throws IOException {

		MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
		headers.add("Authorization", getBasicAuthentication());

		Response response = client
				.target(this.target)
				.path(this.basePath + "/statements")
				.request()
				.headers(headers)
				.post(Entity.entity(data, mime));

		log.debug("status: " + response.getStatus());
		log.debug("reason: " + response.getStatusInfo().getReasonPhrase());

		return response.getStatus();
	}

	public int transaction(InputStream data) throws IOException {

		MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
		headers.add("Authorization", getBasicAuthentication());

		Response response = client
				.target(this.target)
				.path(this.basePath + "/statements")
				.request()
				.headers(headers)
				.post(Entity.entity(data, MIME_TRANSACTION));

		log.debug("Reason: " + response.getStatusInfo().getReasonPhrase());

		return response.getStatus();

	}

	public InputStream sparql(String queryString) throws IOException {
		log.debug(queryString);
		return sendQuery(queryString, MIME_SPARQL_RESULT);
	}

	public InputStream sparqlJson(String queryString) throws IOException {
		log.debug(queryString);
		return sendQuery(queryString, MIME_SPARQL_JSON);
	}

	public InputStream sparqlBinary(String queryString) throws IOException {
		log.debug(queryString);
		return sendQuery(queryString, MIME_SPARQL_BINARY);
	}	

	public InputStream constructTurtle(String queryString) throws IOException {
		log.debug(queryString);
		return sendQuery(queryString, MIME_TURTLE);
	}

	public InputStream constructN3(String queryString) throws IOException {
		log.debug(queryString);
		return sendQuery(queryString, MIME_N3);
	}	

	public InputStream constructRDFXML(String queryString) throws IOException {
		log.debug(queryString);
		return sendQuery(queryString, MIME_RDFXML);
	}

	public InputStream constructNTriples(String queryString) throws IOException {
		log.debug(queryString);
		return sendQuery(queryString, MIME_NTRIPLES);
	}

	/**
	 * Delete from sesame repo matching the S P O pattern.
	 * 
	 * Be carefull, calling this methos with three empty Strings will
	 * delete the whole repo
	 * 
	 * @param s   subject, e.g. "textgrid:1234.0"
	 * @param p   predicate, e.g. "http://www.openarchives.org/ore/terms/aggregates"
	 * @param o   object, e.g. "textgrid:1235.0"
	 * 
	 * @return    sesame http response code
	 * @throws IOException 
	 */
	public int delete(String s, String p, String o) throws IOException {

		StringBuffer transaction = new StringBuffer();
		transaction.append("<?xml version='1.0' encoding='UTF-8'?>")
		.append("<transaction>")
		.append("<remove>")
		.append(transactionUri(s))
		.append(transactionUri(p))
		.append(transactionUri(o))
		.append("<contexts/></remove></transaction>");

		ByteArrayInputStream bin = new ByteArrayInputStream(transaction.toString().getBytes());

		return transaction(bin);

	}

	private String transactionUri(String uri) {
		StringBuffer transaction = new StringBuffer();
		if(uri.equals("")) {
			transaction.append("<null/>");
		} else {
			transaction.append("<uri>").append(uri).append("</uri>");
		}
		return transaction.toString();
	}


	private InputStream sendQuery(String query, String contentType) throws IOException {

		Response response;

		if(READONLY) {
			response = getQuery(query, contentType);
		} else {
			response = postQuery(query, contentType);
		}

		return response.readEntity(InputStream.class);
	}

	private Response postQuery(String query, String contentType) throws IOException {

		MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
		headers.add("Accept", contentType);
		headers.add("Authorization", getBasicAuthentication());

		MultivaluedMap<String, String> queryparams = new MultivaluedHashMap<String, String>();
		queryparams.add("queryLn", "sparql");
		queryparams.add("query", "query");

		Response response = client
				.target(this.target)
				.path(this.basePath + "/statements")
				.request()
				.headers(headers)
				.post(Entity.form(queryparams));

		return response;
	}

	private Response getQuery(String query, String contentType) throws IOException {

		MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
		headers.add("Accept", contentType);

		// resteasy-client breaks if finding { or }
		query = query.replace("{","%7B").replace("}","%7D");

		Response response = client
				.target(this.target)
				.path(this.basePath)
				.queryParam("query", query)
				.request()
				.headers(headers)
				.get();

		return response;

	}

	private String getBasicAuthentication() {
		String token = this.username + ":" + this.password;
		try {
			return "BASIC " + DatatypeConverter.printBase64Binary(token.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException ex) {
			throw new IllegalStateException("Cannot encode with UTF-8", ex);
		}
	}


	@Deprecated
	public String getCharset() {
		return charset;
	}

	@Deprecated
	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getEndpoint() {
		return this.target + this.basePath;
	}

}
