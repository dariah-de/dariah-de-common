/*
 * #%L
 * TextGrid :: HTTPClients
 * %%
 * Copyright (C) 2011 - 2012 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen (http://www.sub.uni-goettingen.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package info.textgrid.utils.sesameclient;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

public class TestTGSparqlUtils {

	final static String PUBLIC_RDF_REPO = "http://textgridlab.org/1.0/triplestore/textgrid-public";

	//@Ignore
	@Test
	public void testGetLatestUri() throws IOException {
		SesameClient s1 = new SesameClient(PUBLIC_RDF_REPO);

		String uri = TGSparqlUtils.getLatestUri(s1, "textgrid:jfsm");
		System.out.println("uri: " + uri);

		int rev = TGSparqlUtils.getLatestRevision(s1, "textgrid:jfsm");
		System.out.println("rev: " + rev);	

		String uriu = TGSparqlUtils.getLatestUnfilteredUri(s1, "textgrid:jfsm");
		System.out.println("uriu: " + uriu);

		int revu = TGSparqlUtils.getLatestUnfilteredRevision(s1, "textgrid:jfsm");
		System.out.println("revu: " + revu);

	}

	/**
	 * not sure how to test this, as new revisions may be published
	 * @throws IOException
	 */
	@Test
	public void testGetLatestRevision() throws IOException {
		SesameClient s1 = new SesameClient(PUBLIC_RDF_REPO);
		int rev = TGSparqlUtils.getLatestRevision(s1, "textgrid:jfsm");
		assertTrue(rev >=0);
	}

	@Test
	public void testGetAggregatedUrisRecursive() throws IOException {
		SesameClient s1 = new SesameClient(PUBLIC_RDF_REPO);
		List<String> resl = TGSparqlUtils.getAggregatedUrisRecursive(s1, "textgrid:jfsm.0");

		assertTrue(resl.contains("textgrid:jfsm.0"));
		assertTrue(resl.contains("textgrid:jfsn.0"));
		assertTrue(resl.contains("textgrid:jfsr.0"));

		assertEquals(resl.size(), 40);

		/*for(String tmp : resl) {
			System.out.println(tmp);
		}*/
	}

	// too slow right now, reactivate with newer sesame?
	/*	@Test
	public void testIsSuccessor() throws Exception {
		String topAgg = "textgrid:jfsm.0";
		String sub1Agg = "textgrid:jfsn.0";
		String sub2Agg = "textgrid:jfsr.0";

    	SesameClient s1 = new SesameClient(PUBLIC_RDF_REPO);

    	// include aggregation in itself
    	boolean b1 = TGSparqlUtils.isSuccessor(s1, topAgg, topAgg);
    	assertTrue(b1);

    	// include toplevel aggregation in sub aggregation
    	boolean b2 = TGSparqlUtils.isSuccessor(s1, topAgg, sub1Agg);
    	assertTrue(b2);

    	// include sub aggregation in toplevel aggregation
    	boolean b3 = TGSparqlUtils.isSuccessor(s1, sub1Agg, topAgg);
    	assertFalse(b3);

    	// include toplevel aggregation in sub aggregation
    	//boolean b4 = TGSparqlUtils.isSuccessor(s1, topAgg, sub2Agg);
    	// TODO: reactivate check when sesame is ready
    	//assertTrue(b4);

    	// include sub aggregation in toplevel aggregation
// FIXME? Why this test doesnÄt work????? PLEASE CHECK UBBO!!!!
//    	boolean b5 = TGSparqlUtils.isSuccessor(s1, sub2Agg, topAgg);
//    	assertFalse(b5);
	}*/


}
