/*
 * #%L
 * TextGrid :: HTTPClients
 * %%
 * Copyright (C) 2011 - 2012 TextGrid Consortium (http://www.textgrid.de), SUB Göttingen (http://www.sub.uni-goettingen.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package info.textgrid.utils.sesameclient;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.Ignore;


public class SesameClientTest {

	@Test
	public void testGet() throws IOException {
		SesameClient s1 = new SesameClient("https://textgridlab.org/1.0/triplestore/textgrid-public", "a", "b");

		InputStream res2 = s1.sparql("SELECT ?s ?o WHERE {?s <http://textgrid.info/relation-ns#definesSchema> ?o}");

		String st = new BufferedReader(
				new InputStreamReader(res2, StandardCharsets.UTF_8))
				.lines()
				.collect(Collectors.joining("\n"));

		System.out.println(st);

		//System.out.println(IOUtils.readStringFromStream(res2));
		assertNotNull(st);
		assertFalse(st.equals(""));
	}

}
