/**
 * This software is copyright (c) 2021 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.middleware.common;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;

/**
 * TODOLOG
 *
 * TODO Check with Pairtree implementation of CDL in Merritt StorageService!
 * 
 **
 * CHANGELOG
 * 
 * 2014-10-30 - Funk - Moved to tgcrud-common package.
 * 
 * 2013-10-29 - Funk - Corrected some Sonar issues.
 *
 * 2011-11-05 - Funk - Added method to return Pairpath strings as URI fragment.
 * 
 * 2011-08-16 - Funk - Fixed bug, missing \ replacement: case '\\': result += "^5c"; Removed lading
 * ff's from the Pairtree ID and path, if hex values > 0x7e.
 * 
 * 2011-07-27 - Funk - Updated Pairtree specification link.
 * 
 * 2010-09-03 - Funk - Put into maven module. Separated test code from main code.
 * 
 * 2008-12-09 - Funk - PPath now created from the ENCODED ID!
 * 
 * 2008-10-22 - Funk - Renamed to JPairtree.
 * 
 * 2008-10-21 - Funk - Folder creation added, ID encoding fixed, ID decoding implemented.
 * 
 * 2008-10-17 - Funk - First version.
 */

/**
 * <p>
 * Java implementation of John Kunze's Internet Draft: Pairtrees for Object Storage, V0.1
 * (2008-12-08).
 * </p>
 * 
 * <p>
 * Please see <a href="https://confluence.ucop.edu/display/Curation/PairTree">https
 * ://confluence.ucop.edu/display/Curation/PairTree</a>.
 * </p>
 * 
 * <p>
 * NOTE This implementation may not be thread-safe with every File object, e.g.
 * org.gridlab.gat.io.File is not working properly with version of JavaGAT 2.0.5 and the Globus
 * Toolkit GridFTP adaptors!
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2021-03-17
 * @since 2008-10-17
 */

public class JPairtree {

  // **
  // STATIC FINALS
  // **

  public static final String VERSION = "JPairtree v0.4";
  public static final String PAIRTREE_ROOT = "pairtree_root";
  private static final int BEGIN_RECODE_UTF8 = 0x21;
  private static final int END_RECODE_UTF8 = 0x7e;
  private static final int ALSO_RECODE_UTF8 = 0xff;

  // **
  // CLASS VARIABLES
  // **

  private String ptreeOriginalId;
  private String ptreeEncodedId;

  // **
  // CONSTRUCTORS
  // **

  /**
   * <p>
   * Constructor: Creates a JPairtree from an given identifier string.
   * </p>
   * 
   * @param theId
   * @throws UnsupportedEncodingException
   */
  public JPairtree(String theId) throws UnsupportedEncodingException {
    this.ptreeOriginalId = theId;
    this.ptreeEncodedId = encodeId();
  }

  /**
   * <p>
   * Constructor: Creates a JPairtree from an given URI and a root folder.
   * </p>
   * 
   * @param theId
   * @throws UnsupportedEncodingException
   */
  public JPairtree(URI theId) throws UnsupportedEncodingException {
    this.ptreeOriginalId = theId.toASCIIString();
    this.ptreeEncodedId = encodeId();
  }

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Creates the Pairtree folders.
   * </p>
   * 
   * @param theRootFolder
   * @return The JPairtree PPath folder as a file.
   */
  public File createPPathFolders(File theRootFolder) {

    File result = null;
    String farray[] = this.getPtreePathString().split("/");

    if (farray.length != 0) {
      File lf = theRootFolder;
      for (int i = 0; i < farray.length; i++) {
        File f = new File(lf, farray[i]);
        // Only create folder, if not yet existing!
        if (!f.exists()) {
          f.mkdir();
        }
        lf = f;
      }

      result = lf;
    }

    return result;
  }

  // **
  // INTERNAL
  // **

  /**
   * <p>
   * Cleans the given ID string from special characters.
   * </p>
   * 
   * @return The encoded ID as a string.
   * @throws UnsupportedEncodingException
   */
  private String encodeId() throws UnsupportedEncodingException {

    String result = "";

    byte utf8Array[] = this.ptreeOriginalId.getBytes("UTF-8");

    for (int i = 0; i < utf8Array.length; i++) {
      // Recode visible ASCII characters: " < ? * = ^ + > | \ ,
      switch (utf8Array[i]) {
        case '"':
          result += "^22";
          break;
        case '<':
          result += "^3c";
          break;
        case '?':
          result += "^3f";
          break;
        case '*':
          result += "^2a";
          break;
        case '=':
          result += "^3d";
          break;
        case '^':
          result += "^5e";
          break;
        case '+':
          result += "^2b";
          break;
        case '>':
          result += "^3e";
          break;
        case '|':
          result += "^7c";
          break;
        case '\\':
          result += "^5c";
          break;
        case ',':
          result += "^2c";
          break;
        default:
          // Recode all characters outside of the 21-7e hex range.
          if (utf8Array[i] < BEGIN_RECODE_UTF8 || utf8Array[i] > END_RECODE_UTF8) {
            // TODO Does this ALWAYS work?
            result += "^" + Integer.toHexString(utf8Array[i] & ALSO_RECODE_UTF8);
          } else {
            result += (char) utf8Array[i];
          }
      }
    }

    // At last replace the following: '/' -> '=', ':' -> '+' and '.' -> ','.
    result = result.replaceAll("/", "=");
    result = result.replaceAll(":", "+");
    result = result.replaceAll("\\.", ",");

    this.ptreeEncodedId = result;

    return this.ptreeEncodedId;
  }

  /**
   * <p>
   * Decodes the Pairtree path folders into the ID again.
   * </p>
   * 
   * @return The decoded ID.
   */
  @SuppressWarnings("unused")
  private static String decodeId() {
    return "NOT YET IMPLEMENTED!!";
  }

  /**
   * @return The pairtree string.
   */
  private String getPairpathStringFromId() {
    return getPairpathStringFromId(File.separatorChar);
  }

  /**
   * @param theSeparatorChar
   * @return The pairtree string.
   */
  private String getPairpathStringFromId(char theSeparatorChar) {

    StringBuffer result = new StringBuffer(PAIRTREE_ROOT + theSeparatorChar);
    int i = 0;

    for (i = 0; i < this.ptreeEncodedId.length() - 1; i += 2) {
      result.append(this.ptreeEncodedId.substring(i, i + 2));
      result.append(theSeparatorChar);
    }

    if (i < this.ptreeEncodedId.length()) {
      result.append(this.ptreeEncodedId.substring(i, i + 1));
      result.append(theSeparatorChar);
    }

    return result.toString();
  }

  /**
   * TODO
   * 
   * @return The ID from a pairtree string.
   */
  @SuppressWarnings("unused")
  private static String getIdFromPairpathString() {
    return "NOT YET IMPLEMENTED!!";
  }

  // **
  // GETTERS and SETTERS
  // **

  /**
   * @return The pairtree string.
   */
  public String getPtreePathString() {
    return getPairpathStringFromId();
  }

  /**
   * @return The pairtree string as an URI fragment.
   */
  public String getPtreePathStringAsUriFragment() {
    return getPairpathStringFromId('/');
  }

  /**
   * @return The pairtree original ID.
   */
  public String getPtreeOriginalId() {
    return this.ptreeOriginalId;
  }

  /**
   * @return The pairtree encoded ID.
   */
  public String getPtreeEncodedId() {
    return this.ptreeEncodedId;
  }

}
