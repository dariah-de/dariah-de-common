/**
 * This software is copyright (c) 2021 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.middleware.common;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import info.textgrid.middleware.common.JPairtree;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2014-10-30 - Funk - Moved to tgcrud-common package.
 * 
 * 2011-11-03 - Funk - Replaced all "/"s from pathes to File.SeparatorChar.
 * 
 * 2011-08-16 - Funk - Added more test cases from various locations.
 * 
 * 2010-09-03 - Funk - First version.
 */

/**
 * <p>
 * JUnit class for testing the JPairtree implementation.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2014-10-30
 * @since 2010-09-03
 */

public class TestJPairtree {

  // **
  // STATIC FINALS
  // **

  static final String C = String.valueOf(File.separatorChar);
  static final String ROOT = "pairtree_root";

  // **
  // SET UPS
  // **

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  // **
  // TESTS
  // **

  /**
   * <p>
   * Simple URI.
   * </p>
   * 
   * @throws UnsupportedEncodingException
   */
  @Test
  public void createSimpleJPairtree() throws UnsupportedEncodingException {

    System.out.println("###  Testing createSimpleJPairtree()  ###");

    String uri = "textgrid:urgl:argl:aua:123456789";
    String expectedId = "textgrid+urgl+argl+aua+123456789";
    String expectedPpath = ROOT + File.separatorChar + "te" + C + "xt" + C + "gr" + C + "id" + C
        + "+u" + C + "rg" + C + "l+" + C + "ar" + C + "gl" + C + "+a" + C + "ua" + C + "+1" + C
        + "23" + C + "45" + C + "67" + C + "89" + C;

    if (!testIdCreation(uri, expectedId, expectedPpath)) {
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Old TextGrid URI.
   * </p>
   * 
   * @throws UnsupportedEncodingException
   */
  @Test
  public void createOldTextGridJPairtree() throws UnsupportedEncodingException {

    System.out.println("###  Testing createOldTextGridJPairtree()  ###");

    String uri =
        "textgrid:Campe+Ba...nd+1%2C+die+zweite:TEI-LITE+to+BASELINE+Adaptor:20080905T120728:xml%2Fxsl:1";
    String expectedId =
        "textgrid+Campe^2bBa,,,nd^2b1%2C^2bdie^2bzweite+TEI-LITE^2bto^2bBASELINE^2bAdaptor+20080905T120728+xml%2Fxsl+1";
    String expectedPpath =
        ROOT + C + "te" + C + "xt" + C + "gr" + C + "id" + C + "+C" + C + "am" + C + "pe" + C + "^2"
            + C + "bB" + C + "a," + C + ",," + C + "nd" + C + "^2" + C + "b1" + C + "%2" + C + "C^"
            + C + "2b" + C + "di" + C + "e^" + C + "2b" + C + "zw" + C + "ei" + C + "te" + C + "+T"
            + C + "EI" + C + "-L" + C + "IT" + C + "E^" + C + "2b" + C + "to" + C + "^2" + C + "bB"
            + C + "AS" + C + "EL" + C + "IN" + C + "E^" + C + "2b" + C + "Ad" + C + "ap" + C + "to"
            + C + "r+" + C + "20" + C + "08" + C + "09" + C + "05" + C + "T1" + C + "20" + C + "72"
            + C + "8+" + C + "xm" + C + "l%" + C + "2F" + C + "xs" + C + "l+" + C + "1" + C;
    if (!testIdCreation(uri, expectedId, expectedPpath)) {
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Simple URI.
   * </p>
   * 
   * @throws UnsupportedEncodingException
   */
  @Test
  public void createSpecialCharPairtree() throws UnsupportedEncodingException {

    System.out.println("###  Testing createSpecialCharPairtree()  ###");

    String uri = "textgrid:special:chars:<:?:*:=:^:+:>:|:,:ready";
    String expectedId = "textgrid+special+chars+^3c+^3f+^2a+^3d+^5e+^2b+^3e+^7c+^2c+ready";
    String expectedPpath = ROOT + C + "te" + C + "xt" + C + "gr" + C + "id" + C + "+s" + C + "pe"
        + C + "ci" + C + "al" + C + "+c" + C + "ha" + C + "rs" + C + "+^" + C + "3c" + C + "+^" + C
        + "3f" + C + "+^" + C + "2a" + C + "+^" + C + "3d" + C + "+^" + C + "5e" + C + "+^" + C
        + "2b" + C + "+^" + C + "3e" + C + "+^" + C + "7c" + C + "+^" + C + "2c" + C + "+r" + C
        + "ea" + C + "dy" + C;

    if (!testIdCreation(uri, expectedId, expectedPpath)) {
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Test PairtreePathes from JavaScript implementation.
   * 
   * @throws UnsupportedEncodingException
   * @see https://github.com/gsf/pairtree.js/blob/master/test/test-path.js
   */
  @Test
  public void testPairtreePathesFromJSImplementation() throws UnsupportedEncodingException {

    System.out.println("###  Testing testPairtreePathesFromJSImplementation()  ###");

    HashMap<String, String> testCases = new HashMap<String, String>();

    testCases.put("abc", C + "ab" + C + "c" + C);
    testCases.put("abcd", C + "ab" + C + "cd" + C);
    testCases.put("abcdefg", C + "ab" + C + "cd" + C + "ef" + C + "g" + C);
    // Don't understand this one:
    // equal(pairtree.path('abcde', '\\'), '\\ab\\cd\\e\\', '5-char with \\ separator');
    testCases.put("xy", C + "xy" + C);
    testCases.put("z", C + "z" + C);
    testCases.put("", C);
    testCases.put("abcdefg", C + "ab" + C + "cd" + C + "ef" + C + "g" + C);
    testCases.put("12-986xy4", C + "12" + C + "-9" + C + "86" + C + "xy" + C + "4" + C);
    testCases.put("13030_45xqv_793842495", C + "13" + C + "03" + C + "0_" + C + "45" + C + "xq" + C
        + "v_" + C + "79" + C + "38" + C + "42" + C + "49" + C + "5" + C);
    testCases.put("ark:/13030/xt12t3", C + "ar" + C + "k+" + C + "=1" + C + "30" + C + "30" + C
        + "=x" + C + "t1" + C + "2t" + C + "3" + C);
    testCases.put("/", C + "=" + C);
    testCases.put("http://n2t.info/urn:nbn:se:kb:repos-1",
        C + "ht" + C + "tp" + C + "+=" + C + "=n" + C + "2t" + C + ",i" + C + "nf" + C + "o=" + C
            + "ur" + C + "n+" + C + "nb" + C + "n+" + C + "se" + C + "+k" + C + "b+" + C + "re" + C
            + "po" + C + "s-" + C + "1" + C);
    testCases.put("what-the-*@?#!^!?", C + "wh" + C + "at" + C + "-t" + C + "he" + C + "-^" + C
        + "2a" + C + "@^" + C + "3f" + C + "#!" + C + "^5" + C + "e!" + C + "^3" + C + "f" + C);
    testCases.put("\\\"*+,<=>?^|",
        C + "^5" + C + "c^" + C + "22" + C + "^2" + C + "a^" + C + "2b" + C + "^2" + C + "c^" + C
            + "3c" + C + "^3" + C + "d^" + C + "3e" + C + "^3" + C + "f^" + C + "5e" + C + "^7" + C
            + "c" + C);
    testCases.put("Années de Pèlerinage",
        C + "An" + C + "n^" + C + "c3" + C + "^a" + C + "9e" + C + "s^" + C + "20" + C + "de" + C
            + "^2" + C + "0P" + C + "^c" + C + "3^" + C + "a8" + C + "le" + C + "ri" + C + "na" + C
            + "ge" + C);
    testCases.put("€ŽPZ", C + "^e" + C + "2^" + C + "82" + C + "^a" + C + "c^" + C + "c5" + C + "^b"
        + C + "dP" + C + "Z" + C);

    for (String id : testCases.keySet()) {
      if (!testIdCreation(id, null, ROOT + testCases.get(id))) {
        assertTrue(false);
      }
    }
  }

  /**
   * <p>
   * Test PairtreePathes used in specs.
   * 
   * @throws UnsupportedEncodingException
   * @see https://confluence.ucop.edu/display/Curation/PairTree
   */
  @Test
  public void testPairtreePathesFromSpecs() throws UnsupportedEncodingException {

    System.out.println("###  Testing testPairtreePathesFromSpecs()  ###");

    HashMap<String, String> testPathes = new HashMap<String, String>();
    HashMap<String, String> testIds = new HashMap<String, String>();

    testPathes.put("ark:/13030/xt12t3", C + "ar" + C + "k+" + C + "=1" + C + "30" + C + "30" + C
        + "=x" + C + "t1" + C + "2t" + C + "3" + C);
    testPathes.put("http://n2t.info/urn:nbn:se:kb:repos-1",
        C + "ht" + C + "tp" + C + "+=" + C + "=n" + C + "2t" + C + ",i" + C + "nf" + C + "o=" + C
            + "ur" + C + "n+" + C + "nb" + C + "n+" + C + "se" + C + "+k" + C + "b+" + C + "re" + C
            + "po" + C + "s-" + C + "1" + C);
    testPathes.put("what-the-*@?#!^!?", C + "wh" + C + "at" + C + "-t" + C + "he" + C + "-^" + C
        + "2a" + C + "@^" + C + "3f" + C + "#!" + C + "^5" + C + "e!" + C + "^3" + C + "f" + C);

    testIds.put("ark:/13030/xt12t3", "ark+=13030=xt12t3");
    testIds.put("http://n2t.info/urn:nbn:se:kb:repos-1", "http+==n2t,info=urn+nbn+se+kb+repos-1");
    testIds.put("what-the-*@?#!^!?", "what-the-^2a@^3f#!^5e!^3f");

    for (String id : testPathes.keySet()) {
      if (!testIdCreation(id, testIds.get(id), ROOT + testPathes.get(id))) {
        assertTrue(false);
      }
    }
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Create a JPairtree and check its IDs and pathes.
   * </p>
   * 
   * @param theUri
   * @param theExpectedEncodedId
   * @param theExpectedPPathString
   * @return
   * @throws UnsupportedEncodingException
   */
  private static boolean testIdCreation(String theId, String theExpectedEncodedId,
      String theExpectedPPathString) throws UnsupportedEncodingException {

    // Create JPairtree.
    JPairtree jpt = new JPairtree(theId);

    // Compute encoded ID and pairtree pathes.
    String encodedId = jpt.getPtreeEncodedId();
    String ppathString = jpt.getPtreePathString();

    // TODO LOG!
    System.out.println("Checking Pairtree for ID:");
    System.out.println("\t" + theId);
    if (theExpectedEncodedId != null) {
      System.out.println("Checking encoding:");
      System.out.println("\t" + encodedId + " =");
      System.out.println("\t" + theExpectedEncodedId);
    }
    if (theExpectedPPathString != null) {
      System.out.println("Checking pairtree path:");
      System.out.println("\t" + ppathString + " =");
      System.out.println("\t" + theExpectedPPathString);
    }

    // Compare IDs and pathes.
    if ((theExpectedEncodedId == null || theExpectedEncodedId.equals(encodedId))
        && (theExpectedPPathString == null || theExpectedPPathString.equals(ppathString))) {
      return true;
    }

    return false;
  }

}
