/*******************************************************************************
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.middleware.common;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

/*******************************************************************************
 * TODOLOG
 * 
 *******************************************************************************
 * CHANGELOG
 * 
 * 2018-010-05 - Funk - Copied from TestRDFUtils.
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * JUnit class for testing the CachingUtils implementation.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-01-13
 * @since 2018-10-05
 ******************************************************************************/

public class TestCachingUtils {
	/**
	 * 
	 */
	@Test
	public void testGetPathFromUriEscaped() {

		String dhTestHdl = "hdl:21.T11991%2F0000-0008-B832-0";
		String expDhTestPath = "hdl/21.T11991-0000-0008/21.T11991-0000-0008-B832-0";
		String resDhTestPath = CachingUtils.getCachingPathFromURI(dhTestHdl);
		// Do compare strings instead of pathes due to different file separator chars.
		String expDhTestString = new File(expDhTestPath).toString();
		String resDhTestString = new File(resDhTestPath).toString();

		if (!expDhTestString.equals(resDhTestString)) {
			System.out.println(expDhTestString + " != " + resDhTestString);
			assertTrue(false);
		}

		String dhProdHdl = "hdl:21.11113%2F0000-000B-CAC4-4";
		String expDhProdPath = "hdl/21.11113-0000-000B/21.11113-0000-000B-CAC4-4";
		String resDhProdPath = CachingUtils.getCachingPathFromURI(dhProdHdl);
		// Do compare strings instead of pathes due to different file separator chars.
		String expDhProdString = new File(expDhProdPath).toString();
		String resDhProdString = new File(resDhProdPath).toString();

		if (!expDhProdString.equals(resDhProdString)) {
			System.out.println(expDhProdString + " != " + resDhProdString);
			assertTrue(false);
		}
	}

	/**
	 * 
	 */
	@Test
	public void testGetPathFromUri() {

		String tgUri = "textgrid:1234.0";
		String expTgPath = "textgrid/12/1234.0";
		String resTgPath = CachingUtils.getCachingPathFromURI(tgUri);
		// Do compare strings instead of pathes due to different file separator chars.
		String expTgString = new File(expTgPath).toString();
		String resTgString = new File(resTgPath).toString();

		if (!expTgString.equals(resTgString)) {
			System.out.println(expTgString + " != " + resTgString);
			assertTrue(false);
		}

		String dhTestHdl = "hdl:21.T11991/0000-0008-B832-0";
		String expDhTestPath = "hdl/21.T11991-0000-0008/21.T11991-0000-0008-B832-0";
		String resDhTestPath = CachingUtils.getCachingPathFromURI(dhTestHdl);
		// Do compare strings instead of pathes due to different file separator chars.
		String expDhTestString = new File(expDhTestPath).toString();
		String resDhTestString = new File(resDhTestPath).toString();

		if (!expDhTestString.equals(resDhTestString)) {
			System.out.println(expDhTestString + " != " + resDhTestString);
			assertTrue(false);
		}

		String dhProdHdl = "hdl:21.11113/0000-000B-CAC4-4";
		String expDhProdPath = "hdl/21.11113-0000-000B/21.11113-0000-000B-CAC4-4";
		String resDhProdPath = CachingUtils.getCachingPathFromURI(dhProdHdl);
		// Do compare strings instead of pathes due to different file separator chars.
		String expDhProdString = new File(expDhProdPath).toString();
		String resDhProdString = new File(resDhProdPath).toString();
		
		if (!expDhProdString.equals(resDhProdString)) {
			System.out.println(expDhProdString + " != " + resDhProdString);
			assertTrue(false);
		}
	}

}
