/*******************************************************************************
 * This software is copyright (c) 2018 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@sub.uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.middleware.common;

import java.io.File;

/*******************************************************************************
 * TODOLOG
 * 
 *******************************************************************************
 * CHANGELOG
 * 
 * 2018-10-05 - Funk - Copied from LogUtils.
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * Caching utilities.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2018-10-05
 * @since 2018-10-05
 *******************************************************************************/

public class CachingUtils {

  public static final String TG_PATH_PREFIX = "textgrid:";
  public static final String DH_PATH_PREFIX = "hdl:";

  /**
   * <p>
   * Used for generating caching pathes from URIs.
   * </p>
   * 
   * @param theURI
   * @return The path used for caching for the specified URI.
   */
  public static String getCachingPathFromURI(String theURI) {

    String result = "";

    if (theURI.startsWith(TG_PATH_PREFIX)) {
      // cut "textgrid:", take first three chars as subfolder.
      String id = theURI.substring(9);
      String dir = id.substring(0, 2);
      result = "textgrid" + File.separatorChar + dir + File.separatorChar + id;
    }

    else if (theURI.startsWith(DH_PATH_PREFIX)) {
      // cut "hdl:" & replace "/" and "%2F", get everything but the last 6 chars "1234-5" as
      // subfolder.
      String id = theURI.substring(4).replace("/", "-").replace("%2F", "-");
      String dir = id.substring(0, id.length() - 7);
      result = "hdl" + File.separatorChar + dir + File.separatorChar + id;
    }

    return result;
  }

}
