/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.common;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 *
 * 2024-10-29 - Funk - Add REINDEX.
 *
 * 2024-07-01 - Funk - Add N4N, RECACHE, and CONSISTENCY.
 * 
 * 2018-01-11 - Funk - Add READTECHMD.
 * 
 * 2014-10-24 - Funk - Refactored to TGCrudOperations.
 * 
 * 2014-10-23 - Funk - First version.
 */

/**
 * <p>
 * The collection of TG-crud operation/method names.
 * </p>
 * 
 * @version 2024-10-29
 * @since 2014-10-23
 */

public final class CrudOperations {

  // **
  // STATIC FINALS
  // **

  public static final int INIT = 1;
  public static final int MAIN = 2;
  public static final int GETVERSION = 3;
  public static final int CREATE = 4;
  public static final int CREATEMETADATA = 5;
  public static final int READ = 6;
  public static final int READMETADATA = 7;
  public static final int UPDATE = 8;
  public static final int UPDATEMETADATA = 9;
  public static final int DELETE = 10;
  public static final int GETURI = 11;
  public static final int MOVEPUBLIC = 12;
  public static final int LOCK = 13;
  public static final int UNLOCK = 14;
  public static final int READTECHMD = 15;
  public static final int N4N = 16;
  public static final int RECACHE = 17;
  public static final int CONSISTENCY = 18;
  public static final int REINDEXMETADATA = 19;

  public static final String INIT_STRING = "INIT";
  public static final String MAIN_STRING = "MAIN";
  public static final String GETVERSION_STRING = "GETVERSION";
  public static final String CREATE_STRING = "CREATE";
  public static final String CREATEMETADATA_STRING = "CREATEMETADATA";
  public static final String READ_STRING = "READ";
  public static final String READMETADATA_STRING = "READMETADATA";
  public static final String UPDATE_STRING = "UPDATE";
  public static final String UPDATEMETADATA_STRING = "UPDATEMETADATA";
  public static final String DELETE_STRING = "DELETE";
  public static final String GETURI_STRING = "GETURI";
  public static final String MOVEPUBLIC_STRING = "MOVEPUBLIC";
  public static final String LOCK_STRING = "LOCK";
  public static final String UNLOCK_STRING = "UNLOCK";
  public static final String READTECHMD_STRING = "READTECHMD";
  public static final String N4N_STRING = "NUMBER4NOID";
  public static final String RECACHE_STRING = "RECACHE";
  public static final String CONSISTENCY_STRING = "CHECKCONSISTENCY";
  public static final String REINDEXMETADATA_STRING = "REINDEXMETADATA";

  /**
   * <p>
   * Returns the name of the operation as a string.
   * </p>
   * 
   * @param theNumber
   * @return The name of the operation as a string.
   */
  public static String getName(int theNumber) {

    switch (theNumber) {
      case INIT:
        return INIT_STRING;
      case MAIN:
        return MAIN_STRING;
      case GETVERSION:
        return GETVERSION_STRING;
      case CREATE:
        return CREATE_STRING;
      case CREATEMETADATA:
        return CREATEMETADATA_STRING;
      case READ:
        return READ_STRING;
      case READMETADATA:
        return READMETADATA_STRING;
      case UPDATE:
        return UPDATE_STRING;
      case UPDATEMETADATA:
        return UPDATEMETADATA_STRING;
      case DELETE:
        return DELETE_STRING;
      case GETURI:
        return GETURI_STRING;
      case MOVEPUBLIC:
        return MOVEPUBLIC_STRING;
      case LOCK:
        return LOCK_STRING;
      case UNLOCK:
        return UNLOCK_STRING;
      case READTECHMD:
        return READTECHMD_STRING;
      case N4N:
        return N4N_STRING;
      case RECACHE:
        return RECACHE_STRING;
      case CONSISTENCY:
        return CONSISTENCY_STRING;
      case REINDEXMETADATA:
        return REINDEXMETADATA_STRING;
      default:
        return "UNKNOWN";
    }
  }

}
