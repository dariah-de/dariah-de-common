/**
 * This software is copyright (c) 2021 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.middleware.common;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 */

/**
 * <p>
 * Some RDF constants for crud and publish and other services.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-03-16
 * @since 2021-03-16
 */

public final class RDFConstants {

  // **
  // STATIC FINALS
  // **

  public static final String RDF_XML = "RDF/XML";
  public static final String TURTLE = "TURTLE";
  public static final String NTRIPLES = "NTRIPLES";
  public static final String RDF_JSON = "RDF/JSON";
  public static final String JSON_LD = "JSON-LD";

  public static final String MEDIATYPE_TURTLE = "text/turtle";
  public static final String MEDIATYPE_NTRIPLES = "application/n-triples";

  public static final String RDFXML_FILE_SUFFIX = ".rdf";
  public static final String TTL_FILE_SUFFIX = ".ttl";
  public static final String NTRIPLES_FILE_SUFFIX = ".nt";
  public static final String RDFJSON_FILE_SUFFIX = ".rj";
  public static final String JSONLD_FILE_SUFFIX = ".jsonld";

  public static final String RDF_PREFIX = "rdf";
  public static final String HDL_PREFIX = "hdl";
  public static final String DOI_PREFIX = "doi";
  public static final String DC_PREFIX = "dc";
  public static final String DCTERMS_PREFIX = "dcterms";
  public static final String DARIAH_PREFIX = "dariah";
  public static final String DARIAHSTORAGE_PREFIX = "dariahstorage";
  public static final String PREMIS_PREFIX = "premis";

  public static final String RDF_NAMESPACE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
  public static final String HDL_NAMESPACE = "http://hdl.handle.net/";
  public static final String DOI_NAMESPACE = "http://dx.doi.org/";
  public static final String DC_NAMESPACE = "http://purl.org/dc/elements/1.1/";
  public static final String DCTERMS_NAMESPACE = "http://purl.org/dc/terms/";
  public static final String DARIAH_NAMESPACE = "http://de.dariah.eu/rdf/dataobjects/terms/";
  public static final String DARIAHSTORAGE_NAMESPACE = "https://de.dariah.eu/storage/";
  public static final String PREMIS_NAMESPACE = "http://www.loc.gov/premis/rdf/v1#";

  public static final String ELEM_DARIAH_COLLECTION = "Collection";
  public static final String ELEM_DARIAH_DATAOBJECT = "DataObject";
  public static final String DARIAH_COLLECTION = DARIAH_NAMESPACE + "Collection";
  public static final String DARIAH_DATAOBJECT = DARIAH_NAMESPACE + "DataObject";

  public static final String ELEM_RDF_TYPE = "type";
  public static final String ELEM_RDF_RESOURCE = "resource";
  public static final String ELEM_RDF_ABOUT = "about";
  public static final String ELEM_RDF_DESCRIPTION = "Description";
  public static final String RDF_TYPE = RDF_NAMESPACE + ELEM_RDF_TYPE;
  public static final String RDF_DESCRIPTION = RDF_NAMESPACE + ELEM_RDF_DESCRIPTION;

  public static final String ELEM_DC_CONTRIBUTOR = "contributor";
  public static final String ELEM_DC_COVERAGE = "coverage";
  public static final String ELEM_DC_CREATOR = "creator";
  public static final String ELEM_DC_DATE = "date";
  public static final String ELEM_DC_DESCRIPTION = "description";
  public static final String ELEM_DC_FORMAT = "format";
  public static final String ELEM_DC_IDENTIFIER = "identifier";
  public static final String ELEM_DC_LANGUAGE = "language";
  public static final String ELEM_DC_PUBLISHER = "publisher";
  public static final String ELEM_DC_RELATION = "relation";
  public static final String ELEM_DC_RIGHTS = "rights";
  public static final String ELEM_DC_SOURCE = "source";
  public static final String ELEM_DC_SUBJECT = "subject";
  public static final String ELEM_DC_TITLE = "title";
  public static final String ELEM_DC_TYPE = "type";

  public static final String ELEM_DCTERMS_HASPART = "hasPart";
  public static final String ELEM_DCTERMS_MODIFIED = "modified";
  public static final String ELEM_DCTERMS_CREATED = "created";
  public static final String ELEM_DCTERMS_FORMAT = "format";
  public static final String ELEM_DCTERMS_EXTENT = "extent";

  public static final String ELEM_PREMIS_SIZE = "hasSize";
  public static final String ELEM_PREMIS_MD = "hasMessageDigest";
  public static final String ELEM_PREMIS_MDT = "hasMessageDigestType";
  public static final String ELEM_PREMIS_MDO = "hasMessageDigestOriginator";

}
