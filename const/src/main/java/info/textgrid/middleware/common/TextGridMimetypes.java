/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license Lesser General Public (http://www.gnu.org/licenses/lgpl-3.0.txt GNU)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.middleware.common;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * TODOLOG
 * 
 * TODO Use Mimetypes from a general registry, where all types used in the TG-lab are gathered and
 * registered. Problem: Every Eclipse plugin used in the TG-lab can define their own mimetypes.
 * 
 **
 * CHANGELOG
 *
 * 2024-11-19 - Funk - Add types for image/tif and image/jpg.
 *
 * 2024-01-12 - Funk - Add new mimetype for derived text formats (application/xml;derived=true).
 * 
 * 2020-06-15 - Funk - Add text/markdown to original set.
 * 
 * 2020-05-20 - Funk - Add mimetype text/markdown.
 * 
 * 2017-08-30 - Funk - Added DARIAH-DE collection mimetype.
 * 
 * 2014-06-24 - Funk - Changed Ubbo's and Max's special aggregations.
 * 
 * 2014-04-29 - Funk - Added Ubbo's special Blumenbach aggregation.
 * 
 * 2014-04-14 - Funk - Added Ubbo's special Blumenbach mimetype.
 * 
 * 2013-09-27 - Funk - Added PLAINTEXT to original set.
 * 
 * 2013-04-05 - Funk - Added GIF mimetype and image set.
 * 
 * 2012-09-19 - Funk - Removed HTTP link to TextGrid mimetypes in the internal TextGrid wiki (was
 * parsed at compile time).
 * 
 * 2012-03-11 - Funk - Added OCTET_STREAM.
 * 
 * 2012-02-01 - Funk - Added list for objects to put into the original eXist collection.
 * 
 * 2011-02-03 - Funk - Added as maven module to tgcrud.
 * 
 * 2011-01-04 - Funk - First version.
 */

/**
 * <p>
 * The collection of all mimetypes used in the TG-lab and assorted services such as TG-crud,
 * TG-import: The strings for the TextGrid content types, used for recognition of incoming data
 * files, taken from info.textgrid.lab.core.model.contentTypes.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2024-01-12
 * @since 2011-01-04
 */

public final class TextGridMimetypes {

  // **
  // STATIC FINALS
  // **

  // Mimetypes from TextGridLab Version 2.0.5, Revision: 201310011243
  public static final String AGGREGATION = "text/tg.aggregation+xml";
  public static final String EDITION = "text/tg.edition+tg.aggregation+xml";
  public static final String COLLATION_SET = "text/collation-set+xml";
  public static final String COLLATION_EQUIV = "text/collation-equivs+xml";
  public static final String COLLECTION = "text/tg.collection+tg.aggregation+xml";
  public static final String XSD = "text/xsd+xml";
  public static final String CSS = "text/css";
  public static final String PLAINTEXT = "text/plain";
  public static final String IMPORT_EXPORT = "text/tg.imex+xml";
  public static final String ZIP = "application/zip";
  public static final String PROJECTFILE = "text/tg.projectfile+xml";
  public static final String UNKNOWN = "unknown/unknown ";
  public static final String UNKNOWN_IMAGE = "image/x-unknown";
  public static final String WORK = "text/tg.work+xml";
  public static final String LINKEDITOR = "text/linkeditorlinkedfile";
  public static final String JPEG = "image/jpeg";
  public static final String JPG = "image/jpg";
  public static final String TIFF = "image/tiff";
  public static final String TIF = "image/tif";
  public static final String PNG = "image/png";
  public static final String GIF = "image/gif";
  public static final String WORKFLOW = "text/tg.workflow+xml";
  public static final String WORKFLOW_GWDL = "text/gwdl.workflow+xml";
  public static final String SERVICE_DESCRIPTION = "text/tg.servicedescription+xml";
  public static final String XML = "text/xml";
  public static final String DTD = "application/xml-dtd";
  public static final String XSLT = "text/xml+xslt";
  public static final String MEI = "text/mei+xml";
  public static final String ATF = "application/xml;derived=true";

  // Vendor specific mimetypes.
  public static final String INPUTFORM = "text/tg.inputform+rdf+xml";
  public static final String INPUTFORM_AGGREGATION = "text/tg.inputform+tg.aggregation+xml";

  // Mimetype for creating ByteArrayDataSources.
  public static final String OCTET_STREAM = "application/octet-stream";

  // DARIAH-DE Repository mimetypes.
  public static final String DARIAH_COLLECTION = "text/vnd.dariah.dhrep.collection+turtle";

  // Mimetype for files such as README.md used as a project description at testgridrep.org.
  public static final String MARKDOWN = "text/markdown";

  // Special mimetype for projects displayed on textgridrep.org.
  public static final String PORTALCONFIG = "text/tg.portalconfig+xml";

  // Sets that contains all original, aggregation, and image types.
  public static final Set<String> ORIGINAL_SET =
      new HashSet<String>(Arrays.asList(XML, MEI, PLAINTEXT, INPUTFORM, MARKDOWN, ATF));
  public static final Set<String> AGGREGATION_SET =
      new HashSet<String>(Arrays.asList(AGGREGATION, EDITION, COLLECTION, INPUTFORM_AGGREGATION));
  public static final Set<String> IMAGE_SET =
      new HashSet<String>(Arrays.asList(UNKNOWN_IMAGE, JPEG, JPG, TIFF, TIF, PNG, GIF));
}
