/*
 * #%L confclient %% Copyright (C) 2011 TextGrid Consortion %% This program is free software: you
 * can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>. #L%
 */
package info.textgrid.middleware.confclient;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.codehaus.jettison.AbstractXMLStreamReader;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.mapped.MappedXMLStreamReader;

/**
 * Class for retrieving configuration from confserv, for clientside use (e.g. TextGrid-services,
 * TextGridLab). JSON is used for config-retrieval.
 * 
 */
public class ConfservClient {

  private static int DEFAULT_SB_VALUE = 1024;

  private String confServUrl;

  /**
   * Constructor
   * 
   * @param confServUrl URL of ConfigService
   */
  public ConfservClient(String confServUrl) {
    this.confServUrl = confServUrl;
  }

  /**
   * Get single config value from confserver for a given key. For a list of valid keys have a look
   * at {@link ConfservClientConstants}
   * 
   * @param key
   * @return value
   * @throws IOException
   * @throws JSONException
   */
  public String getValue(String key) throws IOException, JSONException {

    URL url = new URL(confServUrl + "/getValueJ?key=" + key);
    InputStream in = url.openStream();
    String body = readStringFromStream(in);
    JSONObject obj = new JSONObject(body);

    return obj.getString("value");

  }

  /**
   * Get all configuration from confserver, returns all entries in a key/value hashmap
   * 
   * @return hashmap with key/value entries for configuration
   * @throws IOException
   * @throws JSONException
   * @throws XMLStreamException
   */
  public HashMap<String, String> getAll() throws IOException, JSONException,
      XMLStreamException {

    HashMap<String, String> confMap = new HashMap<String, String>();

    URL url = new URL(confServUrl + "/getAllJ");
    InputStream in = url.openStream();
    String body = readStringFromStream(in);

    JSONObject obj = new JSONObject(body);
    AbstractXMLStreamReader reader = new MappedXMLStreamReader(obj);
    int event;
    while (true) {
      event = reader.next();
      if (event == XMLStreamReader.START_ELEMENT
          && reader.getLocalName().equals("set")) {
        reader.next();
        String key = reader.getText();
        reader.next();
        reader.next();
        reader.next();
        String value = reader.getText();
        confMap.put(key, value);
      }
      if (event == XMLStreamConstants.END_DOCUMENT) {
        reader.close();
        break;
      }
    }
    return confMap;
  }

  /*
   * from org.apache.cxf.helpers.IOUtils
   */
  /**
   * @param in
   * @return
   * @throws IOException
   */
  private static String readStringFromStream(InputStream in)
      throws IOException {

    StringBuilder sb = new StringBuilder(DEFAULT_SB_VALUE);

    for (int i = in.read(); i != -1; i = in.read()) {
      sb.append((char) i);
    }

    in.close();

    return sb.toString();
  }

}
