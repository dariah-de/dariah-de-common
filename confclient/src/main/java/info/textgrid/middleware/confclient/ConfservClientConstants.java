/*
 * #%L
 * confclient
 * %%
 * Copyright (C) 2011 TextGrid Consortion
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package info.textgrid.middleware.confclient;

public class ConfservClientConstants {

	/**
	 * URL of TextGrid-LogService
	 */
	public static final String LOG_SERVICE = "logservice";
	
	/**
	 * URL for TextGrid-Auth-Service 
	 */
	public static final String TG_AUTH = "tgauth";
	
	/**
	 * URL for TextGrid-Search-Service 
	 */
	public static final String TG_SEARCH = "tgsearch";
	
	/**
	 *  URL for TextGrid-Search-Service for public Objects
	 */
	public static final String TG_SEARCH_PUBLIC = "tgsearch-public";
	
	/**
	 * eXist-SOAP-Query-Service-Endpoint URL
	 */
	@Deprecated
	public static final String EXIST_URL = "exist";
	
	/**
	 * URL for RDF-Repository
	 */
	@Deprecated	
	public static final String RDF_URL = "rdf-repository";
	
	/**
	 * URL for TG-Crud
	 */
	public static final String TG_CRUD = "tgcrud";
	
	/**
	 * URL for RBAC
	 */
	public static final String RBAC = "rbac";
	
	/**
	 * URL for TextGrid-User-Authorisation / Login
	 */
	public static final String AUTHZ = "authz";
	
	/**
	 * URL for webpublisher
	 */
	public static final String WEBPUBLISH = "webpublish";
	
	/**
	 * URL for tgpublish service
	 */
	public static final String TGPUBLISH = "tgpublish";
	
	/**
	 * Currently used TextGrid-Metadata-Namespace
	 */
	public static final String NS = "ns";
	
	/**
	 * Currently used TextGrid-Metadata-Schema
	 */
	public static final String SCHEMA = "schema";
	
	/**
	 * 
	 */
	public static final String LAST_API_CHANGE = "last-api-change";
	
	/**
	 * 
	 */
	public static final String APPROVED_SERVICES_TEXTGRID_URI = "ApprovedServicesTextGridURI";
	
	/**
	 * 
	 */
	public static final String DEFAULT_METADATA_TRANSFORM_STYLESHEET_TEXTGRIDURI = "DefaultMetadataTransformStylesheetTextGridURI";
	
	/**
	 * 
	 */
	public static final String STREAMING_EDITOR = "StreamingEditor";
	
	/**
	 * 
	 */
	public static final String WORKFLOW = "workflow";

	/**
	 * 
	 */
	public static final String COPY_WORKFLOW_TEXTGRIDURI = "CopyWorkflowTextGridURI";
	
	
}
