/*
 * #%L
 * confclient
 * %%
 * Copyright (C) 2011 TextGrid Consortion
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package info.textgrid.middleware.confclient;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.stream.XMLStreamException;

import org.codehaus.jettison.json.JSONException;
import org.junit.Test;
import org.junit.Ignore;

/*
 * TODO: mock httpserver
 */

public class ConfservClientTest {

	private final static String confservurl = "http://textgrid-ws3.sub.uni-goettingen.de/axis2/services/confserv";
	
	@Ignore
	@Test
	public void testGetValue() throws IOException, JSONException {
		
		ConfservClient cc = new ConfservClient(confservurl);
		String res = cc.getValue(ConfservClientConstants.NS);
		assertEquals(res, "http://textgrid.info/namespaces/metadata/core/2010");

	}
	
	@Ignore
	@Test
	public void testGetAll() throws IOException, JSONException, XMLStreamException {
		ConfservClient cc = new ConfservClient(confservurl);
		HashMap<String, String> res = cc.getAll();
		
		assertEquals(res.get(ConfservClientConstants.NS), "http://textgrid.info/namespaces/metadata/core/2010");

	}
}
