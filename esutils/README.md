# ESUtils

Utils Classes to build JSON Objects from TextGrid Metadata 
and Data for storing in ElasticSearch.

Includes a sample for CRUD Operations (src/main/java/info/textgrid/TestCrud.java)
and a Tool for transforming directories of TextGrid data to JSON objects

# Using the Command Line Tool

build with 

    mvn clean package -Pshaded

then there is a esutils-(version)-shaded.jar in /target

it could be used to transform a directory structure like this

/tmp/tg2es/metadata/obj1.0
/tmp/tg2es/metadata/obj2.0
/tmp/tg2es/metadata/obj3.0
/tmp/tg2es/structure/original/obj1.0
/tmp/tg2es/structure/aggregation/obj2.0

where obj1.0 is an xml object and obj2.0 an aggregation, obj3.0 may be an image

running 

    tools/tgmd2json.sh /tmp/tg2es



would create a directory /tmp/tg2es/json, where the JSON objects 
for the textgridobjects are found for ingest into textgrid

# Tools & Mapping

in `tools/` there is the mapping and a little script to prepare a local 
elasticsearch installation. The elasticsearch mapping for TextGrid is located in 
`tools/textgrid/createIndex/textgrid-mapping.json`

## Create index:

* /tools/textgrid/createIndex/createAllNonpublic.sh
* /tools/textgrid/createIndex/createAllPublic.sh

Scripts which create the public or the nonpublic-index, mainly doing:

1. drop old index (if existing)
2. create new index and upload mapping `textgrid-mapping.json`

Usage:

        ./createAllNonpublic.sh localhost:9202

## Update mapping

* /tools/textgrid/createIndex/update-mapping.sh

This script does:

1. Upload `textgrid-mapping.json`
2. run `update_by_query` to apply the mapping

Usage:

        ./update-mapping.sh localhost:9202/textgrid-public


Note: running the `update_by_query` may cause OOMs on elasticsearch if it 
is running with unsufficient amount of ram. What could be done:

1. stop puppet and all tomcat servers

        $ servive puppet stop
        $ service tomcat-* stop

2. edit `/etc/elasticsearch/masternode/jvm.options` and `/etc/elasticsearch/workhorse/jvm.options` 
and change `Xmx` and `Xms` to `8G`

3. restart elasticsearch

        $ service elasticsearch-masternode restart
        $ service elasticsearch-workhorse restart

4. run the script (this may take a long time)

        $ ./update-mapping.sh localhost:9202/textgrid-nonpublic

5. start puppet again, this will resize elasticsearch memory and start all tomcats

        $ servive puppet start


