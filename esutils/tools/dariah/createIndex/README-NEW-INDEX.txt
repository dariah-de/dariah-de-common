stop elasticsearch:
service elasticsearch-masternode stop
service elasticsearch-workhorse stop

change cluster name in config files (puppet!):
emacs /etc/elasticsearch/masternode/elasticsearch.yml
emacs /etc/elasticsearch/workhorse/elasticsearch.yml

restart elasticsearch:
service elasticsearch-masternode start
service elasticsearch-workhorse start

create new index with scope=dariah:
cd /usr/local/src/tgcommon-git/esutils/tools/dariah/createIndex/
./createAllPublic.sh localhost:9202 dariah

set new index in config files (puppet!)
