#!/bin/bash

newMapping() {

  url=$1

  echo -e "\n\nupload mapping to elasticsearch $url: "
  # for updating an existing mapping only the properties are uploaded. jq extracts this parts from dariah-mapping.json
  if cat dariah-mapping.json | jq "{properties: .mappings.metadata.properties}" | curl --fail -XPUT "${url}/_mapping/metadata" -H "Content-Type: application/json" -d @-
  then
    echo -e "done\n"
  else
    echo -e "error: upload mapping failed\ntry yourself:"
    echo -e 'cat dariah-mapping.json | jq "{properties: .mappings.metadata.properties}" | curl -XPUT "${url}/_mapping/metadata" -H "Content-Type: application/json" -d @-'
    exit 1
  fi

  echo "applying the new mapping on $url"
  curl -XPOST "${url}/_update_by_query?refresh&conflicts=proceed&pretty"

}

if [ $1"x" == "x" ]
then
        echo "provide the host, http port, index and type of elastic search instance, e.g."
        echo "$0 localhost:9202/textgrid-public"
        exit 1;
else
    newMapping $1
fi
