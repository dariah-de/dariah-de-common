#! /bin/bash

#------------------SCRIPT DESCRIPTION----------------------------------------------
#SCRIPT-NAME: dhinsertPublic.sh-------------------------------------------------
#SCRIPT-FUNCTION: indexing data into the DH-ElasticSearch index for public data
#DATE: 2021-03-08
#----------------------------------------------------------------------------------

ELASTIC_SEARCH_URL=http://localhost:9202/dariah-public/metadata

recursivIngest ()
{
        counter=0

        for SRCFILE in $(find $1 -type f); do

                echo -n "indexing $SRCFILE  -->  "

                curl -XPUT $ELASTIC_SEARCH_URL/`basename $SRCFILE .json` -H 'Content-Type: application/json' -d @$SRCFILE
                counter=$(( $counter +1 ))

                echo " ...OK"
        done

}

if [ $1"x" == "x" ]
then
        echo provide a directory which contains dariah metadata in json
        exit 1;
else
    echo -e "Starte den Indexierungsprozess der JSON Dateien \n\n"
    sleep 1
    recursivIngest $1
    echo Ingest finished $counter files
fi
