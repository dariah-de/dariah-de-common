#!/bin/bash

createIndex() {

  host=$1

  echo -e "\ndeleting the old version of the index: "

  curl -XDELETE "${host}/textgrid-nonpublic"

  echo -e "\n\ncreating new index and uploading the textgridmetadata conform mapping for ElasticSearch: "
  curl -XPUT "${host}/textgrid-nonpublic" -H 'Content-Type: application/json' -d @textgrid-mapping.json

}

if [ $1"x" == "x" ]
then
        echo provide the host and http port of elastic search instance, e.g.
        echo "$0 localhost:9202"
        exit 1;
else
    createIndex $1
fi
