# some tools to reindex a list of textgrid-uris from disk to elasticsearch

Note: If you want to copy json entries from public to nonpublic index, you may also want to look at
https://gitlab.gwdg.de/dariah-de/textgridrep/repository-maintenance/-/tree/master/es-public-to-nonpub

## 1. gather list of uris

you will know how to find your uri list. see an example below

### example: 10 nonpublic inputform objects that do not have a fulltext index field.

        ./getRdfInputFormList.sh 10000 | jq -r '.hits.hits[]._source.textgridUri' > uris.list

the first param of `getRdfInputFormList.sh` is the number of uris to get.

if you do not want to fiddle with a scroll you may just run this query, index and upload 
to elasticsearch - and then run it again, as then only the missing items without fulltext
filter will be listed on the second run.

## 2. collect data

create dirs

        ../collectData/create_dirs.sh

public?

        while read line; do ./reindex.sh -p $line; done < uris.list

or nonpublic

        while read line; do ./reindex.sh $line; done < uris.list

## 3. convert to json, with the help of esutils

public?

        java -jar ../../../target/esutils-4.0.2-SNAPSHOT-shaded.jar tgrep es-dataingest-public/

        
or nonpublic

        java -jar ../../../target/esutils-4.0.2-SNAPSHOT-shaded.jar tgrep es-dataingest-nonpublic/

 
## 4. upload to elasticsearch

public?

        ../ingest.py -i textgrid-public es-dataingest-public/json/
        
or nonpublic

        ../ingest.py -i textgrid-nonpublic es-dataingest-nonpublic/json/
        
        
## one example run - reindex 100 uris:

        $ ./getRdfInputFormList.sh 0 1 | jq .hits.total
        14526

        $ rm -r es-dataingest-*

        $ ../collectData/create_dirs.sh

        $ ./getRdfInputFormList.sh 100 | jq -r '.hits.hits[]._source.textgridUri' > uris.list

        $ while read line; do ./reindex.sh $line; done < uris.list
        [...]
        [OK] /data/nonpublic/productive/pairtree_root/te/xt/gr/id/+3/cb/0h/,0 exists
        [OK] /data/nonpublic/productive/pairtree_root/te/xt/gr/id/+3/cb/0j/,0 exists

        $ java -jar ../../../target/esutils-4.0.2-SNAPSHOT-shaded.jar tgrep es-dataingest-nonpublic/
        [...]
        es-dataingest-nonpublic/metadata/3c5cp.0
        es-dataingest-nonpublic/metadata/3c5db.0
        transformed 100 files in 1098ms ~=1s

        $ ../ingest.py -i textgrid-nonpublic es-dataingest-nonpublic/json/
        [...]
        es-dataingest-nonpublic/json/3cb0h.0.json : updated
        es-dataingest-nonpublic/json/3cb0j.0.json : updated

        [2021-10-19 23:27:51.495419] ingest finished - created: 0 / updated: 100 / errors: 0 / unknown: 0
        delta: 0:00:04.480299

        $ ./getRdfInputFormList.sh 0 1 | jq .hits.total
        14426


see, there are 100 less entries without a fulltext index field
