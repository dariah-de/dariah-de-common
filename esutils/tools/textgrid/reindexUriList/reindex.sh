#!/bin/bash

### commons -> move to /opt/dhrep/functions.d

#######################################################################
# function isMimeGrep
# test if pattern is in mime of grid metadata
# $1 mime pattern (--> grep re)
# $2 file in grid
function isMimeGrep {
  local CNT=$(grep -c -e "format>$1</"  $2.meta)
  if [ "$CNT" -ge "1" ]
    then
	    return 0
    else
	    return 1
    fi
}


#######################################################################
# function isAggregation
# tests if file in grid is an aggregation
# $1 file in Grid.
function isAggregation {
    isMimeGrep "text/\(tg\..*\)*tg.aggregation+xml" $1
}

#######################################################################
# function isAggregationOrPortalconfig
# tests if file in grid is an aggregation or portalconfig
# $1 file in Grid.
function isAggregationOrPortalconfig {
    isMimeGrep "\(text/\(tg\..*\)*tg.aggregation+xml\|text/tg.portalconfig+xml\)" $1
}

#######################################################################
# function isImage
# tests if file in grid is an image
# $1 file in Grid.
function isImage {
    isMimeGrep "image/.*" $1
}

#######################################################################
# function isTextXML
# test if file in grid is text/xml, application/xml or
# some other like application/xml;derived=true
# $1 file in Grid
function isTextXML {
    isMimeGrep "\(text\|application\)/xml.*" $1
}

#######################################################################
# function isTextXML or RDF inputform
# test if file in grid is text/xml, application/xml(.*)
# or some tg.inputform object
# $1 file in Grid
function isTextXMLorRDF {
    isMimeGrep "\(text\|application\)/\(xml.*\|tg.inputform+rdf+xml\)" $1
}

#######################################################################
# function isTextPlain
# test if file in grid is text/plain
# $1 file in Grid
function isTextPlain {
    isMimeGrep "text/plain" $1
}

#######################################################################
# function isTextPlainOrMarkdown
# test if file in grid is text/plain or text/markdown
# $1 file in Grid
function isTextPlainOrMarkdown {
    isMimeGrep "text/\(plain\|markdown\)" $1
}

#######################################################################
# function copyToEsutilsDirs
# copy file to directory structure for esutils
# what goes where? see:
# https://gitlab.gwdg.de/dariah-de/dariah-de-common/-/blob/develop/const/src/main/java/info/textgrid/middleware/common/TextGridMimetypes.java
# $1 filename in pairtree of grid
function copyToEsutilsDirs {

    local GRIDFILE=$1
    local IN=$(echo $GRIDFILE | sed "s|^.*+||")

    local FNAME=${IN/,/.}
    local PTLOC=$(pairtreeFN $IN)

    # copy metadatafile
    cp ${GRIDFILE}.meta $MDOUT_DIR$FNAME
    
    isAggregationOrPortalconfig $GRIDFILE
    if [ "$?" -eq "0" ]
    then
        # copy aggregation or portalconfig
        cp $GRIDFILE $AGGOUT_DIR$FNAME
    else
	    isTextXMLorRDF $GRIDFILE
	    if [ "$?" -eq "0" ]
	    then
        # copy original xml
        cp $GRIDFILE $XMLOUT_DIR$FNAME
      else
        isTextPlainOrMarkdown $GRIDFILE
        if [ "$?" -eq "0" ]
        then
           cp $GRIDFILE $XMLOUT_DIR$FNAME.txt
        fi
	    fi
    fi

}


### / end commons

case "$0" in
    /*)
        SCRIPTPATH=$(dirname "$0")
        ;;
    *)
        SCRIPTPATH=$(dirname "$PWD/$0")
        ;;
esac

# common config
DATA_PATH=/data/nonpublic/productive/pairtree_root/te/xt/gr/id
SESAME_URL=http://localhost:9091/openrdf-sesame/repositories/textgrid-nonpublic
#ELASTICSEARCH_URL=http://localhost:9202/textgrid-nonpublic/metadata
OUTPUT_BASE_DIR=./es-dataingest-nonpublic/

# source common functions and settings
source /opt/dhrep/functions.d/textgrid-shared.sh
source /opt/dhrep/functions.d/inspect.sh

function show_help {
    echo "usage: $0 textgridUri"
    echo "options:"
    echo "--verbose | -v    verbose"
    echo "--help    | -h    help (this message)"
    echo "--public  | -p    look in public repo (nonpublic is default)"
}

while :; do
    case $1 in
        -h|-\?|--help)
            show_help    # Display a usage synopsis.
            exit
            ;;
        -v|--verbose)
            VERBOSE=$((VERBOSE + 1))  # Each -v adds 1 to verbosity.
            ;;
        -p|--public)
            DATA_PATH=/data/public/productive/pairtree_root/te/xt/gr/id
            SESAME_URL=http://localhost:9091/openrdf-sesame/repositories/textgrid-public
            #ELASTICSEARCH_URL=http://localhost:9202/textgrid-public/metadata
            OUTPUT_BASE_DIR=./es-dataingest-public/
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
    esac

    shift
done

# exit and show help if no uri given
if [ "$#" == "0" ]; then
    echo -e "\e[31mno textgridUri given\e[0m"
    show_help    # Display a usage synopsis.
    exit
fi

MDOUT_DIR=${OUTPUT_BASE_DIR}metadata/
XMLOUT_DIR=${OUTPUT_BASE_DIR}structure/original/
# the aggregation dir also contains portalconfig now
# (dir could be renamed in esutils-java and here if need be)
AGGOUT_DIR=${OUTPUT_BASE_DIR}structure/aggregation/

#### here the main programm starts

uri=$1

# split incoming param after : to remove the textgrid: prefix if there
id=${uri#*:}

path=$(id2path $id)

if [ -d $path ] ; then
    ok "${path} exists"
    #ls -alh $path/textgrid*
else 
   error "${path} does not exist in storage"
fi

copyToEsutilsDirs ${path}/textgrid+${id/./,}

