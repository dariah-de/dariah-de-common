#!/bin/bash

curl -s -XGET http://localhost:9202/textgrid-nonpublic/_search --header 'Content-Type: application/json' -d '{
  "from": '0',
  "size": '$1',
  "query": {
    "bool": {
      "filter": [
        {
          "bool": {
            "must": [
              {
                "term": {
                  "format.untouched": {
                    "value": "text/tg.inputform+rdf+xml"
                  }
                }
              }
            ],
            "must_not": [
              {
                "exists": {
                  "field": "fulltext"
                }
              }
            ]
          }
        }
      ]
    }
  },
  "_source": {
    "includes": [
      "textgridUri"
    ]
  }
}'

