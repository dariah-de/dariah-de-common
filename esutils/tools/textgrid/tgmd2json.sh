#!/bin/bash
# -Xmx1024m may lead to heap space problems ...?

SELF=`dirname $0`
JAR=`ls $SELF/../../target/esutils-*-SNAPSHOT-jar-with-dependencies.jar`

java -Dfile.encoding=UTF-8 -jar $JAR $1 $2

