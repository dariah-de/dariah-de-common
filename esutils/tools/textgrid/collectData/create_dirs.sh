#!/bin/bash

for OUTPUT_BASE_DIR in ./es-dataingest-nonpublic/ ./es-dataingest-public/

do
  JSON_DIR=${OUTPUT_BASE_DIR}json/
  MDOUT_DIR=${OUTPUT_BASE_DIR}metadata/
  XMLOUT_DIR=${OUTPUT_BASE_DIR}structure/original/
  AGGOUT_DIR=${OUTPUT_BASE_DIR}structure/aggregation/

  mkdir -p $MDOUT_DIR
  mkdir -p $XMLOUT_DIR
  mkdir -p $AGGOUT_DIR
  mkdir -p $JSON_DIR
done

