#!/bin/bash

PIDFILE=es_ingest_copy_nonpublic.pid
PAIRTREE_ROOT=/data/nonpublic/productive/pairtree_root/te/xt/gr/id/

OUTPUT_BASE_DIR=./es-dataingest-nonpublic/
MDOUT_DIR=${OUTPUT_BASE_DIR}metadata/
# xml dir also has text/plain and markdown
XMLOUT_DIR=${OUTPUT_BASE_DIR}structure/original/
# the aggregation dir also contains portalconfig now
# (dir could be renamed in esutils-java and here if need be)
AGGOUT_DIR=${OUTPUT_BASE_DIR}structure/aggregation/

#######################################################################
# function pairtreeFN
# generate pair tree paths
# $1 string that shall be transformed to pairtree path
function pairtreeFN {
    local OUT=""
    local LEN=${#1}
    local STEPS=$(($LEN / 2))
    for (( i = 0; i < $LEN; i+=2 ))
    do
        OUT=$OUT/${1:i:2}
    done;
    echo $OUT
}

#######################################################################
# function isMimeGrep
# test if pattern is in mime of grid metadata
# $1 mime pattern (--> grep re)
# $2 file in grid
function isMimeGrep {
    local CNT=$(grep -c -e "format>$1</"  $2.meta)
    if [ "$CNT" -ge "1" ]
    then
	return 0
    else
	return 1
    fi
}


#######################################################################
# function isAggregation
# tests if file in grid is an aggregation
# $1 file in Grid.
function isAggregation {
    isMimeGrep "text/\(tg\..*\)*tg.aggregation+xml" $1
}

#######################################################################
# function isAggregationOrPortalconfig
# tests if file in grid is an aggregation or portalconfig
# $1 file in Grid.
function isAggregationOrPortalconfig {
    isMimeGrep "\(text/\(tg\..*\)*tg.aggregation+xml\|text/tg.portalconfig+xml\)" $1
}

#######################################################################
# function isImage
# tests if file in grid is an image
# $1 file in Grid.
function isImage {
    isMimeGrep "image/.*" $1
}

#######################################################################
# function isTextXML
# test if file in grid is text/xml
# $1 file in Grid
function isTextXML {
    isMimeGrep "text/xml" $1
}

#######################################################################
# function isTextPlain
# test if file in grid is text/plain
# $1 file in Grid
function isTextPlain {
    isMimeGrep "text/plain" $1
}

#######################################################################
# function isTextPlainOrMarkdown
# test if file in grid is text/plain or text/markdown
# $1 file in Grid
function isTextPlainOrMarkdown {
    isMimeGrep "text/\(plain\|markdown\)" $1
}

#######################################################################
# function copyToEsutilsDirs
# copy file to directory structure for esutils
# $1 filename in pairtree of grid
function copyToEsutilsDirs {

    local GRIDFILE=$1
    local IN=$(echo $GRIDFILE | sed "s|^.*+||")

    local FNAME=${IN/,/.}
    local PTLOC=$(pairtreeFN $IN)

    # copy metadatafile
    cp ${GRIDFILE}.meta $MDOUT_DIR$FNAME
    
    isAggregationOrPortalconfig $GRIDFILE
    if [ "$?" -eq "0" ]
    then
        # copy aggregation or portalconfig
        cp $GRIDFILE $AGGOUT_DIR$FNAME
    else
	    isTextXML $GRIDFILE
	    if [ "$?" -eq "0" ]
	    then
          	# copy original xml
          	cp $GRIDFILE $XMLOUT_DIR$FNAME
        else
          isTextPlainOrMarkdown $GRIDFILE
          if [ "$?" -eq "0" ]
          then
             cp $GRIDFILE $XMLOUT_DIR$FNAME.txt
          fi
	    fi
    fi

}



#######################################################################
# decide whether to search for files or not
if [ $1"x" != "x" ]
then
    copyToEsutilsDirs $1
else
    echo $$ > $PIDFILE
    echo "started"
    date
    find $PAIRTREE_ROOT $FIND_RESTRICTION -type f -not -name "*.meta*" -not -name "*.old" -not -name "*.tech" -exec $0 \{} \;
    rm $PIDFILE
    echo "finished"
    date
fi
