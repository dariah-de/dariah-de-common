#!/usr/bin/python3
# ingest json data to elasticsearch in ~ half the time of a recursive curl calling from bash
# install requirements: sudo apt install python3-requests python3-click python3-elasticsearch

import json
import click
from os import path, listdir
from datetime import datetime, timedelta 
from elasticsearch import Elasticsearch

# show help output if argument or option missing
click.Context.get_usage = click.Context.get_help

@click.command()
@click.argument('directory', type=click.Path(exists=True))
@click.option('--esindex', '-i', required=True, help='the index. normally "textgrid-public" or "textgrid-nonpublic"')
@click.option('--port', '-p', default=9202, show_default=True, help='elasticsearch port')
@click.option('--start-from', '-s', help="continue uploading from the ID specified, (to continue a broken ingest), e.g. '-s 11mm9.0'")
def main(directory, port, esindex, start_from):
    """Upload directory of json files (*.json) to textgrid elasticsearch index.✨

       The files from a directory are sorted by name for upload,
       so continuing a broken ingest is possible with "-s"
    """

    started = datetime.now()
    click.echo(f'[{started}] uploading *.json from {directory} to http://localhost:{port}/{esindex}/metadata')

    es = Elasticsearch([{'host': 'localhost', 'port': port}])

    errors = 0
    updated = 0
    created = 0
    unknown = 0

    filenames = sorted(listdir(directory))
    if start_from is not None:
        if not start_from.endswith('.json'):
            start_from = start_from + '.json'
        click.echo(f'starting ingest from {start_from}')
        start_id = filenames.index(start_from)
        filenames = filenames[start_id:]

    for filename in filenames:
        if filename.endswith(".json"):
            loc = path.join(directory, filename)
            file = open(loc)
            content = file.read()
            id = filename[0:-5]

            res = es.index(index=esindex, 
                     ignore=400, 
                     id=id,
                     doc_type='metadata',
                     request_timeout=60,
                     body=json.loads(content))

            if 'error' in res:
                click.echo(f'{loc} : {res}')
                errors += 1
            else: 
                click.echo(f'{loc} : {res["result"]}')
                if res["result"] == "updated":
                    updated += 1
                elif res["result"] == "created":
                    created += 1
                else:
                    unkown += 1

    finished = datetime.now()
    delta = finished - started
    click.echo(f'\n[{finished}] ingest finished - created: {created} / updated: {updated} / errors: {errors} / unknown: {unknown}')
    click.echo(f'delta: {delta}')


if __name__ == "__main__":
    main()

