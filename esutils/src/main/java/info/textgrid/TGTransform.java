package info.textgrid;

import info.textgrid.middleware.common.elasticsearch.ESJsonBuilder;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import jakarta.xml.bind.JAXB;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParserException;

public class TGTransform {

	private static int	SEC	= 1000;

	/**
	 * @param args
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParseException
	 * @throws XmlPullParserException
	 * @throws TransformerConfigurationException
	 * @throws ParserConfigurationException
	 */
	public static void main(String[] args) throws SAXException, IOException,
			ParseException, XmlPullParserException,
			TransformerConfigurationException, ParserConfigurationException {

		if (args.length < 1) {

			System.out.println("provide path to textgrid data as argument.\n"
					+ "optionally provide the output path as second."
					+ " default ouput is /json in textgrid-data-path.");
			return;
		}

		String path = args[0];

		if (new File(path).isFile()) {

			ESJsonBuilder esj = new ESJsonBuilder();
			InputStream metadata = new FileInputStream(path);
			try {
				System.out.println(esj.buildTextgridMetadataObject(
						JAXB.unmarshal(metadata, MetadataContainerType.class))
						.toJSONString());
			} catch (TransformerException e) {
				System.out.println("error transforming due to XML failures: "
						+ e.getMessage());
			}

		} else {

			String mpath = path + "metadata/";
			String ftpath = path + "structure/original/";
			String aggpath = path + "structure/aggregation/";
			String outpath = path + "json/";

			// change outpath if second command line var provided
			if (args.length > 1) {
				outpath = args[1];
				if (!outpath.endsWith("/")) {
					outpath += '/';
				}
			}

			System.out.println("using " + new File(outpath).getCanonicalPath()
					+ " for output");

			ESJsonBuilder esj = new ESJsonBuilder();

			long start = System.currentTimeMillis();
			int cnt = 0;

			for (String uri : new File(mpath).list()) {

				cnt++;

				InputStream data = null;
				InputStream metadata = new FileInputStream(mpath + uri);
				System.out.println(mpath + uri);

				File xmlFile = new File(ftpath + uri);
				File txtFile = new File(ftpath + uri + ".txt");
				File aggFile = new File(aggpath + uri);

				JSONObject result;

				try {
					if (xmlFile.exists()) {
						data = new FileInputStream(xmlFile);
						result = esj.buildMetaAndFulltextJsonFromXML(
						    JAXB.unmarshal(metadata, MetadataContainerType.class), data);
					} else if (txtFile.exists()) {
						data = new FileInputStream(txtFile);
						result = esj.buildMetaAndFulltextJsonFromTxt(
						    JAXB.unmarshal(metadata, MetadataContainerType.class), data);
					} else if (aggFile.exists()) {
						data = new FileInputStream(aggFile);
						result = esj.buildMetaAndAggregationObject(
						    JAXB.unmarshal(metadata, MetadataContainerType.class), data);
					} else {
						result = esj.buildTextgridMetadataObject(
						    JAXB.unmarshal(metadata, MetadataContainerType.class));
					}

					FileUtils.writeStringToFile(new File(outpath + uri
							+ ".json"), result.toJSONString());
					metadata.close();
					if (data != null) {
						data.close();
					}

				} catch (TransformerException e) {
					System.out
							.println("error transforming due to XML failures: "
									+ e.getMessage());
				}
			}

			long end = System.currentTimeMillis();
			System.out.println("transformed " + cnt + " files in "
					+ (end - start) + "ms ~=" + ((end - start) / SEC) + "s");
		}
	}

}
