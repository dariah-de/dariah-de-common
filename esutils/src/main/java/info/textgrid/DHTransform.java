package info.textgrid;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.net.URLEncoder;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BoundedInputStream;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.jena.rdf.model.Model;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.json.simple.parser.ParseException;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParserException;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;
import info.textgrid.middleware.common.elasticsearch.ESJsonBuilder;

/**
 * <p>
 * Prepares a re-index of ElasticSearch for all repository data existing in the PublicStorage from
 * PID metadata.
 * </p>
 * 
 * <ol>
 * <li>First get all PID Handles containing INST 2000 (should be all DARIAH-DE Repository PIDs):
 * curl -D- -u "user:pw" -X GET -H "Content-Type: application/json"
 * https://pid.gwdg.de/handles/21.11113/?INST=2000</li>
 * <li>Get out all PIDs that have got a PUBLISHED metadata flag set to "true"</li>
 * <li>Get the URL from every package</li>
 * <li>Download the BAGs</li>
 * <li>Store all needed metadata files for every bag</li>
 * <li>Put all metadata into esutils to create JSON files</li>
 * <li>Use JSON files to re-index ElasticSearch</li>
 * </ol>
 */

public class DHTransform {

  // **
  // FINALS
  // **

  public static final String HDL_URL = "http://hdl.handle.net/";

  private static final String PROPERTIES_FILE =
      "src/main/resources/info/textgrid/middleware/common/handle/PROD.handle.properties";
  // private static final String PROPERTIES_FILE =
  // "src/main/resources/info/textgrid/middleware/common/handle/DHREPWORKSHOP.handle.properties";
  // private static final String PROPERTIES_FILE =
  // "src/main/resources/info/textgrid/middleware/common/handle/TREP.handle.properties";
  // private static final String PROPERTIES_FILE =
  // "src/main/resources/info/textgrid/middleware/common/handle/VM1.handle.properties";
  // private static final String DHREP_PATH = "vm1";
  // private static final String DHREP_PATH = "trep";
  private static final String DHREP_PATH = "repository";
  private static final String METADATA_PATH = "metadata";
  private static final String JSON_PATH = "json";
  private static final String HANDLE_LIST_FILE = "handle_list.txt";
  private static final String HELP = "please provide pass to be processed:\n" +
      "  1 - retrieve all dariah-de pids from handle service and store it to ./dhrep/ folder\n" +
      "  2 - download all bag's metadata and store it to ./dhrep/metadata/ folder\n" +
      "  3 - create json files in ./dhrep/json/ folder\n" +
      "  X - do all of the above one after another\n\n"
      + "...then please copy all the JSON files to dhrep server and use script dhinsertPublic.sh in /usr/local/src/tgcommon-git/esutils/tools/dariah/ for re-indexing the es database!";
  private static final String DMD_PREFIX = "01_metadata";
  private static final String ADMMD_PREFIX = "02_adm";
  private static final String TECHMD_PREFIX = "03_tech";
  private static final String STARTING = "==> starting - pass ";
  private static final String ENDING = "<== completed - pass ";

  // **
  // STATICS
  // **

  private static String handleServiceLocation;
  private static String handleServicePath;
  private static String handleServiceInstitutionID;
  private static String handleServiceUser;
  private static String handleServicePassword;
  private static String handleSearchKey;
  private static String handleSearchValue;

  private static int publishedCount = 0;
  private static int bagCount = 0;
  private static int doiCount = 0;
  private static int skipped404Count = 0;
  private static int downloadCount = 0;

  /**
   * @param args
   * @throws SAXException
   * @throws IOException
   * @throws ParseException
   * @throws XmlPullParserException
   * @throws ParserConfigurationException
   * @throws AuthenticationException
   * @throws JSONException
   * @throws UnsupportedOperationException
   * @throws TransformerException
   * @throws DatatypeConfigurationException
   * @throws XMLStreamException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  public static void main(String[] args)
      throws SAXException, IOException, ParseException, XmlPullParserException,
      ParserConfigurationException, AuthenticationException, UnsupportedOperationException,
      JSONException, TransformerException, XMLStreamException, DatatypeConfigurationException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    if (args.length < 1) {
      System.out.println(HELP);
      return;
    }

    // Read properties file.
    File propertiesFile = new File(PROPERTIES_FILE);
    if (!propertiesFile.exists()) {
      System.err.println("PROPERTIES FILE NOT FOUND: " + PROPERTIES_FILE);
    }

    Properties p = new Properties();
    p.load(new FileInputStream(propertiesFile));
    handleServiceLocation = p.getProperty("location");
    handleServicePath = p.getProperty("path");
    handleServiceInstitutionID = p.getProperty("institution_id");
    handleServiceUser = p.getProperty("user");
    handleServicePassword = p.getProperty("pass");
    handleSearchKey = p.getProperty("search_key");
    handleSearchValue = p.getProperty("search_val");

    String pass = args[0];

    // **
    // Process PASS 1: Retrieve all DARIAH-DE PIDs from handle service and store it to ./dhrep/
    // folder.
    // **
    if (pass.equals("1")) {
      callPass1(pass);
    }

    // **
    // Process PASS 2: Download all BAG's metadata and store it to ./dhrep/metadata/ folder.
    // **
    else if (pass.equals("2")) {
      callPass2(pass);
    }

    // **
    // Process PASS 3: Create JSON files in ./dhrep/json/ folder.
    // **
    else if (pass.equals("3")) {
      callPass3(pass);
    }

    // **
    // Process PASS X: Do passes 1-3 one after another.
    // **
    else if (pass.equals("X")) {
      callPass1(pass);
      callPass2(pass);
      callPass3(pass);
    }
  }

  /**
   * <p>
   * PASS 1: Query the Handle PID service for all the PIDs of INST=2000 (should be all DARIAH-DE
   * PIDs!).
   * </p>
   * 
   * @throws ClientProtocolException
   * @throws IOException
   * @throws AuthenticationException
   * @see https://doc.pidconsortium.eu
   * @see curl -D- -u "YOURUSERNAME:YOURPASSWORD" -X GET -H "Content-Type: application/json"
   *      https://epic.grnet.gr/api/v2/handles/11239/?URL=*grnet*
   */
  private static void getPIDsFromPidService()
      throws ClientProtocolException, IOException, AuthenticationException {

    System.out.println("retrieving pids from handle service...");

    String pidRequest = handleServiceLocation + "/" + handleServicePath + "/"
        + handleServiceInstitutionID + "?" + handleSearchKey + "=" + handleSearchValue + "&limit=0";

    // Get response.
    HttpResponse response =
        getHTTPServiceResponse(pidRequest, handleServiceUser, handleServicePassword);

    // Store service response to disk.
    String allTheHandles = IOUtils.toString(response.getEntity().getContent(), "UTF-8");

    File allPids = new File(DHREP_PATH, HANDLE_LIST_FILE);
    FileWriter w = new FileWriter(allPids);
    w.append(allTheHandles);
    w.close();

    System.out.println("stored pid service response: " + allPids.getCanonicalPath());
  }

  /**
   * <p>
   * Get information on one PID. Get URL of DARIAH-DE PublicStorage and download BAG, then store
   * metadata files for further processing. Only get PUBLISHED=true PIDs. We do need all
   * 
   * "type":"BAG", "parsed_data":"https://cdstar.de.dariah.eu/public/EAEA0-F888-A0A4-390C-0"
   * 
   * URLs, if
   * 
   * "type":"PUBLISHED", "parsed_data":"true".
   * </p>
   * 
   * FIXME We only have 55 PUBLISHED keys for 570 BAGs? Why is that?
   * 
   * @param thePID
   * @throws IOException
   * @throws ClientProtocolException
   * @throws AuthenticationException
   * @throws JSONException
   * @throws UnsupportedOperationException
   * @see https://doc.pidconsortium.eu
   * @see curl -D- -u "YOURUSERNAME:YOURPASSWORD" -X GET -H "Content-Type: application/json"
   *      https://epic.grnet.gr/api/v2/handles/11239/05C3DB56-5692-11E3-AF8F-1C6F65A666B5
   */
  private static void downloadBAGandStoreMetadata(String thePID)
      throws ClientProtocolException, IOException, AuthenticationException,
      UnsupportedOperationException, JSONException {

    // Get Handle metadata.
    String pidRequest = handleServiceLocation + "/" + handleServicePath + "/"
        + handleServiceInstitutionID + "/" + thePID;
    HttpResponse pidMetadataResponse =
        getHTTPServiceResponse(pidRequest, handleServiceUser, handleServicePassword);

    // Create JSON object from response string.
    JSONArray pidInfo =
        new JSONArray(IOUtils.toString(pidMetadataResponse.getEntity().getContent(), "UTF-8"));

    // Get BAG URL from PID metadata.
    String bagLocation = "";
    for (int i = 0; i < pidInfo.length(); i++) {
      JSONObject jo = pidInfo.getJSONObject(i);
      String type = jo.getString("type");
      String data = jo.getString("parsed_data");
      if (type.equals("PUBLISHED") && data.equals("true")) {
        publishedCount++;
      }
      if (type.equals("BAG")) {
        bagLocation = data;
        bagCount++;
      }
      if (type.equals("DOI")) {
        doiCount++;
      }
    }

    // Download BagIt BAG from DARIAH-DE PublicStorage, if BAG metadata URL is existing.
    if (bagLocation != null && !bagLocation.isEmpty()) {

      // Create folder.
      File folder =
          new File(DHREP_PATH + File.separatorChar + METADATA_PATH + File.separatorChar
              + handleServiceInstitutionID + "_" + thePID);
      if (folder.exists()) {
        System.out.println("skipping download of " + thePID + "! FOLDER ALREADY EXISTS: "
            + folder.getCanonicalPath());
        return;
      }

      System.out.println("downloading " + thePID + ": " + bagLocation);

      HttpResponse bagResponse = getHTTPServiceResponse(bagLocation);

      if (bagResponse == null) {
        throw new FileNotFoundException("data not found in storage!");
      }

      boolean created = folder.mkdirs();
      if (!created) {
        String message = "ERROR CREATING FOLDER: " + folder.getCanonicalPath();
        throw new IOException(message);
      }

      // Extract BAG, read and store all the metadata files.
      System.out.println("extracting metadata files: " + handleServiceInstitutionID + "/" + thePID);

      ZipInputStream theZIPStream = new ZipInputStream(bagResponse.getEntity().getContent());

      boolean gotDMD = false;
      boolean gotADMMD = false;
      boolean gotTECHMD = false;
      ZipEntry entry;
      while ((entry = theZIPStream.getNextEntry()) != null) {

        String e = entry.getName().substring(entry.getName().lastIndexOf(File.separatorChar) + 1);

        // Break loop if metadata has already been read completely.
        if (gotDMD && gotADMMD && gotTECHMD) {
          break;
        }

        // Look for and store descriptive metadata file.
        if (e.startsWith(DMD_PREFIX)) {
          String metadata =
              IOUtils.toString(new BoundedInputStream(theZIPStream, entry.getSize()), "UTF-8");
          File dmd = new File(folder, e);
          FileWriter w = new FileWriter(dmd);
          w.append(metadata);
          w.close();

          System.out.println("  >> written dmd file: " + dmd.getCanonicalPath());

          gotDMD = true;
          theZIPStream.closeEntry();
          continue;
        }

        // Look for and store administrative metadata file.
        if (e.startsWith(ADMMD_PREFIX)) {
          String adm =
              IOUtils.toString(new BoundedInputStream(theZIPStream, entry.getSize()), "UTF-8");
          File admmd = new File(folder, e);
          FileWriter w = new FileWriter(admmd);
          w.append(adm);
          w.close();

          System.out.println("  >> written admmd file: " + admmd.getCanonicalPath());

          gotADMMD = true;
          theZIPStream.closeEntry();
          continue;
        }

        // Look for and store TECHMD metadata file.
        if (e.startsWith(TECHMD_PREFIX)) {
          String tech =
              IOUtils.toString(new BoundedInputStream(theZIPStream, entry.getSize()), "UTF-8");
          File techmd = new File(folder, e);
          FileWriter w = new FileWriter(techmd);
          w.append(tech);
          w.close();

          System.out.println("  >> written techmd file: " + techmd.getCanonicalPath());

          gotTECHMD = true;
          theZIPStream.closeEntry();
          continue;
        }
      }

      downloadCount++;

      theZIPStream.close();
    }
  }

  /**
   * <p>
   * Do create JSON files from metadata RDF XML files.
   * </p>
   * 
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ParseException
   * @throws TransformerException
   * @throws DatatypeConfigurationException
   * @throws XMLStreamException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  private static void createJSONFiles()
      throws IOException, XMLStreamException, DatatypeConfigurationException, SAXException,
      ParserConfigurationException, TransformerException, ParseException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    // Get all files in PID named folders.
    File pidFolders[] = new File(DHREP_PATH + File.separatorChar + METADATA_PATH).listFiles();
    long dmdErrorAmount = 0;
    long admmdErrorAmount = 0;
    for (File f : pidFolders) {

      boolean dmdError = false;
      boolean admmdError = false;

      System.out.println("entering folder: " + f.getCanonicalPath());

      URI prefixUri = URI.create("" + f.getName().replaceAll("_", "/"));

      // Create RDF XML from TTL DMD metadata file.
      File dmdTTLFile = new File(f, DMD_PREFIX + ".ttl");
      String dmd = getPreparedStringFromUnpreparedDMDModelFile(prefixUri, dmdTTLFile);

      if (dmd.isEmpty()) {
        dmdError = true;
        dmdErrorAmount++;
        System.err.println("RDF string is empty for DMD " + f.getName());
      }

      // Create RDF XML from TTL ADMMD metadata file.
      File admmdTTLFile = new File(f, ADMMD_PREFIX + ".ttl");
      String admmd = getPreparedStringFromUnpreparedADMMDModelFile(prefixUri, admmdTTLFile);

      if (admmd.isEmpty()) {
        admmdError = true;
        admmdErrorAmount++;
        System.err.println("RDF string is empty for ADMMD " + f.getName());
      }

      // Get TECHMD XML.
      File ttlTECHMDFile = new File(f, TECHMD_PREFIX + ".xml");
      String techmd = IOUtils.toString(new FileInputStream(ttlTECHMDFile), "UTF-8");

      // Create and store JSON file.
      if (dmdError) {
        System.err.println(
            " >> NO json file written due to error in DMD --> JSON transformation: " + f.getName());
      } else if (admmdError) {
        System.err
            .println(" >> NO json file written due to error in ADMMD --> JSON transformation: "
                + f.getName());
      } else {
        ESJsonBuilder eb = new ESJsonBuilder();
        org.json.simple.JSONObject json = eb.buildDariahMetadataObject(dmd, admmd, techmd, "");

        String dooferDateiname = URLEncoder.encode(prefixUri.toASCIIString(), "UTF-8");
        File jFile = new File(DHREP_PATH + File.separatorChar + JSON_PATH, dooferDateiname);
        FileWriter jsonFile = new FileWriter(jFile);
        json.writeJSONString(jsonFile);
        jsonFile.close();

        System.out.println(" >> json file written: " + jFile.getCanonicalPath());
      }
    }

    if (dmdErrorAmount != 0) {
      System.err.println("Overall errors in DMD --> JSON transformation: " + dmdErrorAmount);
    }
    if (admmdErrorAmount != 0) {
      System.err.println("Overall errors in ADMMD --> JSON transformation: " + admmdErrorAmount);
    }
  }

  /**
   * @param theRequest
   * @return
   * @throws ClientProtocolException
   * @throws IOException
   * @throws AuthenticationException
   */
  private static HttpResponse getHTTPServiceResponse(String theRequest)
      throws ClientProtocolException, IOException, AuthenticationException {
    return getHTTPServiceResponse(theRequest, null, null);
  }

  /**
   * @param theRequest
   * @param theUser
   * @param thePassword
   * @return
   * @throws ClientProtocolException
   * @throws IOException
   * @throws AuthenticationException
   */
  private static HttpResponse getHTTPServiceResponse(String theRequest, String theUser,
      String thePassword) throws ClientProtocolException, IOException, AuthenticationException {

    HttpResponse result;

    System.out.println("http service query in progress: " + theRequest);

    // Set credentials, if provided.
    HttpClient client;
    if (theUser != null && thePassword != null) {
      CredentialsProvider credprov = new BasicCredentialsProvider();
      UsernamePasswordCredentials credentials =
          new UsernamePasswordCredentials(theUser, thePassword);
      credprov.setCredentials(AuthScope.ANY, credentials);
      client = HttpClientBuilder.create().setDefaultCredentialsProvider(credprov).build();
    } else {
      client = HttpClientBuilder.create().build();
    }

    // Get response.
    result = client.execute(new HttpGet(theRequest));

    // Check status.
    int statusCode = result.getStatusLine().getStatusCode();
    String reasonPhrase = result.getStatusLine().getReasonPhrase();
    if (statusCode != HttpStatus.SC_OK) {
      if (statusCode == 404) {
        return null;
      } else {
        System.err.println("HTTP ERROR: " + statusCode + " " + reasonPhrase);
        throw new IOException();
      }
    }

    System.out.println("http response: " + statusCode + " " + reasonPhrase);

    return result;
  }

  /**
   * <p>
   * Create /dhrep/metdaata/ folder.
   * </p>
   * 
   * @throws IOException
   */
  private static void createMetadataFolder() throws IOException {

    File metadataFolder = new File(DHREP_PATH, METADATA_PATH);
    if (!metadataFolder.exists()) {
      boolean folderCreated = metadataFolder.mkdirs();
      if (folderCreated) {
        System.out.println("folder creation complete: " + metadataFolder.getCanonicalPath());
      } else {
        System.err.println("FOLDER CREATION FAILED: " + metadataFolder.getCanonicalPath());
      }
    }
  }

  /**
   * <p>
   * Create /dhrep/json/ folders.
   * </p>
   * 
   * @throws IOException
   */
  private static void createJSONFolder() throws IOException {

    File jsonFolder = new File(DHREP_PATH, JSON_PATH);
    if (!jsonFolder.exists()) {
      boolean metadataFolderCreated = jsonFolder.mkdirs();
      if (metadataFolderCreated) {
        System.out.println("folder creation complete: " + jsonFolder.getCanonicalPath());
      } else {
        System.err.println("FOLDER CREATION FAILED: " + jsonFolder.getCanonicalPath());
      }
    }
  }

  /**
   * <p>
   * Process PASS 1: Retrieve all DARIAH-DE PIDs from handle service and store it to ./dhrep/
   * folder.
   * </p>
   * 
   * @param pass
   * @throws IOException
   * @throws AuthenticationException
   */
  private static void callPass1(final String pass) throws IOException, AuthenticationException {

    String intpass = "1";

    System.out.println(STARTING + intpass);

    createMetadataFolder();

    getPIDsFromPidService();

    System.out.println(ENDING + intpass);
  }

  /**
   * <p>
   * PASS 2: Download all BAG's metadata and store it to ./dhrep/metadata/ folder.
   * </p>
   * 
   * @throws IOException
   * @throws JSONException
   * @throws UnsupportedOperationException
   * @throws AuthenticationException
   */
  private static void callPass2(final String pass)
      throws IOException, AuthenticationException, UnsupportedOperationException, JSONException {

    String intpass = "2";

    System.out.println(STARTING + intpass);

    createMetadataFolder();

    File allPIDs = new File(DHREP_PATH, HANDLE_LIST_FILE);
    if (!allPIDs.exists()) {
      String message = "HANDLE LIST FILE NOT FOUND. PLEASE TRY PASS 1 FIRST!";
      System.err.println(message);
      throw new IOException(message);
    }

    List<String> handleList = IOUtils.readLines(new FileInputStream(allPIDs), "UTF-8");

    System.out.println("found " + handleList.size() + " pid" + (handleList.size() != 1 ? "s" : "")
        + " in handle list file");

    int count = 0;
    for (String pid : handleList) {
      count++;

      System.out.println();
      System.out.println("[" + count + "/" + handleList.size() + ": " + pid + "]  -->  DOIs: "
          + doiCount + " | BAGs: " + bagCount + " | PUBLISHed: " + publishedCount + " | NOT FOUND: "
          + skipped404Count + " | DOWNLOADed: " + downloadCount);
      System.out.println();

      System.out.println("download and store metadata for pid " + pid);

      try {
        downloadBAGandStoreMetadata(pid);
      } catch (FileNotFoundException e) {
        skipped404Count++;
        System.out.println("skipped error: " + e.getMessage());
      }
    }

    System.out.println(ENDING + intpass);
  }

  /**
   * <p>
   * Create JSON files in ./dhrep/json/ folder.
   * </p>
   * 
   * @param pass
   * @throws IOException
   * @throws ParseException
   * @throws TransformerException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws DatatypeConfigurationException
   * @throws XMLStreamException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  private static void callPass3(String pass) throws IOException, SAXException,
      ParserConfigurationException, TransformerException, ParseException, XMLStreamException,
      DatatypeConfigurationException, org.apache.jena.riot.lang.extra.javacc.ParseException {

    String intpass = "3";

    System.out.println(STARTING + intpass);

    createJSONFolder();

    createJSONFiles();

    System.out.println(ENDING + intpass);
  }

  /**
   * @param theURI
   * @param theFile
   * @return
   * @throws FileNotFoundException
   * @throws IOException
   * @throws XMLStreamException
   * @throws DatatypeConfigurationException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  public static String getPreparedStringFromUnpreparedDMDModelFile(URI theURI, File theFile)
      throws FileNotFoundException, IOException, XMLStreamException,
      DatatypeConfigurationException, org.apache.jena.riot.lang.extra.javacc.ParseException {

    String result = "";

    Model dmdOrigModel = RDFUtils.readModel(IOUtils.toString(new FileInputStream(theFile), "UTF-8"),
        RDFConstants.TURTLE);
    Model dmdModel4ES = RDFUtils.prepareDMDforJSONCreation(theURI, dmdOrigModel);

    if (dmdModel4ES.isEmpty()) {
      return result;
    }

    StringWriter dmdWriter = new StringWriter();
    dmdModel4ES.write(dmdWriter, RDFConstants.RDF_XML);
    result = dmdWriter.toString();
    dmdWriter.close();

    return result;
  }

  /**
   * @param theURI
   * @param theFile
   * @return
   * @throws FileNotFoundException
   * @throws IOException
   * @throws XMLStreamException
   * @throws DatatypeConfigurationException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  public static String getPreparedStringFromUnpreparedADMMDModelFile(URI theURI, File theFile)
      throws FileNotFoundException, IOException, XMLStreamException,
      DatatypeConfigurationException, org.apache.jena.riot.lang.extra.javacc.ParseException {

    String result = "";

    Model admmdOrigModel = RDFUtils
        .readModel(IOUtils.toString(new FileInputStream(theFile), "UTF-8"), RDFConstants.TURTLE);
    Model admmdModel4ES = RDFUtils.prepareADMMDforJSONCreation(theURI, admmdOrigModel);

    if (admmdModel4ES.isEmpty()) {
      return result;
    }

    StringWriter admmdWriter = new StringWriter();
    admmdModel4ES.write(admmdWriter, RDFConstants.RDF_XML);
    result = admmdWriter.toString();
    admmdWriter.close();

    return result;
  }

}
