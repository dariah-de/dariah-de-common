package info.textgrid.middleware.common.elasticsearch;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import info.textgrid.DHTransform;
import info.textgrid.TGTransform;

/**
 *
 */

public class Dispatcher {

  /**
   * 
   */
  private static final Map<String, Class<?>> ENTRY_POINTS = new HashMap<String, Class<?>>();

  static {
    ENTRY_POINTS.put("tgrep", TGTransform.class);
    ENTRY_POINTS.put("dhrep", DHTransform.class);
  }

  /**
   * @param args
   * @throws Exception
   */
  public static void main(final String[] args) throws Exception {

    if (args.length < 1) {
      printHelp();
      return;
    }

    final Class<?> entryPoint = ENTRY_POINTS.get(args[0]);

    if (entryPoint == null) {
      System.out.println("command not existing: " + args[0]);
      printHelp();
      return;
    }

    final String[] argsCopy =
        args.length > 1 ? Arrays.copyOfRange(args, 1, args.length) : new String[0];
    entryPoint.getMethod("main", String[].class).invoke(null, (Object) argsCopy);
  }

  /**
   * 
   */
  private static void printHelp() {
    System.out.println("please provide command to execute, one of ");
    for (String entry : ENTRY_POINTS.keySet()) {
      System.out.println("  " + entry);
    }
  }

}
