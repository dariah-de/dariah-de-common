/**
 * This software is copyright (c) 2024 by
 *
 * TextGrid Consortium (https://textgrid.de)
 *
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Ubbo Veentjer, SUB Göttingen
 * @author Andreas Siemon, GWDG
 * @author Fatih Berber, GWDG
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 */

package info.textgrid.middleware.common.elasticsearch;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.xml.bind.JAXB;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Templates;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.mapped.Configuration;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.copy.HierarchicalStreamCopier;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import com.thoughtworks.xstream.io.xml.XppReader;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;

/**
 * TODOLOG
 *
 * TODO Just check if we have to instantiate the Saxon transformer explicitly (for Jenkins does not
 * use it by default as my local installation).
 *
 * TODO Test if Saxon XML transformation is faster.
 *
 * TODO Test if transformation to plain text slows down tgcrud updates of big TEI files.
 *
 * TODO Only use one JSON library (now JSON simple and JSONXML are used, while also Jettison could
 * be on some class pathes from CXF).
 *
 * TODO Optimise memory usage (streaming may not be possible).
 *
 **
 * CHANGELOG
 *
 * 2024-10-22 - Funk - Add error handling for non-valid TECHMD HTML, fixes #35.
 *
 * 2017-12-18 - Funk - Using Transformer Templates now for ESJaon transformer thread safety!
 *
 * 2015-11-13 - Funk/Brodhun - Added AdmMD, TechMD, and ProvMD things for DARIAH-DE Repository.
 *
 * 2015-03-23 - Funk Removed debug output. Funk Fixed typo in dc2json XSLT.
 *
 * 2015-03-03 - Funk - Changed type of DC metadata to String, so RDF DC can be processed, too. Using
 * Saxon for XSLT now, we need XSLT V2.0!
 *
 * 2014-03-13 - Funk - Implemented the projectfile data XML to JSON method.
 *
 * 2013-10-25 - Funk - Changed String to Object type for key/value pair creation method.
 *
 * 2013-09-24 - Funk - Minor changes and removed some warnings.
 */

/**
 * <p>
 * Provides methods to create JSON objects from XML and plaintext.
 * </p>
 *
 * @author Fatih Berber, GWDG
 * @author Ubbo Veentjer, SUB Göttingen
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @author Maximilian Brodhun, SUB Göttingen
 * @version 2024-10-22
 * @since 2010-05-21
 **/

public class ESJsonBuilder {

  // **
  // FINAL STATICS
  // **

  public static final String DEFAULT_CHARSET = "UTF-8";
  public static final String TG_ORIGINAL_METADATA_FIELDNAME = "original_metadata";
  public static final String TG_ORIGINAL_DATA_FIELDNAME = "original_data";
  public static final String TG_PLAINTEXT_DATA_FIELDNAME = "fulltext";
  public static final String TG_METADATA_XSLT_JSON = "textgridMetadata2Json.xsl";
  public static final String TG_XML_TO_PLAINTEXT = "textgridXml2Text.xsl";
  public static final String DH_DMD_TO_JSON = "dariahDMD2Json.xsl";
  public static final String DH_ADMMD_TO_JSON = "dariahAdmMD2Json.xsl";
  public static final String DH_TECHMD_TO_JSON = "dariahTechMD2Json.xsl";
  public static final String DH_PROVMD_TO_JSON = "dariahProvMD2Json.xsl";

  // **
  // STATE
  // **

  private Templates textgridMetadataTransformerTemplate;
  private Templates textgridXml2textTransformerTemplate;
  private Templates dariahDMDTransformerTemplate;
  private Templates dariahAdmMDTransformerTemplate;
  private Templates dariahTechMDTransformerTemplate;
  private Templates dariahProvMDTransformerTemplate;

  // TODO Should original data be included with plain text by default?
  private boolean storeOriginal = false;
  private Logger log = Logger.getLogger(ESJsonBuilder.class.toString());

  /**
   * @throws TransformerConfigurationException
   * @throws TransformerFactoryConfigurationError
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws TransformerConfigurationException
   */
  public ESJsonBuilder()
      throws SAXException, ParserConfigurationException, TransformerConfigurationException {

    // Using thread safe transformer templates now
    // See:
    // <https://docs.oracle.com/javase/8/docs/api/javax/xml/transform/TransformerFactory.html#newTemplates-javax.xml.transform.Source>
    // and <https://stackoverflow.com/a/5573734>

    // TextGrid transformer.
    try {
      this.textgridMetadataTransformerTemplate = TransformerFactory.newInstance().newTemplates(
          new StreamSource(this.getClass().getResourceAsStream(TG_METADATA_XSLT_JSON)));
    } catch (TransformerConfigurationException e) {
      this.log.log(Level.SEVERE, "error creating transformer for " + TG_METADATA_XSLT_JSON, e);
      throw (e);
    }
    try {
      this.textgridXml2textTransformerTemplate = TransformerFactory.newInstance()
          .newTemplates(new StreamSource(this.getClass().getResourceAsStream(TG_XML_TO_PLAINTEXT)));
    } catch (TransformerConfigurationException e) {
      this.log.log(Level.SEVERE, "error creating transformer for " + TG_XML_TO_PLAINTEXT, e);
      throw (e);
    }

    // DARIAH transformer.
    // FIXME Don't use IMPL class instantiation here!!!! That's a really bad habit!!!!
    try {
      this.dariahDMDTransformerTemplate = new net.sf.saxon.TransformerFactoryImpl()
          .newTemplates(new StreamSource(this.getClass().getResourceAsStream(DH_DMD_TO_JSON)));
    } catch (TransformerConfigurationException e) {
      this.log.log(Level.SEVERE, "error creating transformer for " + DH_DMD_TO_JSON, e);
      throw (e);
    }
    try {
      this.dariahAdmMDTransformerTemplate =
          new net.sf.saxon.TransformerFactoryImpl().newTemplates(new StreamSource(this.getClass()
              .getResourceAsStream(DH_ADMMD_TO_JSON)));
    } catch (TransformerConfigurationException e) {
      this.log.log(Level.SEVERE, "error creating transformer for " + DH_ADMMD_TO_JSON, e);
      throw (e);
    }
    try {
      this.dariahTechMDTransformerTemplate = new net.sf.saxon.TransformerFactoryImpl()
          .newTemplates(new StreamSource(this.getClass().getResourceAsStream(DH_TECHMD_TO_JSON)));
    } catch (TransformerConfigurationException e) {
      this.log.log(Level.SEVERE, "error creating transformer for " + DH_TECHMD_TO_JSON, e);
      throw (e);
    }
    try {
      this.dariahProvMDTransformerTemplate = new net.sf.saxon.TransformerFactoryImpl()
          .newTemplates(new StreamSource(this.getClass().getResourceAsStream(DH_PROVMD_TO_JSON)));
    } catch (TransformerConfigurationException e) {
      this.log.log(Level.SEVERE, "error creating transformer for " + DH_PROVMD_TO_JSON, e);
      throw (e);
    }

    this.log.info("using " + this.textgridMetadataTransformerTemplate.getClass().getCanonicalName()
        + " for xslt");
  }

  /**
   * <p>
   * Build JSONObject which includes metadata and data, and optionally plaintext field from data.
   * </p>
   *
   * @param metadata the textgrid metadata
   * @param data the data
   * @param fulltext weather to include plaintext field from data
   * @param xmlOrNotXml if true object is being transformed
   * @return
   * @throws IOException
   * @throws ParseException
   * @throws TransformerException
   * @throws XmlPullParserException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  @Deprecated
  public JSONObject buildMetaAndDataObject(MetadataContainerType metadata, InputStream data,
      boolean fulltext, boolean xmlOrNotXml) throws IOException, ParseException,
      XmlPullParserException, TransformerException, SAXException, ParserConfigurationException {

    JSONObject resJsonObject = new JSONObject();

    resJsonObject.putAll(buildTextgridMetadataObject(metadata));

    if (fulltext) {
      try {
        resJsonObject.putAll(this.buildFulltextDataObject(data, xmlOrNotXml));
      } catch (TransformerException e) {
        this.log.info("could not build fulltext string" + e.getMessage());
      }
    } else {
      resJsonObject.putAll(this.buildDataObject(data));
    }

    return resJsonObject;
  }

  /**
   * Create a metadata object with fullext field. fulltext is extracted from data with an xslt, so
   * data needs to be xml
   *
   * @param metadata textgrid metadata object
   * @param data XML data
   * @return JSONobject for elasticsearch
   * @throws IOException
   * @throws ParseException
   * @throws XmlPullParserException
   * @throws TransformerException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  public JSONObject buildMetaAndFulltextJsonFromXML(MetadataContainerType metadata,
      InputStream data) throws IOException, ParseException,
      XmlPullParserException, TransformerException, SAXException, ParserConfigurationException {

    JSONObject resJsonObject = new JSONObject();

    resJsonObject.putAll(buildTextgridMetadataObject(metadata));

    try {
      resJsonObject.putAll(this.buildJsonPlaintextFieldFromXML(data));
    } catch (TransformerException e) {
      this.log.info("could not build fulltext and escaped xml fields from xml: " + e.getMessage());
    }

    return resJsonObject;
  }

  /**
   * @param metadata
   * @param data
   * @return
   * @throws IOException
   * @throws XmlPullParserException
   * @throws ParseException
   * @throws TransformerException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  public JSONObject buildMetaAndFulltextJsonFromTxt(MetadataContainerType metadata,
      InputStream data) throws IOException,
      XmlPullParserException, ParseException, TransformerException, SAXException,
      ParserConfigurationException {

    JSONObject resJsonObject = new JSONObject();

    resJsonObject.putAll(buildTextgridMetadataObject(metadata));
    resJsonObject.putAll(this.buildJsonPlaintextField(IOUtils.toString(data, DEFAULT_CHARSET)));

    return resJsonObject;
  }

  /**
   * Create a metadata object with original aggregation xml included.
   *
   * @param metadata textgrid metadata object
   * @param data aggregation XML
   * @return JSONobject for elasticsearch
   * @throws IOException
   * @throws ParseException
   * @throws XmlPullParserException
   * @throws TransformerException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  public JSONObject buildMetaAndAggregationObject(MetadataContainerType metadata, InputStream data)
      throws IOException, ParseException, TransformerException, XmlPullParserException,
      SAXException, ParserConfigurationException {

    JSONObject resJsonObject = new JSONObject();

    resJsonObject.putAll(buildTextgridMetadataObject(metadata));
    resJsonObject.putAll(this.buildJsonAggregationField(data));

    return resJsonObject;
  }

  /**
   * <p>
   * Build a base64 object, for original data storage.
   *
   * Attention: storage of original data is disabled, look for elasticsearch ingest pipeline
   * extension or buildMetaAndAggregationObject for aggregations
   * </p>
   *
   * @param data the data
   * @return
   * @throws IOException
   */
  @Deprecated
  public JSONObject buildDataObject(InputStream data) throws IOException {

    JSONObject resJsonObject = new JSONObject();

    System.out.println(
        "attention: storage of original data is disabled, look for elasticsearch ingest pipeline extension or buildMetaAndAggregationObject for aggregations");

    return resJsonObject;
  }

  /**
   * <p>
   * Return plaintext field from XML, optionally also a field with original data.
   * </p>
   *
   * @param data the data
   * @param xmlOrNotXml if true object is being transformed
   * @return
   * @throws IOException
   * @throws TransformerException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  @Deprecated
  public JSONObject buildFulltextDataObject(InputStream data, boolean xmlOrNotXml)
      throws IOException, TransformerException, SAXException, ParserConfigurationException {
    return buildFulltextDataObject(data, this.storeOriginal, xmlOrNotXml);
  }

  /**
   * <p>
   * Return plaintext field from XML, optionally also a field with original data.
   * </p>
   *
   * Attention: storage of original data is disabled, look for elasticsearch ingest pipeline
   * extension or buildMetaAndAggregationObject for aggregations
   *
   * TODO: Empty field should be returned if XML invalid or no data given.
   *
   * @param data
   * @param includeOriginal Attention: storage of original data is disabled
   * @param xmlOrNotXml if true object is being transformed
   * @return
   * @throws IOException
   * @throws TransformerException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  @Deprecated
  public JSONObject buildFulltextDataObject(InputStream data, boolean includeOriginal,
      boolean xmlOrNotXml)
      throws IOException, TransformerException, SAXException, ParserConfigurationException {

    JSONObject resJsonObject = new JSONObject();
    String dataString;
    dataString = IOUtils.toString(data);

    String plaintext = dataString;
    if (xmlOrNotXml) {
      plaintext = transform(this.textgridXml2textTransformerTemplate, dataString);
    }

    resJsonObject.put(ESJsonBuilder.TG_PLAINTEXT_DATA_FIELDNAME, plaintext);

    if (includeOriginal) {
      System.out.println(
          "attention: storage of original data is disabled, look for elasticsearch ingest pipeline extension");
    }

    return resJsonObject;
  }

  /**
   * build JSONobject with plaintext field extracted from XML and a field containing original xml as
   * string
   *
   *
   * @param data the xml to extract plaintext from
   * @return
   * @throws IOException
   * @throws TransformerException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  public JSONObject buildJsonPlaintextFieldFromXML(InputStream data)
      throws IOException, TransformerException, SAXException, ParserConfigurationException {

    String dataString = IOUtils.toString(data, DEFAULT_CHARSET);
    String plaintext = transform(this.textgridXml2textTransformerTemplate, dataString);

    JSONObject resJsonObject = new JSONObject();
    resJsonObject.putAll(buildJsonPlaintextField(plaintext));

    return resJsonObject;
  }

  /**
   * Build JSONobject with plaintext field
   *
   * @param plaintext
   * @return
   */
  public JSONObject buildJsonPlaintextField(String plaintext) {

    JSONObject resJsonObject = new JSONObject();
    resJsonObject.put(ESJsonBuilder.TG_PLAINTEXT_DATA_FIELDNAME, plaintext);

    return resJsonObject;
  }

  /**
   * Build JSONobject with aggregation field (stored in original-data)
   *
   * @param data the original aggregation xml
   * @return
   */
  public JSONObject buildJsonAggregationField(InputStream data) throws IOException {

    JSONObject resJsonObject = new JSONObject();
    String dataString = IOUtils.toString(data, DEFAULT_CHARSET);
    String dataBase64 = Base64.encodeBase64String(dataString.getBytes());
    resJsonObject.put(ESJsonBuilder.TG_ORIGINAL_DATA_FIELDNAME, dataBase64);

    return resJsonObject;
  }

  /**
   * <p>
   * Build JSON object from key/value pair.
   * </p>
   *
   * @param theKey
   * @param theObject
   * @return
   */
  public static JSONObject buildKeyValueObject(String theKey, Object theObject) {

    JSONObject resJsonObject = new JSONObject();
    resJsonObject.put(theKey, theObject);

    return resJsonObject;
  }

  /**
   * <p>
   * Build JSON Object from textgrid metadata.
   * </p>
   *
   * @param metadata
   * @return
   * @throws IOException
   * @throws ParseException
   * @throws TransformerException
   * @throws XmlPullParserException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  public JSONObject buildTextgridMetadataObject(MetadataContainerType metadata)
      throws IOException, ParseException, TransformerException, XmlPullParserException,
      SAXException, ParserConfigurationException {

    JSONObject resJsonObject = new JSONObject();

    // Put the given MetadataContainerType into a String.
    StringWriter metadataStringwriter = new StringWriter();
    JAXB.marshal(metadata, metadataStringwriter);
    String metadataString = metadataStringwriter.toString();

    // Transform to flat XML.
    String xml = transform(this.textgridMetadataTransformerTemplate, metadataString);

    String json = xml2json(xml);
    json = json.replaceAll("\"\\$\"", "\"\"");

    JSONParser parser = new JSONParser();
    Object obj2 = parser.parse(json);
    JSONObject jsonObject = (JSONObject) obj2;

    JSONObject metadataArray = (JSONObject) jsonObject.get("metadataContainerType");
    resJsonObject.putAll(metadataArray);

    String mdBase64 = Base64.encodeBase64String(metadataString.getBytes());
    resJsonObject.put(TG_ORIGINAL_METADATA_FIELDNAME, mdBase64);

    // System.out.println(resJsonObject);

    return resJsonObject;
  }

  /**
   * <p>
   * Build a DARIAH metadata object for ES import. Just assemble all existing metadata (DMD, ADMMD,
   * TECHMD, PROVMD).
   * </p>
   *
   * @param theDMD
   * @param theAdmMD
   * @param theTechMD
   * @param theProvMD
   * @return
   * @throws ParserConfigurationException
   * @throws ParseException
   * @throws SAXException
   * @throws TransformerException
   * @throws IOException
   */
  public JSONObject buildDariahMetadataObject(String theDMD, String theAdmMD, String theTechMD,
      String theProvMD) throws IOException, TransformerException, SAXException, ParseException,
      ParserConfigurationException {

    JSONObject response = new JSONObject();

    if (theDMD != null && !theDMD.equals("")) {
      response.putAll(this.buildDariahDMDObject(theDMD));
    }
    if (theAdmMD != null && !theAdmMD.equals("")) {
      response.putAll(this.buildDariahAdmMDObject(theAdmMD));
    }
    if (theTechMD != null && !theTechMD.equals("")) {
      response.putAll(this.buildDariahTechMDObject(theTechMD));
    }
    if (theProvMD != null && !theProvMD.equals("")) {
      response.putAll(this.buildDariahProvMDObject(theProvMD));
    }

    return response;
  }

  // **
  // PRIVATE
  // **

  /**
   * <p>
   * Builds an DARIAH administrative metadata JSON object from ADMMD metadata string.
   * </p>
   *
   * @param admMetadata
   * @return
   * @throws IOException
   * @throws TransformerException
   * @throws SAXException
   * @throws ParseException
   * @throws ParserConfigurationException
   */
  private JSONObject buildDariahDMDObject(String dmdMetadata) throws IOException,
      TransformerException, SAXException, ParseException, ParserConfigurationException {

    JSONObject result = new JSONObject();

    JSONParser p = new JSONParser();
    result.putAll((JSONObject) p.parse(transform(this.dariahDMDTransformerTemplate, dmdMetadata)));

    return result;
  }

  /**
   * <p>
   * Builds an DARIAH administrative metadata JSON object from ADMMD metadata string.
   * </p>
   *
   * @param admMetadata
   * @return
   * @throws IOException
   * @throws TransformerException
   * @throws SAXException
   * @throws ParseException
   * @throws ParserConfigurationException
   */
  private JSONObject buildDariahAdmMDObject(String admMetadata) throws IOException,
      TransformerException, SAXException, ParseException, ParserConfigurationException {

    JSONObject result = new JSONObject();

    JSONParser p = new JSONParser();
    result
        .putAll((JSONObject) p.parse(transform(this.dariahAdmMDTransformerTemplate, admMetadata)));

    return result;
  }

  /**
   * <p>
   * Builds an DARIAH technical metadata JSON object from TECHMD metadata string.
   * </p>
   *
   * @param techMetadata
   * @return
   * @throws IOException
   * @throws TransformerException
   * @throws SAXException
   * @throws ParseException
   * @throws ParserConfigurationException
   */
  private JSONObject buildDariahTechMDObject(String techMetadata)
      throws IOException, TransformerException, SAXException, ParserConfigurationException {

    JSONObject result = new JSONObject();

    JSONParser p = new JSONParser();
    try {
      JSONObject jsonTechMD =
          (JSONObject) p.parse(transform(this.dariahTechMDTransformerTemplate, techMetadata));
      result.putAll(jsonTechMD);
    } catch (ParseException e) {
      this.log.log(Level.WARNING, "error creating json from techmd xml due to a "
          + e.getClass().getName() + ": error on pos. " + e.getPosition() + " of techMD metadata",
          e);
      // TODO Add tech md warning to ElasticSearch if transformation fails? Or version="error"?
    }

    return result;
  }

  /**
   * <p>
   * Builds an DARIAH provenance metadata JSON object from PROVMD metadata string.
   * </p>
   *
   * @param provMetadata
   * @return
   * @throws IOException
   * @throws TransformerException
   * @throws SAXException
   * @throws ParseException
   * @throws ParserConfigurationException
   */
  private JSONObject buildDariahProvMDObject(String provMetadata) throws IOException,
      TransformerException, SAXException, ParseException, ParserConfigurationException {

    JSONObject result = new JSONObject();

    JSONParser p = new JSONParser();
    result.putAll(
        (JSONObject) p.parse(transform(this.dariahProvMDTransformerTemplate, provMetadata)));

    return result;
  }

  /**
   * @param xml
   * @return
   * @throws XmlPullParserException
   */
  public String xml2json(String xml) throws XmlPullParserException {

    XmlPullParser pparser = XmlPullParserFactory.newInstance().newPullParser();
    HierarchicalStreamReader sourceReader = new XppReader(new StringReader(xml), pparser);

    StringWriter buffer = new StringWriter();
    Configuration conf = new Configuration();
    // conf.setAttributeKey("");
    conf.setSupressAtAttributes(true);
    // conf.setWriteNullAsString(false);
    // conf.setReadNullAsEmptyString(false);
    JettisonMappedXmlDriver jettisonDriver = new JettisonMappedXmlDriver(conf);
    jettisonDriver.createWriter(buffer);
    HierarchicalStreamWriter destinationWriter = jettisonDriver.createWriter(buffer);

    HierarchicalStreamCopier copier = new HierarchicalStreamCopier();
    copier.copy(sourceReader, destinationWriter);

    return buffer.toString();
  }

  /**
   * <p>
   * Transform input stream to string using given transformer.
   * </p>
   *
   * TODO: could data be inputstream instead of string to prevent memory waste?
   *
   * @param transformerTemplate
   * @param data
   * @return
   * @throws TransformerException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  private static String transform(Templates transformerTemplate, String data)
      throws TransformerException, SAXException, ParserConfigurationException {

    // Create new reader every time we do transform.
    SAXParserFactory spf = SAXParserFactory.newInstance();
    spf.setNamespaceAware(true);
    spf.setValidating(false);
    XMLReader dtdIgnoringXmlReader;

    dtdIgnoringXmlReader = spf.newSAXParser().getXMLReader();

    SAXSource source = new SAXSource(dtdIgnoringXmlReader, new InputSource(new StringReader(data)));

    // System.out.println(" !! data: " + (data != null ? data : "NULL"));

    ByteArrayOutputStream bout = new ByteArrayOutputStream();

    // System.out.println(" !! bout: " + (bout != null ? bout.size() : "NULL"));

    Result streamResult = new StreamResult(bout);

    // System.out.println(" !! result: " + (streamResult != null ? streamResult : "NULL"));

    transformerTemplate.newTransformer().transform(source, streamResult);

    return new String(bout.toByteArray());
  }

}
