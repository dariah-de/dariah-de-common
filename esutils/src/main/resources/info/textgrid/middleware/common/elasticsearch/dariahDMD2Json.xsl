<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    exclude-result-prefixes="xs" version="2.0">

    <xsl:output media-type="text/json" method="text" encoding="UTF-8"/>

    <xsl:template name="getDcElement">
        <xsl:param name="field"/>
        <xsl:param name="fieldName"/>
        <xsl:variable name="count" select="count($field)"/>
        <xsl:if test="$field">"<xsl:value-of select="$fieldName"/>" : <xsl:choose>
                <xsl:when test="$count &gt; 1">[<xsl:for-each
                        select="$field"><xsl:variable name="fieldCheck"
                            select="following-sibling::*[1]"/>
                            "<xsl:value-of select="replace(current(), '&quot;', '\\&quot;')"/>"<xsl:if
                            test="$fieldCheck = $field">,</xsl:if>
                    </xsl:for-each> ], </xsl:when>
                <xsl:otherwise>"<xsl:value-of select="replace($field, '&quot;', '\\&quot;')"/>",
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>
    <xsl:variable name="new-line" select="'&#10;'"/>

    <xsl:template match="/">
        <xsl:variable name="step1"> { "descriptiveMetadata" : {
            <xsl:apply-templates/> } } </xsl:variable>
        <xsl:value-of select="replace($step1, ',\n\s*\}\s*\n\}', '}}')"/>
    </xsl:template>

    <xsl:template match="rdf:RDF/rdf:Description">
        <xsl:call-template name="getDcElement">
            <xsl:with-param name="field" select="dc:contributor"/>
            <xsl:with-param name="fieldName" select="'dc:contributor'"/>
        </xsl:call-template>
        <xsl:call-template name="getDcElement">
            <xsl:with-param name="field" select="dc:creator"/>
            <xsl:with-param name="fieldName" select="'dc:creator'"/>
        </xsl:call-template>
        <xsl:call-template name="getDcElement">
            <xsl:with-param name="field" select="dc:date"/>
            <xsl:with-param name="fieldName" select="'dc:date'"/>
        </xsl:call-template>
        <xsl:call-template name="getDcElement">
            <xsl:with-param name="field" select="dc:description"/>
            <xsl:with-param name="fieldName" select="'dc:description'"/>
        </xsl:call-template>
        <xsl:call-template name="getDcElement">
            <xsl:with-param name="field" select="dc:format"/>
            <xsl:with-param name="fieldName" select="'dc:format'"/>
        </xsl:call-template>
        <xsl:call-template name="getDcElement">
            <xsl:with-param name="field" select="dc:identifier"/>
            <xsl:with-param name="fieldName" select="'dc:identifier'"/>
        </xsl:call-template>
        <xsl:call-template name="getDcElement">
            <xsl:with-param name="field" select="dc:language"/>
            <xsl:with-param name="fieldName" select="'dc:language'"/>
        </xsl:call-template>
        <xsl:call-template name="getDcElement">
            <xsl:with-param name="field" select="dc:publisher"/>
            <xsl:with-param name="fieldName" select="'dc:publisher'"/>
        </xsl:call-template>
        <xsl:call-template name="getDcElement">
            <xsl:with-param name="field" select="dc:relation"/>
            <xsl:with-param name="fieldName" select="'dc:relation'"/>
        </xsl:call-template>
        <xsl:call-template name="getDcElement">
            <xsl:with-param name="field" select="dc:rights"/>
            <xsl:with-param name="fieldName" select="'dc:rights'"/>
        </xsl:call-template>
        <xsl:call-template name="getDcElement">
            <xsl:with-param name="field" select="dc:source"/>
            <xsl:with-param name="fieldName" select="'dc:source'"/>
        </xsl:call-template>
        <xsl:call-template name="getDcElement">
            <xsl:with-param name="field" select="dc:subject"/>
            <xsl:with-param name="fieldName" select="'dc:subject'"/>
        </xsl:call-template>
        <xsl:call-template name="getDcElement">
            <xsl:with-param name="field" select="dc:title"/>
            <xsl:with-param name="fieldName" select="'dc:title'"/>
        </xsl:call-template>
        <xsl:call-template name="getDcElement">
            <xsl:with-param name="field" select="dc:type"/>
            <xsl:with-param name="fieldName" select="'dc:type'"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="normalize-newline">
        <xsl:param name="str" select="''"/>
        <xsl:variable name="temp" select="concat($str, $new-line)"/>
        <xsl:variable name="head"
            select="substring-before($temp, $new-line)"/>
        <xsl:variable name="tail"
            select="substring-after($temp, $new-line)"/>
        <xsl:variable name="hasHead"
            select="translate(normalize-space($head), ' ', '') != ''"/>
        <xsl:variable name="hasTail"
            select="translate(normalize-space($tail), ' ', '') != ''"/>
        <xsl:if test="$hasHead">
            <xsl:value-of select="$head"/>
            <xsl:if test="$hasTail">
                <xsl:value-of select="$new-line"/>
            </xsl:if>
        </xsl:if>
        <xsl:if test="$hasTail">
            <xsl:call-template name="normalize-newline">
                <xsl:with-param name="str" select="$tail"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
