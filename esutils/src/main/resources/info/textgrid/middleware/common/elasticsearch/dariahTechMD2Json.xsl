<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:fits="http://hul.harvard.edu/ois/xml/ns/fits/fits_output"
    exclude-result-prefixes="xs"
    version="2.0">
<xsl:output media-type="text/json" method="text" encoding="UTF-8" />
<xsl:template match="/">
{ "technicalMetadata" : {
    	<xsl:if test="//fits:identity">
        "format" : "<xsl:value-of select="//fits:identity/@format"/>",
        "mimetype" : "<xsl:value-of select="//fits:identity/@mimetype"/>",
        </xsl:if><xsl:if test="//fits:version">
        "version" : "<xsl:value-of select="//fits:version"/>",
        </xsl:if><xsl:if test="//fits:externalIdentifier">
        "externalIdentifierType" : "<xsl:value-of select="//fits:externalIdentifier/@type"/>",
        "externalIdentifier" : "<xsl:value-of select="//fits:externalIdentifier"/>",
        </xsl:if><xsl:if test="//fits:filestatus">
        "well-formed" : "<xsl:value-of select="//fits:filestatus/fits:well-formed"/>",
        "valid" : "<xsl:value-of select="//fits:filestatus/fits:valid"/>",
        <xsl:if test="//fits:filestatus/fits:message">
        "message" : "<xsl:value-of select="//fits:filestatus/fits:message"/>",        
        </xsl:if></xsl:if>
} } 
</xsl:template>
</xsl:stylesheet>
