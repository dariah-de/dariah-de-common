<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:ns3="http://textgrid.info/namespaces/metadata/core/2010"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:eltec="https://textgridrep.org/terminology/eltec/"
    version="2.0">

    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

    <xsl:template match="/metadataContainerType/ns3:object">
        <metadataContainerType>
            <!-- generic/provided Elements -->
            <xsl:for-each select="ns3:generic/ns3:provided/ns3:title">
                <title>
                    <xsl:value-of select="current()"/>
                </title>
            </xsl:for-each>

            <xsl:for-each select="ns3:generic/ns3:provided/ns3:identifier">
                <identifier>
                    <xsl:value-of select="current()"/>
                </identifier>
            </xsl:for-each>

            <xsl:for-each select="ns3:generic/ns3:provided/ns3:format">
                <format>
                    <xsl:value-of select="current()"/>
                </format>
            </xsl:for-each>

            <xsl:for-each select="ns3:generic/ns3:provided/ns3:notes">
                <notes>
                    <xsl:value-of select="current()"/>
                </notes>
            </xsl:for-each>

            <!-- generic/generated Elements -->

            <created>
                <xsl:value-of select="ns3:generic/ns3:generated/ns3:created"/>
            </created>
            <lastModified>
                <xsl:value-of select="ns3:generic/ns3:generated/ns3:lastModified"/>
            </lastModified>

            <xsl:for-each select="ns3:generic/ns3:generated/ns3:issued">
                <issued>
                    <xsl:value-of select="current()"/>
                </issued>
            </xsl:for-each>

            <textgridUri>
                <xsl:value-of select="ns3:generic/ns3:generated/ns3:textgridUri"/>
            </textgridUri>
            <baseUri>
                <xsl:value-of
                    select="replace(ns3:generic/ns3:generated/ns3:textgridUri, '\.[^.]+$', '')"/>
            </baseUri>

            <xsl:for-each select="ns3:generic/ns3:generated/ns3:revision">
                <revision>
                    <xsl:value-of select="current()"/>
                </revision>
            </xsl:for-each>

            <xsl:for-each select="ns3:generic/ns3:generated/ns3:pid">
                <pid>
                    <value>
                        <xsl:value-of select="current()"/>
                    </value>
                    <pidType>
                        <xsl:value-of select="@pidType"/>
                    </pidType>
                </pid>
            </xsl:for-each>

            <xsl:for-each select="ns3:generic/ns3:generated/ns3:extent">
                <extent>
                    <xsl:value-of select="current()"/>
                </extent>
            </xsl:for-each>

            <xsl:for-each select="ns3:generic/ns3:generated/ns3:fixity">
                <fixity>
                    <xsl:for-each select="ns3:messageDigestAlgorithm">
                        <messageDigestAlgorithm>
                            <xsl:value-of select="current()"/>
                        </messageDigestAlgorithm>
                    </xsl:for-each>
                    <xsl:for-each select="ns3:messageDigest">
                        <messageDigest>
                            <xsl:value-of select="current()"/>
                        </messageDigest>
                    </xsl:for-each>
                    <xsl:for-each select="ns3:messageDigestOrginator">
                        <messageDigestOrginator>
                            <xsl:value-of select="current()"/>
                        </messageDigestOrginator>
                    </xsl:for-each>
                </fixity>
            </xsl:for-each>

            <xsl:for-each select="ns3:generic/ns3:generated/ns3:dataContributor">
                <dataContributor>
                    <xsl:value-of select="current()"/>
                </dataContributor>
            </xsl:for-each>

            <xsl:for-each select="ns3:generic/ns3:generated/ns3:project">
                <project>
                    <!--xsl:attribute name="id">
                        <xsl:value-of select="@id"/>
                    </xsl:attribute>
                    <xsl:attribute name="value">
                        <xsl:value-of select="current()"/>
                    </xsl:attribute-->
                    <value>
                        <xsl:value-of select="current()"/>
                    </value>
                    <id>
                        <xsl:value-of select="@id"/>
                    </id>
                </project>
            </xsl:for-each>

            <xsl:for-each select="ns3:generic/ns3:generated/ns3:warning">
                <warning>
                    <xsl:value-of select="current()"/>
                </warning>
            </xsl:for-each>

            <xsl:for-each select="ns3:generic/ns3:generated/ns3:permissions">
                <permissions>
                    <xsl:value-of select="current()"/>
                </permissions>
            </xsl:for-each>

            <xsl:for-each select="ns3:generic/ns3:generated/ns3:availability">
                <availability>
                    <xsl:value-of select="current()"/>
                </availability>
            </xsl:for-each>

            <!-- item Elementes -->

            <xsl:for-each select="ns3:item/ns3:rightsHolder">
                <item>
                    <rightsHolder>
                        <xsl:value-of select="current()"/>
                    </rightsHolder>
                </item>
            </xsl:for-each>

            <!-- edition Elements -->
            <xsl:if test="ns3:edition != ''">
                <xsl:for-each select="ns3:edition">
                    <edition>

                        <xsl:for-each select="ns3:isEditionOf">
                            <isEditionOf>
                                <xsl:value-of select="current()"/>
                            </isEditionOf>
                        </xsl:for-each>

                        <xsl:for-each select="ns3:agent">
                            <xsl:if test="current() != '' or @id != ''">
                                <agent>
                                    <role>
                                        <xsl:value-of select="@role"/>
                                    </role>
                                    <id>
                                        <xsl:value-of select="@id"/>
                                    </id>
                                    <value>
                                        <xsl:value-of select="current()"/>
                                    </value>
                                </agent>
                            </xsl:if>
                        </xsl:for-each>

                        <xsl:for-each select="ns3:formOfNotation">
                            <formOfNotation>
                                <xsl:value-of select="current()"/>
                            </formOfNotation>
                        </xsl:for-each>

                        <xsl:if test="ns3:language != ''">
                            <xsl:for-each select="ns3:language">
                                <language>
                                    <xsl:value-of select="current()"/>
                                </language>
                            </xsl:for-each>
                        </xsl:if>

                        <xsl:for-each select="ns3:license">
                            <license>
                                <!--xsl:attribute name="licenseUri">
                            <xsl:value-of select="@licenseUri"/>
                         </xsl:attribute>
                         <xsl:attribute name="value">
                            <xsl:value-of select="current()"/>
                         </xsl:attribute-->
                                <licenseUri>
                                    <xsl:value-of select="@licenseUri"/>
                                </licenseUri>
                                <value>
                                    <xsl:value-of select="current()"/>
                                </value>
                            </license>
                        </xsl:for-each>


                        <!-- edition/source/bibliographicCitation Elementes -->
                        <xsl:for-each select="ns3:source">
                            <source>
                                <xsl:for-each select="ns3:bibliographicCitation">
                                    <bibliographicCitation>

                                        <xsl:for-each select="ns3:author">
                                            <author>
                                                <xsl:for-each select="@id">
                                                    <id>
                                                      <xsl:value-of select="current()"/>
                                                    </id>
                                                </xsl:for-each>
                                                <xsl:for-each select="@isCorporateBody">
                                                    <isCorporateBody>
                                                      <xsl:value-of select="current()"/>
                                                    </isCorporateBody>
                                                </xsl:for-each>
                                                <value>
                                                    <xsl:value-of select="current()"/>
                                                </value>
                                            </author>
                                        </xsl:for-each>


                                        <xsl:for-each select="ns3:editor">
                                            <editor>
                                                <xsl:for-each select="@id">
                                                    <id>
                                                      <xsl:value-of select="current()"/>
                                                    </id>
                                                </xsl:for-each>
                                                <xsl:for-each select="@isCorporateBody">
                                                    <isCorporateBody>
                                                      <xsl:value-of select="current()"/>
                                                    </isCorporateBody>
                                                </xsl:for-each>
                                                <value>
                                                    <xsl:value-of select="current()"/>
                                                </value>
                                            </editor>
                                        </xsl:for-each>

                                        <xsl:for-each select="ns3:editionTitle">
                                            <editionTitle>
                                                <xsl:value-of select="current()"/>
                                            </editionTitle>
                                        </xsl:for-each>

                                        <xsl:for-each select="ns3:placeOfPublication/ns3:value">
                                            <placeOfPublication>
                                                <xsl:for-each select="@id">
                                                    <id>
                                                      <xsl:value-of select="current()"/>
                                                    </id>
                                                </xsl:for-each>
                                                <xsl:for-each select="@isCorporateBody">
                                                    <isCorporateBody>
                                                      <xsl:value-of select="current()"/>
                                                    </isCorporateBody>
                                                </xsl:for-each>
                                                <xsl:for-each select="current()">
                                                    <value>
                                                      <xsl:value-of select="current()"/>
                                                    </value>
                                                </xsl:for-each>
                                            </placeOfPublication>
                                        </xsl:for-each>


                                        <xsl:for-each select="ns3:publisher">
                                            <publisher>
                                                <xsl:for-each select="@id">
                                                    <id>
                                                      <xsl:value-of select="current()"/>
                                                    </id>
                                                </xsl:for-each>
                                                <xsl:for-each select="@isCorporateBody">
                                                    <isCorporateBody>
                                                      <xsl:value-of select="current()"/>
                                                    </isCorporateBody>
                                                </xsl:for-each>
                                                <value>
                                                    <xsl:value-of select="current()"/>
                                                </value>
                                            </publisher>
                                        </xsl:for-each>


                                        <xsl:for-each select="ns3:dateOfPublication">
                                            <dateOfPublication>
                                                <xsl:for-each select="@date">
                                                    <date>
                                                      <xsl:value-of select="current()"/>
                                                    </date>
                                                </xsl:for-each>
                                                <xsl:for-each select="@notBefore">
                                                    <notBefore>
                                                      <xsl:value-of select="current()"/>
                                                    </notBefore>
                                                </xsl:for-each>
                                                <xsl:for-each select="@notAfter">
                                                    <notAfter>
                                                      <xsl:value-of select="current()"/>
                                                    </notAfter>
                                                </xsl:for-each>
                                                <xsl:for-each select="current()">
                                                    <value>
                                                      <xsl:value-of select="current()"/>
                                                    </value>
                                                </xsl:for-each>
                                            </dateOfPublication>
                                        </xsl:for-each>

                                        <xsl:for-each select="ns3:editionNo">
                                            <editionNo>
                                                <xsl:value-of select="current()"/>
                                            </editionNo>
                                        </xsl:for-each>

                                        <xsl:for-each select="ns3:series">
                                            <series>
                                                <xsl:value-of select="current()"/>
                                            </series>
                                        </xsl:for-each>

                                        <xsl:for-each select="ns3:volume">
                                            <volume>
                                                <xsl:value-of select="current()"/>
                                            </volume>
                                        </xsl:for-each>
                                        <xsl:for-each select="ns3:issue">
                                            <issue>
                                                <xsl:value-of select="current()"/>
                                            </issue>
                                        </xsl:for-each>
                                        <xsl:for-each select="ns3:spage">
                                            <spage>
                                                <xsl:value-of select="current()"/>
                                            </spage>
                                        </xsl:for-each>
                                        <xsl:for-each select="ns3:epage">
                                            <epage>
                                                <xsl:value-of select="current()"/>
                                            </epage>
                                        </xsl:for-each>
                                        <xsl:for-each select="ns3:bibidentifier">
                                            <bibidentifier>
                                                <xsl:value-of select="current()"/>
                                            </bibidentifier>
                                        </xsl:for-each>

                                    </bibliographicCitation>
                                </xsl:for-each>

                                <!-- edition/source/objectCitation Elementes -->
                                <xsl:for-each select="ns3:objectCitation">
                                    <objectCitation>

                                        <xsl:for-each select="ns3:objectTitle">
                                            <objectTitle>
                                                <xsl:value-of select="current()"/>
                                            </objectTitle>
                                        </xsl:for-each>


                                        <xsl:for-each select="ns3:objectContributor">
                                            <objectContributor>
                                                <xsl:for-each select="@role">
                                                    <role>
                                                      <xsl:value-of select="current()"/>
                                                    </role>
                                                </xsl:for-each>
                                                <xsl:for-each select="@id">
                                                    <id>
                                                      <xsl:value-of select="current()"/>
                                                    </id>
                                                </xsl:for-each>
                                                <value>
                                                    <xsl:value-of select="current()"/>
                                                </value>
                                            </objectContributor>
                                        </xsl:for-each>
                                        <xsl:for-each select="ns3:objectDate">
                                            <objectDate>
                                                <xsl:for-each select="@date">
                                                    <date>
                                                      <xsl:value-of select="current()"/>
                                                    </date>
                                                </xsl:for-each>
                                                <xsl:for-each select="@notBefore">
                                                    <notBefore>
                                                      <xsl:value-of select="current()"/>
                                                    </notBefore>
                                                </xsl:for-each>
                                                <xsl:for-each select="@notAfter">
                                                    <notAfter>
                                                      <xsl:value-of select="current()"/>
                                                    </notAfter>
                                                </xsl:for-each>
                                                <value>
                                                    <xsl:value-of select="current()"/>
                                                </value>
                                            </objectDate>
                                        </xsl:for-each>



                                        <xsl:for-each select="ns3:objectIdentifier">
                                            <objectIdentifier>
                                                <type>
                                                    <xsl:value-of select="@type"/>
                                                </type>
                                                <value>
                                                    <xsl:value-of select="current()"/>
                                                </value>
                                            </objectIdentifier>
                                        </xsl:for-each>

                                    </objectCitation>
                                </xsl:for-each>
                            </source>
                        </xsl:for-each>
                    </edition>
                </xsl:for-each>
            </xsl:if>
            <!-- work Elementes -->

            <xsl:for-each select="ns3:work">
                <work>
                    <xsl:for-each select="ns3:agent">
                        <agent>
                            <xsl:for-each select="@role">
                                <role>
                                    <xsl:value-of select="current()"/>
                                </role>
                            </xsl:for-each>
                            <xsl:for-each select="@id">
                                <id>
                                    <xsl:value-of select="current()"/>
                                </id>
                            </xsl:for-each>
                            <value>
                                <xsl:value-of select="current()"/>
                            </value>
                        </agent>
                    </xsl:for-each>

                    <xsl:for-each select="ns3:abstract">
                        <abstract>
                            <xsl:value-of select="current()"/>
                        </abstract>
                    </xsl:for-each>


                    <xsl:for-each select="ns3:genre">
                        <genre>
                            <xsl:value-of select="current()"/>
                        </genre>
                    </xsl:for-each>

                    <xsl:for-each select="ns3:dateOfCreation">
                        <dateOfCreation>
                            <xsl:for-each select="current()">
                                <xsl:for-each select="@notBefore">
                                    <notBefore>
                                        <xsl:value-of select="current()"/>
                                    </notBefore>
                                </xsl:for-each>
                                <xsl:for-each select="@notAfter">
                                    <notAfter>
                                        <xsl:value-of select="current()"/>
                                    </notAfter>
                                </xsl:for-each>
                                <value>
                                    <xsl:value-of select="current()"/>
                                </value>
                            </xsl:for-each>
                        </dateOfCreation>
                    </xsl:for-each>


                    <xsl:for-each select="ns3:spatial">
                        <spatial>
                            <xsl:for-each select="ns3:id">
                                <id>
                                    <xsl:for-each select="@type">
                                        <type>
                                            <xsl:value-of select="current()"/>
                                        </type>
                                    </xsl:for-each>
                                    <value>
                                        <xsl:value-of select="current()"/>
                                    </value>
                                </id>
                            </xsl:for-each>
                            <value>
                                <xsl:value-of select="ns3:value"/>
                            </value>
                        </spatial>
                    </xsl:for-each>

                    <xsl:for-each select="ns3:temporal">
                        <temporal>
                            <xsl:for-each select="ns3:id">
                                <id>
                                    <xsl:for-each select="@type">
                                        <type>
                                            <xsl:value-of select="current()"/>
                                        </type>
                                    </xsl:for-each>
                                    <value>
                                        <xsl:value-of select="current()"/>
                                    </value>
                                </id>
                            </xsl:for-each>
                            <value>
                                <xsl:value-of select="ns3:value"/>
                            </value>
                        </temporal>
                    </xsl:for-each>


                    <xsl:for-each select="ns3:subject">
                        <subject>
                            <xsl:for-each select="ns3:id">
                                <id>
                                    <xsl:for-each select="@type">
                                        <type>
                                            <xsl:value-of select="current()"/>
                                        </type>
                                    </xsl:for-each>
                                    <value>
                                        <xsl:value-of select="current()"/>
                                    </value>
                                </id>
                            </xsl:for-each>
                            <value>
	                            <xsl:for-each select="ns3:value">
	                            	  <xsl:value-of select="current()"/>                          	 
	                            </xsl:for-each>
                            </value>                               
                        </subject>
                    </xsl:for-each>

                    <xsl:for-each select="ns3:type">
                        <type>
                            <xsl:value-of select="current()"/>
                        </type>
                    </xsl:for-each>

                    <!--
                        support own vocabularies to allow filtering for custom facts.
                        eltec namespace is bound to vocab.eltec in elasticsearch.
                        vocab.subject.gnd and vocab.subject.bk are also bound
                        here is room for more.
                    -->
                    <xsl:if test="//ns3:relations/rdf:RDF/rdf:Description/eltec:authorGender
                            or //ns3:relations/rdf:RDF/rdf:Description/eltec:timeSlot
                            or //ns3:relations/rdf:RDF/rdf:Description/eltec:size
                            or //ns3:relations/rdf:RDF/rdf:Description/eltec:reprintCount
                            or //ns3:relations/rdf:RDF/rdf:Description/eltec:corpusCollection
                            or ns3:subject">
                        <vocab>
                            <xsl:if test="//ns3:relations/rdf:RDF/rdf:Description/eltec:authorGender
                                    or //ns3:relations/rdf:RDF/rdf:Description/eltec:timeSlot
                                    or //ns3:relations/rdf:RDF/rdf:Description/eltec:size
                                    or //ns3:relations/rdf:RDF/rdf:Description/eltec:reprintCount
                                    or //ns3:relations/rdf:RDF/rdf:Description/eltec:corpusCollection">
                                <eltec>
                                    <authorGender><xsl:value-of select="//ns3:relations/rdf:RDF/rdf:Description/eltec:authorGender"/></authorGender>
                                    <timeSlot><xsl:value-of select="//ns3:relations/rdf:RDF/rdf:Description/eltec:timeSlot"/></timeSlot>
                                    <size><xsl:value-of select="//ns3:relations/rdf:RDF/rdf:Description/eltec:size"/></size>
                                    <reprintCount><xsl:value-of select="//ns3:relations/rdf:RDF/rdf:Description/eltec:reprintCount"/></reprintCount>
                                    <corpusCollection><xsl:value-of select="//ns3:relations/rdf:RDF/rdf:Description/eltec:corpusCollection"/></corpusCollection>
                                </eltec>
                            </xsl:if>
                            <xsl:if test="ns3:subject">
                                <subject>
                                    <xsl:for-each select="ns3:subject">
                                        <xsl:for-each select="ns3:id">
                                            <xsl:if test="@type eq 'http://uri.gbv.de/terminology/bk/'">
                                                <bk><xsl:value-of select="current()"/></bk>
                                            </xsl:if>
                                            <xsl:if test="@type eq 'https://d-nb.info/gnd/' or @type eq 'http://d-nb.info/gnd/'">
                                                <gnd><xsl:value-of select="current()"/></gnd>
                                            </xsl:if>
                                        </xsl:for-each>
                                    </xsl:for-each>
                                </subject>
                            </xsl:if>
                        </vocab>
                    </xsl:if>

                </work>
            </xsl:for-each>
            
            <!-- collection Elements -->
            <xsl:if test="ns3:collection != ''">
                <xsl:for-each select="ns3:collection">

                    <collection>

                        <xsl:for-each select="ns3:collector">
                            <collector>
                                <xsl:for-each select="@id">
                                    <id>
                                        <xsl:value-of select="current()"/>
                                    </id>
                                </xsl:for-each>
                                <xsl:for-each select="@isCorporateBody">
                                    <isCorporateBody>
                                        <xsl:value-of select="current()"/>
                                    </isCorporateBody>
                                </xsl:for-each>
                                <value>
                                    <xsl:value-of select="current()"/>
                                </value>
                            </collector>
                        </xsl:for-each>

                        <xsl:for-each select="ns3:abstract">
                            <abstract>
                                <xsl:value-of select="current()"/>
                            </abstract>
                        </xsl:for-each>

                        <xsl:for-each select="ns3:collectionDescription">
                            <collectionDescription>
                                <xsl:value-of select="current()"/>
                            </collectionDescription>
                        </xsl:for-each>


                        <xsl:for-each select="ns3:spatial">
                            <spatial>
                                <xsl:for-each select="ns3:id">
                                    <id>
                                        <xsl:for-each select="@type">
                                            <type>
                                                <xsl:value-of select="current()"/>
                                            </type>
                                        </xsl:for-each>
                                        <value>
                                            <xsl:value-of select="current()"/>
                                        </value>
                                    </id>
                                </xsl:for-each>
                                <value>
                                    <xsl:value-of select="ns3:value"/>
                                </value>
                            </spatial>
                        </xsl:for-each>

                        <xsl:for-each select="ns3:temporal">
                            <temporal>
                                <xsl:for-each select="ns3:id">
                                    <id>
                                        <xsl:for-each select="@type">
                                            <type>
                                                <xsl:value-of select="current()"/>
                                            </type>
                                        </xsl:for-each>
                                        <value>
                                            <xsl:value-of select="current()"/>
                                        </value>
                                    </id>
                                </xsl:for-each>
                                <value>
                                    <xsl:value-of select="ns3:value"/>
                                </value>
                            </temporal>
                        </xsl:for-each>


                        <xsl:for-each select="ns3:subject">
                            <subject>
                                <xsl:for-each select="ns3:id">
                                    <id>
                                        <xsl:for-each select="@type">
                                            <type>
                                                <xsl:value-of select="current()"/>
                                            </type>
                                        </xsl:for-each>
                                        <value>
                                            <xsl:value-of select="current()"/>
                                        </value>
                                    </id>
                                </xsl:for-each>
                                <value>
                                    <xsl:value-of select="ns3:value"/>
                                </value>
                            </subject>
                        </xsl:for-each>
                    </collection>
                </xsl:for-each>
            </xsl:if>

            <!-- relation Elements -->

            <xsl:if test="ns3:relations != ''">
            <xsl:for-each select="ns3:relations">
                <xsl:if test="ns3:isDerivedFrom != '' and ns3:isAlternativeFormatOf != '' and ns3:hasAdaptor != '' and ns3:hasSchema != ''">
                <relations>
                    <xsl:for-each select="ns3:isDerivedFrom">
                        <isDerivedFrom>
                            <xsl:value-of select="current()"/>
                        </isDerivedFrom>
                    </xsl:for-each>
                    <xsl:for-each select="ns3:isAlternativeFormatOf">
                        <isAlternativeFormatOf>
                            <xsl:value-of select="current()"/>
                        </isAlternativeFormatOf>
                    </xsl:for-each>
                    <xsl:for-each select="ns3:hasAdaptor">
                        <hasAdaptor>
                            <xsl:value-of select="current()"/>
                        </hasAdaptor>
                    </xsl:for-each>
                    <xsl:for-each select="ns3:hasSchema">
                        <hasSchema>
                            <xsl:value-of select="current()"/>
                        </hasSchema>
                    </xsl:for-each>
                </relations>
                </xsl:if>
            </xsl:for-each>
            </xsl:if>
        </metadataContainerType>
    </xsl:template>

</xsl:stylesheet>
