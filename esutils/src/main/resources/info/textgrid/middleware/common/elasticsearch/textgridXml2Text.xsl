<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs"
    version="1.0">

    <xsl:output method="text" indent="no" encoding="UTF-8"/>

    <xsl:template name="start">
        <xsl:apply-templates select="*" mode="pre"/>
    </xsl:template>

    <xsl:variable name="preprocessing">
        <xsl:call-template name="start"/>
    </xsl:variable>

    <xsl:variable name="final">
        <xsl:apply-templates select="$preprocessing" mode="main"/>
    </xsl:variable>

    <xsl:template match="@*|node()" mode="pre">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" mode="pre"/>
        </xsl:copy>
    </xsl:template>

    <!-- enter elements to omit here: -->
    <xsl:template match="tei:teiHeader" mode="pre"/>

    <xsl:template match="/">
        <xsl:sequence select="$final"/>
    </xsl:template>

    <!-- this should leave everything (non TEI) untouched -->
    <xsl:template match="/" mode="main">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>

</xsl:stylesheet>
