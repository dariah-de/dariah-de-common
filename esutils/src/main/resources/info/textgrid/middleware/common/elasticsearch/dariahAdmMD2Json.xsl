<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:dcterms="http://purl.org/dc/terms/"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output media-type="text/json" method="text" encoding="UTF-8"/>
  
    <xsl:template match="/rdf:RDF/rdf:Description">
    {
   	   	"administrativeMetadata" : {
       	  	"dcterms:extent" : <xsl:value-of select="dcterms:extent"/>,
	        "dcterms:created" : "<xsl:value-of select="dcterms:created"/>",
   	        "dcterms:creator" : "<xsl:value-of select="replace(dcterms:creator, '&quot;', '\\&quot;')"/>",
       	    "dcterms:identifier" : "<xsl:for-each select='dcterms:identifier'><xsl:variable name='id' select='.'/><xsl:if test="starts-with($id, 'hdl:')"><xsl:value-of select='.'/></xsl:if></xsl:for-each>",
       	    "datacite:identifier" : "<xsl:for-each select='dcterms:identifier'><xsl:variable name='id' select='.'/><xsl:if test="starts-with($id, 'doi:')"><xsl:value-of select='.'/></xsl:if></xsl:for-each>",
   	    	"dcterms:modified" : "<xsl:value-of select="dcterms:modified"/>",
   	    	"dcterms:relation" : "<xsl:value-of select="dcterms:relation"/>",
   	    	"dcterms:format" : "<xsl:value-of select="dcterms:format"/>"
   		}
    }
    </xsl:template>    
</xsl:stylesheet>
