/**
 * This software is copyright (c) 2021 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk, SUB Göttingen
 */

package info.textgrid.middleware.common.elasticsearch;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import org.apache.commons.io.IOUtils;
import org.apache.http.auth.AuthenticationException;
import org.apache.jena.rdf.model.Model;
import org.codehaus.jettison.json.JSONException;
import org.json.simple.parser.ParseException;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParserException;
import info.textgrid.DHTransform;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;

/**
 *
 */
public class TestDHTransform {

  // **
  // FINALS
  // **

  private static final String EXPECTED_DMD_RDF = "<rdf:RDF\n" +
      "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
      "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n" +
      "    xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\">\n" +
      "  <dariah:DataObject rdf:about=\"http://hdl.handle.net/21.11113/0000-000B-C901-1\">\n" +
      "    <dc:title>Universal-Kirchenzeitung-XML_021.xml</dc:title>\n" +
      "    <dc:rights>CC BY</dc:rights>\n" +
      "    <dc:relation rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.11113/0000-000B-C8F2-2\"/>\n"
      +
      "    <dc:identifier rdf:resource=\"http://dx.doi.org/10.20375/0000-000B-C901-1\"/>\n" +
      "    <dc:identifier rdf:resource=\"http://hdl.handle.net/21.11113/0000-000B-C901-1\"/>\n" +
      "    <dc:format>text/xml</dc:format>\n" +
      "    <dc:creator>Mache, Beata</dc:creator>\n" +
      "  </dariah:DataObject>\n" +
      "</rdf:RDF>";
  private static final String EXPECTED_DMD4ES = "<rdf:RDF\n" +
      "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n" +
      "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n" +
      "  <rdf:Description rdf:about=\"http://hdl.handle.net/21.11113/0000-000B-C901-1\">\n" +
      "    <dc:creator>Mache, Beata</dc:creator>\n" +
      "    <dc:format>text/xml</dc:format>\n" +
      "    <dc:identifier>hdl:21.11113/0000-000B-C901-1</dc:identifier>\n" +
      "    <dc:identifier>doi:10.20375/0000-000B-C901-1</dc:identifier>\n" +
      "    <dc:relation>hdl:21.11113/0000-000B-C8F2-2</dc:relation>\n" +
      "    <dc:rights>CC BY</dc:rights>\n" +
      "    <dc:title>Universal-Kirchenzeitung-XML_021.xml</dc:title>\n" +
      "  </rdf:Description>\n" +
      "</rdf:RDF>";
  private static final String EXPECTED_ADMMD_RDF = "<rdf:RDF\n" +
      "    xmlns:dcterms=\"http://purl.org/dc/terms/\"\n" +
      "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
      "    xmlns:premis=\"http://www.loc.gov/premis/rdf/v1#\"\n" +
      "    xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\">\n" +
      "  <dariah:DataObject rdf:about=\"http://hdl.handle.net/21.11113/0000-000B-C901-1\">\n" +
      "    <dcterms:modified>2017-12-05T13:52:32.677</dcterms:modified>\n" +
      "    <premis:hasMessageDigestType>md5</premis:hasMessageDigestType>\n" +
      "    <dcterms:creator>BeataMache@dariah.eu</dcterms:creator>\n" +
      "    <premis:hasMessageDigestOriginator>dhcrud-base-7.36.0-DH.201712041055</premis:hasMessageDigestOriginator>\n"
      +
      "    <dcterms:format>text/xml</dcterms:format>\n" +
      "    <dcterms:extent>104525</dcterms:extent>\n" +
      "    <dcterms:relation rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.11113/0000-000B-C8F2-2\"/>\n"
      +
      "    <premis:hasMessageDigest>4b4e7ab8207f0046f2b9dc7ab8906fe3</premis:hasMessageDigest>\n" +
      "    <dcterms:created>2017-12-05T13:52:32.677</dcterms:created>\n" +
      "    <dcterms:identifier rdf:resource=\"http://hdl.handle.net/21.11113/0000-000B-C901-1\"/>\n"
      +
      "    <dcterms:identifier rdf:resource=\"http://dx.doi.org/10.20375/0000-000B-C901-1\"/>\n" +
      "  </dariah:DataObject>\n" +
      "</rdf:RDF>";
  private static final String EXPECTED_ADMMD4ES = "<rdf:RDF\n" +
      "    xmlns:dcterms=\"http://purl.org/dc/terms/\"\n" +
      "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
      "    xmlns:premis=\"http://www.loc.gov/premis/rdf/v1#\">\n" +
      "  <rdf:Description rdf:about=\"http://hdl.handle.net/21.11113/0000-000B-C901-1\">\n" +
      "    <dcterms:modified>2017-12-05T13:52:32.677</dcterms:modified>\n" +
      "    <dcterms:relation>hdl:21.11113/0000-000B-C8F2-2</dcterms:relation>\n" +
      "    <premis:hasMessageDigestType>md5</premis:hasMessageDigestType>\n" +
      "    <dcterms:identifier>hdl:21.11113/0000-000B-C901-1</dcterms:identifier>\n" +
      "    <dcterms:creator>BeataMache@dariah.eu</dcterms:creator>\n" +
      "    <premis:hasMessageDigestOriginator>dhcrud-base-7.36.0-DH.201712041055</premis:hasMessageDigestOriginator>\n"
      +
      "    <dcterms:format>text/xml</dcterms:format>\n" +
      "    <dcterms:identifier>doi:10.20375/0000-000B-C901-1</dcterms:identifier>\n" +
      "    <dcterms:extent>104525</dcterms:extent>\n" +
      "    <premis:hasMessageDigest>4b4e7ab8207f0046f2b9dc7ab8906fe3</premis:hasMessageDigest>\n" +
      "    <dcterms:created>2017-12-05T13:52:32.677</dcterms:created>\n" +
      "  </rdf:Description>\n" +
      "</rdf:RDF>";
  private static final String EXPECTED_ADMMD4ES_32791 = "<rdf:RDF\n"
      + "    xmlns:dcterms=\"http://purl.org/dc/terms/\"\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "    xmlns:premis=\"http://www.loc.gov/premis/rdf/v1#\">\n"
      + "  <rdf:Description rdf:about=\"http://hdl.handle.net/21.T11991/0000-001A-6BD5-C\">\n"
      + "    <premis:hasMessageDigest>209982c73eec1484b40649a8dd8533b8</premis:hasMessageDigest>\n"
      + "    <dcterms:format>text/vnd.dariah.dhrep.collection+turtle</dcterms:format>\n"
      + "    <premis:hasMessageDigestOriginator>dhcrud-base-10.2.5.6-SNAPSHOT+202006091350</premis:hasMessageDigestOriginator>\n"
      + "    <dcterms:source>https://cdstar.de.dariah.eu/test/dariah/EAEA0-01DC-E399-0F82-0</dcterms:source>\n"
      + "    <premis:hasMessageDigestType>md5</premis:hasMessageDigestType>\n"
      + "    <dcterms:created>2020-06-09T13:57:25.164</dcterms:created>\n"
      + "    <dcterms:identifier>doi:10.20375/0000-001A-6BD5-C</dcterms:identifier>\n"
      + "    <dcterms:creator>StefanFunk@dariah.eu</dcterms:creator>\n"
      + "    <dcterms:extent>388</dcterms:extent>\n"
      + "    <dcterms:identifier>hdl:21.T11991/0000-001A-6BD5-C</dcterms:identifier>\n"
      + "    <dcterms:modified>2020-06-09T13:57:25.164</dcterms:modified>\n"
      + "  </rdf:Description>\n" + "</rdf:RDF>";
  private static final String EXPECTED_JSON_STRING =
      "{\"descriptiveMetadata\":{\"dc:relation\":\"hdl:21.11113\\/0000-000B-C8F2-2\",\"dc:title\":\"Universal-Kirchenzeitung-XML_021.xml\",\"dc:creator\":\"Mache, Beata\",\"dc:format\":\"text\\/xml\",\"dc:rights\":\"CC BY\",\"dc:identifier\":[\"hdl:21.11113\\/0000-000B-C901-1\",\"doi:10.20375\\/0000-000B-C901-1\"]},\"administrativeMetadata\":{\"datacite:identifier\":\"doi:10.20375\\/0000-000B-C901-1\",\"dcterms:identifier\":\"hdl:21.11113\\/0000-000B-C901-1\",\"dcterms:extent\":104525,\"dcterms:creator\":\"BeataMache@dariah.eu\",\"dcterms:format\":\"text\\/xml\",\"dcterms:created\":\"2017-12-05T13:52:32.677\",\"dcterms:relation\":\"hdl:21.11113\\/0000-000B-C8F2-2\",\"dcterms:modified\":\"2017-12-05T13:52:32.677\"},\"technicalMetadata\":{\"valid\":\"false\",\"externalIdentifierType\":\"puid\",\"format\":\"Extensible Markup Language\",\"externalIdentifier\":\"fmt\\/101\",\"mimetype\":\"text\\/xml\",\"well-formed\":\"true\",\"message\":\"cvc-elt.1: Cannot find the declaration of element 'TEI'. Line = 5, Column = 42\",\"version\":\"1.0\"}}";
  private static final String EXPECTED_JSON_STRING_359 =
      "{\"descriptiveMetadata\":{\"dc:relation\":\"hdl:21.11113\\/0000-0013-BE18-B\",\"dc:title\":\"kremlin.ru_ru_2024_posts_by_location.html\",\"dc:creator\":\"Giorgio Comai\",\"dc:format\":\"text\\/plain\",\"dc:rights\":\"Attribution: Open Data Commons Attribution License (ODC-By) v1.0\",\"dc:identifier\":[\"doi:10.20375\\/0000-0013-BE2C-5\",\"hdl:21.11113\\/0000-0013-BE2C-5\"]},\"administrativeMetadata\":{\"datacite:identifier\":\"doi:10.20375\\/0000-0013-BE2C-5\",\"dcterms:identifier\":\"hdl:21.11113\\/0000-0013-BE2C-5\",\"dcterms:extent\":4437202,\"dcterms:creator\":\"giocomai@dariah.eu\",\"dcterms:format\":\"text\\/plain\",\"dcterms:created\":\"2024-10-21T12:56:41.964+02:00\",\"dcterms:relation\":\"hdl:21.11113\\/0000-0013-BE18-B\",\"dcterms:modified\":\"2024-10-21T12:56:41.964+02:00\"}}";

  /**
   * <p>
   * Tests pass 1 of the DHTransform class: Download all BAG's metadata and store it to ./dhrep/
   * folder.
   * </p>
   * 
   * NOTE This is an online test. Please Ignore if not needed!
   * 
   * @throws ParserConfigurationException
   * @throws XmlPullParserException
   * @throws ParseException
   * @throws IOException
   * @throws SAXException
   * @throws AuthenticationException
   * @throws JSONException
   * @throws UnsupportedOperationException
   * @throws TransformerException
   * @throws DatatypeConfigurationException
   * @throws XMLStreamException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  @Ignore
  public void testDHTransformPass1() throws SAXException,
      IOException, ParseException, XmlPullParserException, ParserConfigurationException,
      AuthenticationException, UnsupportedOperationException, JSONException, TransformerException,
      XMLStreamException, DatatypeConfigurationException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    String args[] = {"1"};
    DHTransform.main(args);

    // TODO Please check test outcome manually :-)
  }

  /**
   * @throws SAXException
   * @throws IOException
   * @throws ParseException
   * @throws XmlPullParserException
   * @throws ParserConfigurationException
   * @throws AuthenticationException
   * @throws UnsupportedOperationException
   * @throws JSONException
   * @throws TransformerException
   * @throws DatatypeConfigurationException
   * @throws XMLStreamException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  @Ignore
  public void testDHTransformPass2() throws SAXException,
      IOException, ParseException, XmlPullParserException, ParserConfigurationException,
      AuthenticationException, UnsupportedOperationException, JSONException, TransformerException,
      XMLStreamException, DatatypeConfigurationException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    String args[] = {"2"};
    DHTransform.main(args);

    // TODO Please check test outcome manually :-)
  }

  /**
   * @throws SAXException
   * @throws IOException
   * @throws ParseException
   * @throws XmlPullParserException
   * @throws ParserConfigurationException
   * @throws AuthenticationException
   * @throws UnsupportedOperationException
   * @throws JSONException
   * @throws TransformerException
   * @throws DatatypeConfigurationException
   * @throws XMLStreamException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  @Ignore
  public void testDHTransformPass3() throws SAXException,
      IOException, ParseException, XmlPullParserException, ParserConfigurationException,
      AuthenticationException, UnsupportedOperationException, JSONException, TransformerException,
      XMLStreamException, DatatypeConfigurationException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    String args[] = {"3"};
    DHTransform.main(args);

    // TODO Please check test outcome manually :-)
  }

  /**
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws ParseException
   * @throws TransformerException
   * @throws IOException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   * 
   */
  @Test
  public void testRDFTransformationDMD()
      throws IOException, org.apache.jena.riot.lang.extra.javacc.ParseException {

    String dmd =
        IOUtils.toString(new FileInputStream(
            new File("src/test/resources/21.11113_0000-000B-C901-1/01_metadata.ttl")), "UTF-8");
    String transformedRDF = getRDF(dmd);

    if (!EXPECTED_DMD_RDF.equals(transformedRDF.trim())) {

      System.out.println(transformedRDF);
      System.out.println("!=");
      System.out.println(EXPECTED_DMD_RDF);

      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testRDFTransformationADMMD()
      throws IOException, org.apache.jena.riot.lang.extra.javacc.ParseException {

    String admmd = IOUtils.toString(
        new FileInputStream(new File("src/test/resources/21.11113_0000-000B-C901-1/02_adm.ttl")),
        "UTF-8");
    String transformedRDF = getRDF(admmd);

    if (!EXPECTED_ADMMD_RDF.equals(transformedRDF.trim())) {

      System.out.println(transformedRDF);
      System.out.println("!=");
      System.out.println(EXPECTED_ADMMD_RDF);

      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws DatatypeConfigurationException
   * @throws XMLStreamException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testDMDCreation4ElasticSearch() throws FileNotFoundException, IOException,
      XMLStreamException, DatatypeConfigurationException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    File ttlDMDFile = new File("src/test/resources/21.11113_0000-000B-C901-1/01_metadata.ttl");
    URI uri = URI
        .create(DHTransform.HDL_URL + ttlDMDFile.getParentFile().getName().replaceFirst("_", "/"));

    Model dmdOrig = RDFUtils.readModel(IOUtils.toString(new FileInputStream(ttlDMDFile), "UTF-8"),
        RDFConstants.TURTLE);
    Model dmd4ES = RDFUtils.prepareDMDforJSONCreation(uri, dmdOrig);
    Model expectedDMD = RDFUtils.readModel(EXPECTED_DMD4ES);
    if (!dmd4ES.isIsomorphicWith(expectedDMD)) {
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws DatatypeConfigurationException
   * @throws XMLStreamException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testADMMDCreation4ElasticSearch() throws FileNotFoundException, IOException,
      XMLStreamException, DatatypeConfigurationException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    File ttlADMMDFile = new File("src/test/resources/21.11113_0000-000B-C901-1/02_adm.ttl");
    URI uri = URI.create(
        DHTransform.HDL_URL + ttlADMMDFile.getParentFile().getName().replaceFirst("_", "/"));

    Model admmdOrig = RDFUtils.readModel(
        IOUtils.toString(new FileInputStream(ttlADMMDFile), "UTF-8"), RDFConstants.TURTLE);
    Model admmd4ES = RDFUtils.prepareADMMDforJSONCreation(uri, admmdOrig);
    Model expectedADMMD = RDFUtils.readModel(EXPECTED_ADMMD4ES);
    if (!admmd4ES.isIsomorphicWith(expectedADMMD)) {
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws DatatypeConfigurationException
   * @throws XMLStreamException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testADMMDCreation4ElasticSearchBug32791() throws FileNotFoundException, IOException,
      XMLStreamException, DatatypeConfigurationException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    File ttlADMMDFile = new File("src/test/resources/bug#32791.ttl");
    URI uri = URI.create("http://hdl.handle.net/21.T11991/0000-001A-6BD5-C");

    Model admmdOrig = RDFUtils.readModel(
        IOUtils.toString(new FileInputStream(ttlADMMDFile), "UTF-8"), RDFConstants.TURTLE);
    Model admmd4ES = RDFUtils.prepareADMMDforJSONCreation(uri, admmdOrig);
    Model expectedADMMD = RDFUtils.readModel(EXPECTED_ADMMD4ES_32791);
    if (!admmd4ES.isIsomorphicWith(expectedADMMD)) {
      assertTrue(false);
    }
  }

  /**
   * @throws DatatypeConfigurationException
   * @throws XMLStreamException
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ParseException
   * @throws TransformerException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testGetJSON()
      throws FileNotFoundException, IOException, XMLStreamException, DatatypeConfigurationException,
      SAXException, ParserConfigurationException, TransformerException, ParseException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    File parent = new File("src/test/resources/21.11113_0000-000B-C901-1");
    URI prefixUri = URI.create("" + parent.getName().replaceAll("_", "/"));

    File ttlDMDFile = new File("src/test/resources/21.11113_0000-000B-C901-1/01_metadata.ttl");
    File ttlADMMDFile = new File("src/test/resources/21.11113_0000-000B-C901-1/02_adm.ttl");
    File xmlTECHMDFile = new File("src/test/resources/21.11113_0000-000B-C901-1/03_tech.xml");

    String dmd = DHTransform.getPreparedStringFromUnpreparedDMDModelFile(prefixUri, ttlDMDFile);
    String admmd =
        DHTransform.getPreparedStringFromUnpreparedADMMDModelFile(prefixUri, ttlADMMDFile);
    String techmd = IOUtils.toString(new FileInputStream(xmlTECHMDFile), "UTF-8");

    ESJsonBuilder eb = new ESJsonBuilder();
    org.json.simple.JSONObject json = eb.buildDariahMetadataObject(dmd, admmd, techmd, "");

    if (!json.toJSONString().trim().equals(EXPECTED_JSON_STRING)) {
      System.out.println("JSON CREATED:  " + json.toJSONString().trim());
      System.out.println("JSON EXPECTED: " + EXPECTED_JSON_STRING);
      assertTrue(false);
    }
  }

  /**
   * @throws FileNotFoundException
   * @throws IOException
   * @throws XMLStreamException
   * @throws DatatypeConfigurationException
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws TransformerException
   * @throws ParseException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testGetJSONBug359()
      throws FileNotFoundException, IOException, XMLStreamException, DatatypeConfigurationException,
      SAXException, ParserConfigurationException, TransformerException, ParseException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    File parent = new File("src/test/resources/21.11113_0000-0013-BE2C-5");
    URI prefixUri = URI.create("" + parent.getName().replaceAll("_", "/"));

    File ttlDMDFile = new File("src/test/resources/21.11113_0000-0013-BE2C-5/01_metadata.ttl");
    File ttlADMMDFile = new File("src/test/resources/21.11113_0000-0013-BE2C-5/02_adm.ttl");
    File xmlTECHMDFile = new File("src/test/resources/21.11113_0000-0013-BE2C-5/03_tech.xml");

    String dmd = DHTransform.getPreparedStringFromUnpreparedDMDModelFile(prefixUri, ttlDMDFile);
    String admmd =
        DHTransform.getPreparedStringFromUnpreparedADMMDModelFile(prefixUri, ttlADMMDFile);
    String techmd = IOUtils.toString(new FileInputStream(xmlTECHMDFile), "UTF-8");

    ESJsonBuilder eb = new ESJsonBuilder();
    org.json.simple.JSONObject json = eb.buildDariahMetadataObject(dmd, admmd, techmd, "");

    if (!json.toJSONString().trim().equals(EXPECTED_JSON_STRING_359)) {
      System.out.println("JSON CREATED:  " + json.toJSONString().trim());
      System.out.println("JSON EXPECTED: " + EXPECTED_JSON_STRING_359);
      assertTrue(false);
    }
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param theTTL
   * @return
   * @throws IOException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  private static String getRDF(final String theTTL)
      throws IOException, org.apache.jena.riot.lang.extra.javacc.ParseException {

    String result = "";

    Model model = RDFUtils.readModel(theTTL, RDFConstants.TURTLE);
    Writer writer = new StringWriter();
    model.write(writer, RDFConstants.RDF_XML);
    writer.close();
    result = writer.toString();
    model.close();

    return result;
  }

}
