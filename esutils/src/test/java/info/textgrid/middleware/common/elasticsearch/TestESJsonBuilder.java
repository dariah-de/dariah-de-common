/**
 * This software is copyright (c) 2023 by
 *
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Ubbo Veentjer, SUB Göttingen
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */

package info.textgrid.middleware.common.elasticsearch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParserException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import jakarta.xml.bind.JAXB;

/**
 *
 */
public class TestESJsonBuilder {

  private static final String UTF8 = "UTF-8";

  private static ESJsonBuilder esj;
  protected static boolean error = false;
  private static final boolean XML = true;

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    esj = new ESJsonBuilder();
  }

  /**
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws IOException
   * @throws TransformerException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  @Test
  public void testTGXml2Fulltext() throws IOException, TransformerException,
      SAXException, ParserConfigurationException {
    JSONObject res = esj
        .buildFulltextDataObject(new ByteArrayInputStream("<test>blubb</test>".getBytes()), true);
    assertEquals("{\"fulltext\":\"blubb\"}", res.toString());
  }

  /**
   * @throws IOException
   * @throws TransformerException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  @Test
  public void testTGPlaintext2Fulltext()
      throws IOException, TransformerException, SAXException, ParserConfigurationException {
    JSONObject res =
        esj.buildFulltextDataObject(new ByteArrayInputStream("blubb 123".getBytes()), false);
    assertEquals("{\"fulltext\":\"blubb 123\"}", res.toString());
  }

  /**
   * @throws IOException
   * @throws TransformerException
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws ParseException
   */
  @Test
  public void testDHMetadata2Json() throws IOException, TransformerException, SAXException,
      ParseException, ParserConfigurationException {

    String meta = "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\""
        + " xmlns:dc=\"http://purl.org/dc/elements/1.1/\""
        + " xmlns:dcterms=\"http://purl.org/dc/terms/\""
        + " xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\" >"
        + "<rdf:Description rdf:about=\"hdl:11022/0000-0000-8C9D-2\">"
        + "<dc:identifier>hdl:11022/0000-0000-8C9D-2</dc:identifier>"
        + "<dc:source>http://geobrowser.de.dariah.eu/storage/245752</dc:source>"
        + "<dc:format>text/tg.collection+tg.aggregation+xml</dc:format>"
        + "<rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection\"/>"
        + "<dc:contributor>\"fu\"</dc:contributor>"
        + "<dc:contributor>\"fa\"</dc:contributor>"
        + "<dc:creator>The \"Wittgenstein Archives\" at the University of Bergen (WAB)</dc:creator>"
        + "<dc:description>Wittgenstein Source Bergen Text Edition (BTE) provides access to transcriptions and editions of the following items of the Wittgenstein Nachlass</dc:description>"
        + "<dc:title>Wittgenstein Source: Bergen Text Edition</dc:title>"
        + "</rdf:Description>" + "</rdf:RDF>";
    String admMd = "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\""
        + " xmlns:dcterms=\"http://purl.org/dc/terms/\""
        + " xmlns:premis=\"http://www.loc.gov/premis/rdf/v1#\" >"
        + "<rdf:Description rdf:about=\"hdl:11022/0000-0000-8C9D-2\">"
        + "<dcterms:extent>277</dcterms:extent>"
        + "<dcterms:created>2015-10-21T17:08:28.718</dcterms:created>"
        + "<dcterms:identifier>hdl:11022/0000-0000-8C9D-2</dcterms:identifier>"
        + "<dcterms:creator>StefanFunk@dariah.eu</dcterms:creator>"
        + "<dcterms:format></dcterms:format>"
        + "<dcterms:modified>2015-10-21T17:08:28.718</dcterms:modified>"
        + "</rdf:Description>" + "</rdf:RDF>";
    String techMd = "";
    String provMd = "";
    String expected =
        "{\"descriptiveMetadata\":{\"dc:description\":\"Wittgenstein Source Bergen Text Edition (BTE) provides access to transcriptions and editions of the following items of the Wittgenstein Nachlass\",\"dc:contributor\":[\"\\\"fu\\\"\",\"\\\"fa\\\"\"],\"dc:title\":\"Wittgenstein Source: Bergen Text Edition\",\"dc:creator\":\"The \\\"Wittgenstein Archives\\\" at the University of Bergen (WAB)\",\"dc:format\":\"text\\/tg.collection+tg.aggregation+xml\",\"dc:identifier\":\"hdl:11022\\/0000-0000-8C9D-2\",\"dc:source\":\"http:\\/\\/geobrowser.de.dariah.eu\\/storage\\/245752\"},\"administrativeMetadata\":{\"datacite:identifier\":\"\",\"dcterms:identifier\":\"hdl:11022\\/0000-0000-8C9D-2\",\"dcterms:extent\":277,\"dcterms:creator\":\"StefanFunk@dariah.eu\",\"dcterms:format\":\"\",\"dcterms:created\":\"2015-10-21T17:08:28.718\",\"dcterms:relation\":\"\",\"dcterms:modified\":\"2015-10-21T17:08:28.718\"}}";

    String result = esj.buildDariahMetadataObject(meta, admMd, techMd, provMd).toJSONString();

    if (!result.trim().equals(expected.trim())) {
      System.out.println("!transformed:\n" + result);
      System.out.println("!expected:\n" + expected);
      assertTrue(false);
    }
  }

  /**
   * @throws ParserConfigurationException
   * @throws ParseException
   * @throws SAXException
   * @throws TransformerException
   * @throws IOException
   */
  @Test
  public void testDHMetadata2JsonTwoIdentifiersInAdmMd() throws IOException, TransformerException,
      SAXException, ParseException, ParserConfigurationException {

    String meta = "<rdf:RDF\n"
        + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
        + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n"
        + "  <rdf:Description rdf:about=\"http://hdl.handle.net/21.T11991/0000-0002-6095-9\">\n"
        + "    <dc:identifier>hdl:21.T11991/0000-0002-6095-9</dc:identifier>\n"
        + "    <dc:date>2015-06-20T16:54:35</dc:date>\n"
        + "    <dc:rights>free</dc:rights>\n"
        + "    <dc:relation>hdl:21.T11991/0000-0002-608D-3</dc:relation>\n"
        + "    <dc:identifier>doi:10.20375/0000-0002-6095-9</dc:identifier>\n"
        + "    <dc:format>image/jpeg</dc:format>\n"
        + "    <dc:creator>fu</dc:creator>\n"
        + "    <dc:description>dfkgjhsdfkghsf</dc:description>\n"
        + "    <dc:title>IMG_0133.jpg</dc:title>\n"
        + "    <dc:identifier>http://box.dariah.local/1.0/dhcrud/21.T11991/0000-0002-6095-9/index</dc:identifier>\n"
        + "  </rdf:Description>\n" + "</rdf:RDF>";
    String admMd = "<rdf:RDF\n"
        + "    xmlns:dcterms=\"http://purl.org/dc/terms/\"\n"
        + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
        + "    xmlns:premis=\"http://www.loc.gov/premis/rdf/v1#\">\n"
        + "  <rdf:Description rdf:about=\"http://hdl.handle.net/21.T11991/0000-0002-6095-9\">\n"
        + "    <premis:hasMessageDigestType>md5</premis:hasMessageDigestType>\n"
        + "    <premis:hasMessageDigestOriginator>dhcrud-base 7.15.4-SNAPSHOT.201709081238</premis:hasMessageDigestOriginator>\n"
        + "    <dcterms:relation>hdl:21.T11991/0000-0002-608D-3</dcterms:relation>\n"
        + "    <dcterms:identifier>hdl:21.T11991/0000-0002-6095-9</dcterms:identifier>\n"
        + "    <dcterms:creator>StefanFunk@dariah.eu</dcterms:creator>\n"
        + "    <dcterms:extent>779741</dcterms:extent>\n"
        + "    <dcterms:modified>2017-09-08T13:00:21.254</dcterms:modified>\n"
        + "    <dcterms:created>2017-09-08T13:00:21.254</dcterms:created>\n"
        + "    <dcterms:identifier>doi:10.20375/0000-0002-6095-9</dcterms:identifier>\n"
        + "    <dcterms:format>image/jpeg</dcterms:format>\n"
        + "    <premis:hasMessageDigest>012b3599658b27f50ec5f234f3e537d8</premis:hasMessageDigest>\n"
        + "  </rdf:Description>\n" + "</rdf:RDF>";
    String techMd = "";
    String provMd = "";
    String expected =
        "{\"descriptiveMetadata\":{\"dc:description\":\"dfkgjhsdfkghsf\",\"dc:date\":\"2015-06-20T16:54:35\",\"dc:relation\":\"hdl:21.T11991\\/0000-0002-608D-3\",\"dc:title\":\"IMG_0133.jpg\",\"dc:creator\":\"fu\",\"dc:format\":\"image\\/jpeg\",\"dc:rights\":\"free\",\"dc:identifier\":[\"hdl:21.T11991\\/0000-0002-6095-9\",\"doi:10.20375\\/0000-0002-6095-9\",\"http:\\/\\/box.dariah.local\\/1.0\\/dhcrud\\/21.T11991\\/0000-0002-6095-9\\/index\"]},\"administrativeMetadata\":{\"datacite:identifier\":\"doi:10.20375\\/0000-0002-6095-9\",\"dcterms:identifier\":\"hdl:21.T11991\\/0000-0002-6095-9\",\"dcterms:extent\":779741,\"dcterms:creator\":\"StefanFunk@dariah.eu\",\"dcterms:format\":\"image\\/jpeg\",\"dcterms:created\":\"2017-09-08T13:00:21.254\",\"dcterms:relation\":\"hdl:21.T11991\\/0000-0002-608D-3\",\"dcterms:modified\":\"2017-09-08T13:00:21.254\"}}";

    String result = esj
        .buildDariahMetadataObject(meta, admMd, techMd, provMd)
        .toJSONString();

    if (!result.trim().equals(expected.trim())) {
      System.out.println("!transformed:\n" + result);
      System.out.println("!expected:\n" + expected);
      assertTrue(false);
    }
  }

  /**
   * @throws ParserConfigurationException
   * @throws ParseException
   * @throws SAXException
   * @throws TransformerException
   * @throws IOException
   */
  @Test
  public void testJSONCreationWithTechMD() throws IOException, TransformerException, SAXException,
      ParseException, ParserConfigurationException {

    String meta = "<rdf:RDF\n"
        + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
        + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n"
        + "  <rdf:Description rdf:about=\"http://hdl.handle.net/21.T11991/0000-0003-EDD3-4\">\n"
        + "    <dc:rights>free</dc:rights>\n"
        + "    <dc:format>text/vnd.dariah.dhrep.collection+turtle</dc:format>\n"
        + "    <dc:identifier>http://box.dariah.local/1.0/dhcrud/21.T11991/0000-0003-EDD3-4/index</dc:identifier>\n"
        + "    <dc:creator>fu</dc:creator>\n"
        + "    <dc:title>_make more collections_</dc:title>\n"
        + "    <dc:identifier>doi:10.20375/0000-0003-EDD3-4</dc:identifier>\n"
        + "    <dc:identifier>hdl:21.T11991/0000-0003-EDD3-4</dc:identifier>\n"
        + "    <dc:identifier>https://hdl.handle.net/21.T11991/0000-0003-EDD3-4@index</dc:identifier>\n"
        + "  </rdf:Description>\n" + "</rdf:RDF>";
    String admMd = "<rdf:RDF\n"
        + "    xmlns:dcterms=\"http://purl.org/dc/terms/\"\n"
        + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
        + "    xmlns:premis=\"http://www.loc.gov/premis/rdf/v1#\">\n"
        + "  <rdf:Description rdf:about=\"http://hdl.handle.net/21.T11991/0000-0003-EDD3-4\">\n"
        + "    <dcterms:source>https://cdstar.de.dariah.eu/test/dariah/EAEA0-2ED3-D591-C35A-0</dcterms:source>\n"
        + "    <dcterms:creator>StefanFunk@dariah.eu</dcterms:creator>\n"
        + "    <dcterms:extent>437</dcterms:extent>\n"
        + "    <dcterms:identifier>hdl:21.T11991/0000-0003-EDD3-4</dcterms:identifier>\n"
        + "    <premis:hasMessageDigest>db99f8621b020c521eab5acbb9c5e4e3</premis:hasMessageDigest>\n"
        + "    <premis:hasMessageDigestType>md5</premis:hasMessageDigestType>\n"
        + "    <dcterms:identifier>doi:10.20375/0000-0003-EDD3-4</dcterms:identifier>\n"
        + "    <dcterms:created>2017-10-20T15:19:25.537</dcterms:created>\n"
        + "    <dcterms:format>text/vnd.dariah.dhrep.collection+turtle</dcterms:format>\n"
        + "    <premis:hasMessageDigestOriginator>dhcrud-base-7.21.3-SNAPSHOT.201710201513</premis:hasMessageDigestOriginator>\n"
        + "    <dcterms:modified>2017-10-20T15:19:25.537</dcterms:modified>\n"
        + "  </rdf:Description>\n" + "</rdf:RDF>";
    String techMd = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
        + "<fits xmlns=\"http://hul.harvard.edu/ois/xml/ns/fits/fits_output\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://hul.harvard.edu/ois/xml/ns/fits/fits_output http://hul.harvard.edu/ois/xml/xsd/fits/fits_output.xsd\" version=\"1.1.0\" timestamp=\"10/20/17 3:19 PM\">\n"
        + "  <identification>\n"
        + "    <identity format=\"Plain text\" mimetype=\"text/plain\" toolname=\"FITS\" toolversion=\"1.1.0\">\n"
        + "      <tool toolname=\"Droid\" toolversion=\"6.1.5\" />\n"
        + "      <tool toolname=\"Jhove\" toolversion=\"1.16\" />\n"
        + "      <tool toolname=\"file utility\" toolversion=\"5.14\" />\n"
        + "      <externalIdentifier toolname=\"Droid\" toolversion=\"6.1.5\" type=\"puid\">x-fmt/111</externalIdentifier>\n"
        + "    </identity>\n" + "  </identification>\n"
        + "  <fileinfo>\n"
        + "    <size toolname=\"Jhove\" toolversion=\"1.16\">437</size>\n"
        + "    <filepath toolname=\"OIS File Information\" toolversion=\"0.2\" status=\"SINGLE_RESULT\">/tmp/tomcat-crud-tmp/dhrep_db175dbd32694834b4258bb33bafd3fc/dhrep_21.T11991_0000-0003-EDD3-4.bagit/data.txt</filepath>\n"
        + "    <filename toolname=\"OIS File Information\" toolversion=\"0.2\" status=\"SINGLE_RESULT\">data.txt</filename>\n"
        + "    <md5checksum toolname=\"OIS File Information\" toolversion=\"0.2\" status=\"SINGLE_RESULT\">db99f8621b020c521eab5acbb9c5e4e3</md5checksum>\n"
        + "    <fslastmodified toolname=\"OIS File Information\" toolversion=\"0.2\" status=\"SINGLE_RESULT\">1508505565000</fslastmodified>\n"
        + "  </fileinfo>\n" + "  <filestatus>\n"
        + "    <well-formed toolname=\"Jhove\" toolversion=\"1.16\" status=\"SINGLE_RESULT\">true</well-formed>\n"
        + "    <valid toolname=\"Jhove\" toolversion=\"1.16\" status=\"SINGLE_RESULT\">true</valid>\n"
        + "  </filestatus>\n" + "  <metadata>\n" + "    <text>\n"
        + "      <linebreak toolname=\"Jhove\" toolversion=\"1.16\" status=\"SINGLE_RESULT\">LF</linebreak>\n"
        + "      <charset toolname=\"Jhove\" toolversion=\"1.16\">US-ASCII</charset>\n"
        + "      <standard>\n"
        + "        <textMD:textMD xmlns:textMD=\"info:lc/xmlns/textMD-v3\">\n"
        + "          <textMD:character_info>\n"
        + "            <textMD:charset>US-ASCII</textMD:charset>\n"
        + "            <textMD:linebreak>LF</textMD:linebreak>\n"
        + "          </textMD:character_info>\n"
        + "        </textMD:textMD>\n" + "      </standard>\n"
        + "    </text>\n" + "  </metadata>\n"
        + "  <statistics fitsExecutionTime=\"127\">\n"
        + "    <tool toolname=\"MediaInfo\" toolversion=\"0.7.75\" status=\"did not run\" />\n"
        + "    <tool toolname=\"OIS Audio Information\" toolversion=\"0.1\" status=\"did not run\" />\n"
        + "    <tool toolname=\"ADL Tool\" toolversion=\"0.1\" status=\"did not run\" />\n"
        + "    <tool toolname=\"VTT Tool\" toolversion=\"0.1\" status=\"did not run\" />\n"
        + "    <tool toolname=\"Droid\" toolversion=\"6.1.5\" executionTime=\"36\" />\n"
        + "    <tool toolname=\"Jhove\" toolversion=\"1.16\" executionTime=\"95\" />\n"
        + "    <tool toolname=\"file utility\" toolversion=\"5.14\" executionTime=\"104\" />\n"
        + "    <tool toolname=\"Exiftool\" toolversion=\"10.00\" status=\"did not run\" />\n"
        + "    <tool toolname=\"NLNZ Metadata Extractor\" toolversion=\"3.6GA\" status=\"did not run\" />\n"
        + "    <tool toolname=\"OIS File Information\" toolversion=\"0.2\" executionTime=\"1\" />\n"
        + "    <tool toolname=\"OIS XML Metadata\" toolversion=\"0.2\" status=\"did not run\" />\n"
        + "    <tool toolname=\"ffident\" toolversion=\"0.2\" executionTime=\"7\" />\n"
        + "    <tool toolname=\"Tika\" toolversion=\"1.10\" executionTime=\"61\" />\n"
        + "  </statistics>\n" + "</fits>";
    String provMd = "";
    String expected =
        "{\"descriptiveMetadata\":{\"dc:title\":\"_make more collections_\",\"dc:creator\":\"fu\",\"dc:format\":\"text\\/vnd.dariah.dhrep.collection+turtle\",\"dc:rights\":\"free\",\"dc:identifier\":[\"http:\\/\\/box.dariah.local\\/1.0\\/dhcrud\\/21.T11991\\/0000-0003-EDD3-4\\/index\",\"doi:10.20375\\/0000-0003-EDD3-4\",\"hdl:21.T11991\\/0000-0003-EDD3-4\",\"https:\\/\\/hdl.handle.net\\/21.T11991\\/0000-0003-EDD3-4@index\"]},\"administrativeMetadata\":{\"datacite:identifier\":\"doi:10.20375\\/0000-0003-EDD3-4\",\"dcterms:identifier\":\"hdl:21.T11991\\/0000-0003-EDD3-4\",\"dcterms:extent\":437,\"dcterms:creator\":\"StefanFunk@dariah.eu\",\"dcterms:format\":\"text\\/vnd.dariah.dhrep.collection+turtle\",\"dcterms:created\":\"2017-10-20T15:19:25.537\",\"dcterms:relation\":\"\",\"dcterms:modified\":\"2017-10-20T15:19:25.537\"},\"technicalMetadata\":{\"valid\":\"true\",\"externalIdentifierType\":\"puid\",\"format\":\"Plain text\",\"externalIdentifier\":\"x-fmt\\/111\",\"mimetype\":\"text\\/plain\",\"well-formed\":\"true\"}}";

    String result = esj.buildDariahMetadataObject(meta, admMd, techMd, provMd).toJSONString();

    if (!result.trim().equals(expected.trim())) {
      System.out.println("!transformed:\n" + result);
      System.out.println("!expected:\n" + expected);
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Testing XML ESJsonBuilder for TextGrid.
   * </p>
   *
   * @throws SAXException
   * @throws TransformerException
   * @throws IOException
   * @throws ParserConfigurationException
   */
  @Test
  public void testBuildFulltextDataObjectXML()
      throws IOException, TransformerException, SAXException, ParserConfigurationException {

    File f = new File("./src/test/resources/fontane-kasten-c5-1zzdp.xml");
    String s = IOUtils.toString(
        new FileInputStream(new File("./src/test/resources/fontane-kasten-c5-1zzdp.json")), UTF8);
    InputStream is = new FileInputStream(f);
    String o = esj.buildFulltextDataObject(is, XML).toJSONString();

    if (!o.trim().equals(s.trim())) {
      System.out.println("!transformed: " + o);
      System.out.println("!expected:    " + s);
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Testing ESJsonBuilder for TextGrid Work METADATA!
   * </p>
   *
   * @throws SAXException
   * @throws XmlPullParserException
   * @throws TransformerException
   * @throws ParseException
   * @throws IOException
   * @throws ParserConfigurationException
   */
  @Test
  public void testBuildTextgridMetadataFromWORK() throws IOException, ParseException,
      TransformerException, XmlPullParserException, SAXException, ParserConfigurationException {

    File f = new File("./src/test/resources/bug#23530_work.meta.xml");
    String s = IOUtils.toString(
        new FileInputStream(new File("./src/test/resources/bug#23530_work.meta.json")), UTF8);
    MetadataContainerType metadata = JAXB.unmarshal(f, MetadataContainerType.class);
    String o = esj.buildTextgridMetadataObject(metadata).toJSONString();

    if (!o.trim().equals(s.trim())) {
      System.out.println("!transformed: " + o);
      System.out.println("!expected:    " + s);
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Testing ESJsonBuilder for TextGrid Work METADATA with specific ELTEC and VOCAB values!
   * </p>
   *
   * @see https://gitlab.gwdg.de/dariah-de/dariah-de-common/-/issues/16
   * @throws SAXException
   * @throws XmlPullParserException
   * @throws TransformerException
   * @throws ParseException
   * @throws IOException
   * @throws ParserConfigurationException
   */
  @Test
  public void testBuildTextgridMetadataFromWORKspecialELTEC() throws IOException, ParseException,
      TransformerException, XmlPullParserException, SAXException, ParserConfigurationException {

    File f = new File("./src/test/resources/issue#16_work.meta.xml");
    String s = IOUtils.toString(
        new FileInputStream(new File("./src/test/resources/issue#16_work.meta.json")), UTF8);
    MetadataContainerType metadata = JAXB.unmarshal(f, MetadataContainerType.class);
    String o = esj.buildTextgridMetadataObject(metadata).toJSONString();

    if (!o.trim().equals(s.trim())) {
      System.out.println("!transformed: " + o);
      System.out.println("!expected:    " + s);
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Testing ESJsonBuilder for TextGrid Work METADATA including COST Action subject metadata! This
   * is referring to projects issue#32088.
   * </p>
   *
   * @throws SAXException
   * @throws XmlPullParserException
   * @throws TransformerException
   * @throws ParseException
   * @throws IOException
   * @throws ParserConfigurationException
   */
  @Test
  public void testBuildTextgridAdditionalSubjectMetadataFromWORK()
      throws IOException, ParseException, TransformerException, XmlPullParserException,
      SAXException, ParserConfigurationException {

    File f = new File("./src/test/resources/issue#32088_work.meta.xml");
    String s = IOUtils.toString(
        new FileInputStream(new File("./src/test/resources/issue#32088_work.meta.json")), UTF8);
    MetadataContainerType metadata = JAXB.unmarshal(f, MetadataContainerType.class);
    String o = esj.buildTextgridMetadataObject(metadata).toJSONString();

    if (!o.trim().equals(s.trim())) {
      System.out.println("!transformed: " + o);
      System.out.println("!expected: " + s);
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Testing ESJsonBuilder for TextGrid Edition METADATA!
   * </p>
   *
   * @throws SAXException
   * @throws XmlPullParserException
   * @throws TransformerException
   * @throws ParseException
   * @throws IOException
   * @throws ParserConfigurationException
   */
  @Test
  public void testBuildTextgridMetadataFromEDITION() throws IOException, ParseException,
      TransformerException, XmlPullParserException, SAXException, ParserConfigurationException {

    File f = new File("./src/test/resources/bug#23530_edition.meta.xml");
    String s = IOUtils.toString(
        new FileInputStream(new File("./src/test/resources/bug#23530_edition.meta.json")), UTF8);
    MetadataContainerType metadata = JAXB.unmarshal(f, MetadataContainerType.class);
    String o = esj.buildTextgridMetadataObject(metadata).toJSONString();

    if (!o.trim().equals(s.trim())) {
      System.out.println("!transformed: " + o);
      System.out.println("!expected:    " + s);
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Empty <edition/> tags should not result in empty edition field in JSON see
   * https://projects.gwdg.de/projects/tg/work_packages/36304/activity
   * </p>
   *
   * @throws IOException
   * @throws ParseException
   * @throws TransformerException
   * @throws XmlPullParserException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  @Test
  public void testBuildTextgridMetadataFromEmptyEDITIONTag() throws IOException, ParseException,
      TransformerException, XmlPullParserException, SAXException, ParserConfigurationException {

    File f = new File("./src/test/resources/issue#36304_edition.meta.xml");

    MetadataContainerType metadata = JAXB.unmarshal(f, MetadataContainerType.class);
    String o = esj.buildTextgridMetadataObject(metadata).toJSONString();

    JsonNode node = new ObjectMapper().readTree(o);
    assertFalse(node.has("edition"));

  }

  /**
   * <p>
   * Empty <edition/> tags should not result in empty collection field in JSON see
   * https://projects.gwdg.de/projects/tg/work_packages/36304/activity
   * </p>
   *
   * @throws IOException
   * @throws ParseException
   * @throws TransformerException
   * @throws XmlPullParserException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  @Test
  public void testBuildTextgridMetadataFromEmptyCollectionTag() throws IOException, ParseException,
      TransformerException, XmlPullParserException, SAXException, ParserConfigurationException {

    File f = new File("./src/test/resources/issue#36304_collection.meta.xml");

    MetadataContainerType metadata = JAXB.unmarshal(f, MetadataContainerType.class);
    String o = esj.buildTextgridMetadataObject(metadata).toJSONString();

    JsonNode node = new ObjectMapper().readTree(o);
    assertFalse(node.has("collection"));
  }

  /**
   * <p>
   * Empty <author/> or <language/> tags should not result in empty fields in JSON
   * </p>
   *
   * @throws IOException
   * @throws ParseException
   * @throws TransformerException
   * @throws XmlPullParserException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  @Test
  public void testBuildTextgridMetadataWithoutEmptyLangAndAuthor()
      throws IOException, ParseException,
      TransformerException, XmlPullParserException, SAXException, ParserConfigurationException {

    File f = new File("./src/test/resources/ce0005.edition.meta.xml");

    MetadataContainerType metadata = JAXB.unmarshal(f, MetadataContainerType.class);
    String o = esj.buildTextgridMetadataObject(metadata).toJSONString();

    JsonNode node = new ObjectMapper().readTree(o);
    assertTrue(node.has("edition"));
    assertFalse(node.get("edition").has("language"));
    assertFalse(node.get("edition").has("agent"));
  }

  /**
   * <p>
   * Entries from <custom/> shall not result in json being generated
   * https://gitlab.gwdg.de/dariah-de/dariah-de-common/-/issues/20
   * </p>
   *
   * @throws IOException
   * @throws ParseException
   * @throws TransformerException
   * @throws XmlPullParserException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  @Test
  public void testDoNotGenerateJsonForCustomField() throws IOException, ParseException,
      TransformerException, XmlPullParserException, SAXException, ParserConfigurationException {

    File f = new File("./src/test/resources/issue#20.meta.xml");

    MetadataContainerType metadata = JAXB.unmarshal(f, MetadataContainerType.class);
    String o = esj.buildTextgridMetadataObject(metadata).toJSONString();

    JsonNode node = new ObjectMapper().readTree(o);
    assertFalse(node.has("custom"));
  }

  /**
   * <p>
   * Testing if our ESJsonBuilder is thread safe for TextGrid METADATA!
   * </p>
   *
   * @throws SAXException
   * @throws XmlPullParserException
   * @throws TransformerException
   * @throws ParseException
   * @throws IOException
   */
  @Test
  public void testBuildTextgridMetadataObjects() throws IOException, ParseException,
      TransformerException, XmlPullParserException, SAXException {

    File fList[] = new File("./src/test/resources/esjson_transform_tg").listFiles();
    for (File f : fList) {
      if (f.getName().endsWith(".xml.meta")) {
        // Get metadata object.
        ObjectType metadata = JAXB.unmarshal(f, ObjectType.class);
        // Get expected JSON string.
        String expected = IOUtils.toString(
            URI.create("file://" + f.getCanonicalPath().replaceAll(".xml.meta", "") + ".json.meta"),
            UTF8);
        // Call thread to test transforming.
        ESJsonTestThreadTG t = new ESJsonTestThreadTG(metadata, expected);
        // Start synchronously.
        // t.run();
        // Start asynchronously.
        t.start();
        t.setUncaughtExceptionHandler(
            new Thread.UncaughtExceptionHandler() {
              @Override
              public void uncaughtException(Thread t, Throwable e) {
                System.out.println("error in thread #" + t.getId() + ":");
                System.out.println(e.getMessage());
                error = true;
              }
            });
      }
    }

    if (TestESJsonBuilder.error) {
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Starts a new ESJson transforming client as a thread (TextGrid).
   * </p>
   */

  private class ESJsonTestThreadTG extends Thread {

    protected ObjectType metadata;
    protected String expected;

    /**
     * @param theMetadata
     */
    public ESJsonTestThreadTG(ObjectType theMetadata, String theExpected) {
      this.metadata = theMetadata;
      this.expected = theExpected;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
      try {

        System.out.println("thread #" + this.getId() + " started");

        MetadataContainerType mct = new MetadataContainerType();
        mct.setObject(this.metadata);

        String transformed = esj.buildTextgridMetadataObject(mct).toJSONString();

        // Code to store expected JSON files from metadata (only used once or if tests have to be
        // changed!).
        // String uri = mct.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
        // String filename = uri.replaceAll("textgrid:", "") + ".json.meta";
        // System.out.println(filename + ": " + transformed);
        // File f = new File("./src/test/resources/esjson_transform_tg/" + filename);
        // IOUtils.write(transformed, new FileOutputStream(f), "UTF-8");

        if (!transformed.trim().equals(this.expected.trim())) {
          String message = "!transformed: " + transformed + "\n" + "!expected:    " + this.expected;
          throw new AssertionError(message);
        }

        System.out.println("thread #" + this.getId() + " complete");

      } catch (IOException | ParseException | TransformerException | XmlPullParserException
          | SAXException | ParserConfigurationException e) {
        throw new AssertionError(e.getMessage());
      }
    }

  }

}
