/*
 * #%L
 * TextGrid :: Metadataschema JAXB binding
 * %%
 * Copyright (C) 2011 TextGrid Consortion (http://www.textgrid.de), SUB Göttingen (http://www.sub.uni-goettingen.de)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
@XmlSchema(namespace = "http://textgrid.info/namespaces/metadata/core/2010",
    xmlns = { 
		@XmlNs(namespaceURI = "http://textgrid.info/namespaces/metadata/core/2010", prefix = ""),
		@XmlNs(namespaceURI = "http://textgrid.info/relation-ns#", prefix = "tg"),
		@XmlNs(namespaceURI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#", prefix = "rdf")
    },
    elementFormDefault = jakarta.xml.bind.annotation.XmlNsForm.QUALIFIED)

package info.textgrid.namespaces.metadata.core._2010;

import jakarta.xml.bind.annotation.XmlNs;
import jakarta.xml.bind.annotation.XmlSchema;

