## [6.1.3](https://gitlab.gwdg.de/dariah-de/dariah-de-common/compare/v6.1.2...v6.1.3) (2024-11-21)


### Bug Fixes

* add check and correcrtion for TIF and JPG mimetypes while extracting EXIF data ([acbd328](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/acbd328ba78ffecd500370aca35923f5cdb80482))
* add REINDEXMETADATA crud operation ([4cd0e32](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/4cd0e328af936319c268a0c650ecf1cad4e7aac8))
* add tif and jpg to textgrid mimetypes and also to image set ([f6f6ce9](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/f6f6ce9ec8005d9b6f015facdf94791645c9e057))

## [6.1.2](https://gitlab.gwdg.de/dariah-de/dariah-de-common/compare/v6.1.1...v6.1.2) (2024-10-23)


### Bug Fixes

* fix bug while transforming techmd to JSON for ES ([d61d99a](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/d61d99af920208c19ccc3f103cf5e360bdfa494d))
* remove mp client dependency, and dhcrud api dependency ([bea48d9](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/bea48d9b20ca749169a57d5edbec9ce891d7cc76))
* set force.urlconnection.http.conduit option, fix some warnings ([d16ceec](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/d16ceec8a9bbbb03460bf88d661746cbc07d7dd2))

## [6.1.1](https://gitlab.gwdg.de/dariah-de/dariah-de-common/compare/v6.1.0...v6.1.1) (2024-07-04)


### Bug Fixes

* add some crud operations ([3b54fca](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/3b54fca1799a7b70efedbf0242fb438cd897b16c))
* minor operation change ([6b7567b](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/6b7567b4378ec44ee7b6f8ede162d12363f0df79))

# [6.1.0](https://gitlab.gwdg.de/dariah-de/dariah-de-common/compare/v6.0.1...v6.1.0) (2024-05-22)


### Bug Fixes

* **esutils:** JSON for textgrid mapping not valid ([f8fdf82](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/f8fdf82abf7494b605813450e33fdda6b5e8286f))


### Features

* extract gnd and basisklassifikation to own vocab.subject fields ([ff3dc8e](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/ff3dc8ee7638d7bb96d8da284905b382dcec77db))

## [6.0.1](https://gitlab.gwdg.de/dariah-de/dariah-de-common/compare/v6.0.0...v6.0.1) (2024-04-04)


### Bug Fixes

* **esutils:** do not create json fields with empty strings for empty agent or language tags ([63c696e](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/63c696ec32ee12b368c27ad7ba8a7f5b0d13a4c6))

# [6.0.0](https://gitlab.gwdg.de/dariah-de/dariah-de-common/compare/v5.0.0...v6.0.0) (2024-03-04)


### Bug Fixes

* fix last j17 issues, add editorconfig, re-format xml files ([2aa8aa1](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/2aa8aa12bbf4cc4b3deb26485447f8b23f54df58))
* **httpclients:** fix sesame client for usage with resteasy rest client ([25f1673](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/25f167339157db47294ca19d5f781af997a795b0))


### Build System

* **maven:** move common target and build jdk to 17 ([ce20480](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/ce20480c0b207cce4f7afc610df82c87138f3b84))


### Features

* add forMime attribute to portalconfig ([ffbdbc1](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/ffbdbc14304d4ab70dc79419680f03b0851b33df))
* add new mimetype ATF and add to original set ([f711be7](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/f711be75c8d193defc3678053fc1bae1d8983212))


### BREAKING CHANGES

* **maven:** generated artifacts now target jdk17 and are only consumable by projects build with jdk >= 17

# [5.0.0](https://gitlab.gwdg.de/dariah-de/dariah-de-common/compare/v4.7.0...v5.0.0) (2023-09-15)


### Code Refactoring

* remove blazegraph related code from repo ([6f10332](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/6f1033211423f1039af0e532587bb55f501e16f7))
* remove eXist related code from repo ([77699e5](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/77699e52465c4d07e1bbb6e10c52eb13937a2b5e))
* use jax-rs restclient instead of apache httpcomponents. ([9b12da8](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/9b12da8b58fbbf6ade91536bba166b41337b3aac))


### BREAKING CHANGES

* remove info.textgrid.utils.httpclient package
* deprecated class info.textgrid.utils.sesameclient.BlazegraphClient removed
* deprecated class info.textgrid.utils.existclientExistStatusCodes removed

# [4.7.0](https://gitlab.gwdg.de/dariah-de/dariah-de-common/compare/v4.6.0...v4.7.0) (2023-07-03)


### Bug Fixes

* **esutils:** custom metadata field shall not generate JSON for elasticsearch ([4115804](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/41158047e77a0b92aab93821fe35180d4c1ebb80))


### Features

* **elasticsearch-mapping:** enable term_vector:with_positions_offsets for fulltext field ([9c6e93a](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/9c6e93a8ca2c45bc70fb63ab23e281a2e923c90b))

# [4.6.0](https://gitlab.gwdg.de/dariah-de/dariah-de-common/compare/v4.5.0...v4.6.0) (2023-06-20)


### Bug Fixes

* increase imageio and imaging versions ([921f825](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/921f825862d188886c90036b3bc7ede4ecbcea4a))


### Features

* **portalconfig-jaxb:** config for facets ([7cb601c](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/7cb601c44c42b49593f267c790c6d45247e0bfd9))
* **portalconfig-jaxb:** labels and title for facets ([4c11ce8](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/4c11ce8f4913343bc8bef728a6a7c1284d5636e3))

# [4.5.0](https://gitlab.gwdg.de/dariah-de/dariah-de-common/compare/v4.4.1...v4.5.0) (2023-04-28)


### Bug Fixes

* fix more tests ([6b5dcdf](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/6b5dcdf6d8f2a2823d8fe460d5fc80b21d147f96))
* fix tests, add test for new feature eltec ([a56e6f4](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/a56e6f4d5dd5c79de0691d5ff42569287f8716d2))
* increase xstream version ([0ff66c8](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/0ff66c80eec7c6da168c02fedaa5d50ea551f0e5))


### Features

* **cli-tools:** move to own repo https://gitlab.gwdg.de/dariah-de/dariah-de-cli-tools ([1adf271](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/1adf271248284b1c4adef04a16affe7a18268b93))
* **textgrid-clients:** move to own repo https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-java-clients ([9dd5986](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/9dd5986eb70929e9b71250b19d7d59ad7669fb88))
* xslt and mapping for eltec vocabulary ([47eaabb](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/47eaabb2523062f257aa24c0f9699389f689202a))

## [4.4.1](https://gitlab.gwdg.de/dariah-de/dariah-de-common/compare/v4.4.0...v4.4.1) (2022-08-30)


### Bug Fixes

* make fix to release new version ([74d487f](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/74d487f1753ff6d6a4bcdcb255c03d9619a6203b))

# [4.4.0](https://gitlab.gwdg.de/dariah-de/dariah-de-common/compare/v4.3.1...v4.4.0) (2022-06-28)


### Bug Fixes

* **adapt:** adapt fine new dhrep things ([3d28547](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/3d285476208c91abcb0413484adaa1cca995ac7e))
* add CONTRIBUTING.md ([87441b8](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/87441b8c5965acfdeb40ba76e10351d2b2598ca0))
* add correct index field ([ded3111](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/ded3111f97fcc20a5e26ea92b1d4df66913895f1))
* add dhrep prescaling script ([ae7bec4](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/ae7bec43acfdcb307168a0e9ab15b5566a735ec9))
* add doc ([a46c089](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/a46c0897609fb607c9c5566b1f75d80a5a880a17))
* Add husky pre-commit-hook and local husky installation ([7249166](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/724916678b000b9af8ce0e3a5f9231c7c80ef32c))
* add junit reports to gitlab ci ([eb0a4fc](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/eb0a4fc41fcfb2fc7285fe62e0326085dfaeacc4))
* add local dhcrud port ([62008b4](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/62008b43592fc575324d202f6a490b4b3531061c))
* add log ([354a18b](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/354a18bfced6aa22386ce8ef304cade711e90332))
* add log ([b50dbcf](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/b50dbcf9dfc88b1905328c1e04afef769e5dc59e))
* add logging ([8f34659](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/8f3465971aee195a3660a5dc133236c36823f2f0))
* add logging ([3279b7c](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/3279b7c3bc2acaab4cfdd813919373b1d28cbd1c))
* add new dhrep prescale classes ([a9bfc97](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/a9bfc97b77b5cdcbb2a38987c60351b3e77a99cb))
* add prescale-dhrep to dispatcher ([c7e5d85](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/c7e5d8544deecd49737fe917ed80616b343bad10))
* add trim ([7a415b5](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/7a415b5e1f3125b97d517bc901d53ba1e8a7dfbc))
* Correct husky installation ([7d891c8](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/7d891c8ce9a9d8e576bb4fc417478819167d4b3a))
* finish dhrep recache ([abebfe1](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/abebfe1930597fa0ed9cd820443403f165147466))
* fix es params and query ([88ff77e](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/88ff77e5a9c05fc0eebc5133d1192ec26c9769a9))
* fix prescale-dhrep command ([74854f6](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/74854f6638588501fc46863903f96b1dd0cca6b5))
* fix removing prefix from tgrep routine, add to dhrep routine ([dee8a97](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/dee8a976125c663553f6e71969dad2aaaadc1431))
* fix reports handling ([125b6e9](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/125b6e9a33c4b0212a79b68d97da5fb0f62a4bc6))
* more gitlab ci stage changes ([bcfb0ba](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/bcfb0ba5c9f32ac0dfad1dd438499c314b3d63ec))
* more logging ([a3dcd11](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/a3dcd11d73b2da102b1b7b7ad6ef066d9f0dc8f3))
* more reporting configuration ([3ec4680](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/3ec46808e5edaf1904e96e80b0a5b2a80d6b5400))
* remove hdl: prefix ([ff910b2](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/ff910b23609c99415d3122260bc13248f93895a6))
* trim ports ([5d88f79](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/5d88f79a41e20fed3c755ed1bd2b16498ab384a8))
* use OAIPMH settinga now ([2bf6751](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/2bf67515c4b24f0d39f1a096e9da2ae6be7f736f))


### Features

* add re-cache for dhrep servers ([f5e49c2](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/f5e49c2620182df8493d9a93c9359e7b46c85343))

## [4.3.1](https://gitlab.gwdg.de/dariah-de/dariah-de-common/compare/v4.3.0...v4.3.1) (2022-06-16)


### Bug Fixes

* Add maven-semantic-release plugin to package.json ([9c45182](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/9c451828a1ad8702812a1bcd6b68f811f61d509b))
* Deploy SNAPSHOT and RELEASE BOMs ([858cf2a](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/858cf2abda194f8b88f1639bb98fa30099ae54b5))
* Increase SNAPSHOT pom versions ([9754e65](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/9754e656769c740f74d0d26a0e76f459cc1c5f61))
* Re-factor BOM deployment ([9e23585](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/9e235855ad4af3c9d8d7c098911654d83235dd4f))

# [4.3.0](https://gitlab.gwdg.de/dariah-de/dariah-de-common/compare/v4.2.1...v4.3.0) (2022-06-16)


### Bug Fixes

* Add dry-run to semantic release call ([e736895](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/e73689564ea78665364b92c552b018560f5192a8))
* Add get version from pom ([f76533a](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/f76533ac575fea73d2da8142c2865e93a613181c))
* Re-factor stages again ([9f27744](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/9f277448d3b92049683e269be5699d44b68aaab3))
* Re-factor staging ([3a804f8](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/3a804f8bf17e170422ee82dbe929622196cf5f6b))
* Remove dry-run ([5f09272](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/5f092729a7e3b70271dc734acd8f9a244f615a50))
* Set auto create project to TRUE ([6229e3f](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/6229e3f532dc9ecb76bbd9b30828f2a284e464de))
* Update CXF version ([da1e3a7](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/da1e3a7663a591fe1517bfbc405961ded40f96e3))
* Update RELEASE.md ([678e04d](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/678e04d45a9903896fbef995494dcc3bf1c57659))


### Features

* Add new semantic versioning CI workflow ([24565fb](https://gitlab.gwdg.de/dariah-de/dariah-de-common/commit/24565fbf8bd059b7fb0ad332892ee7bcb6d5228e))

# 4.1.1-SNAPSHOT

* Move from Jenkinsfile to Gitlab CI.
* Move repo from projects.gwdg.de to GWDG Gitlab.
* Move every package to ElasticSearch 6, if applicable.

# 3.6.1

* Refactor readResponse() method of dariah-storage-client: No IOException is thrown anymore, instead it returns ALWAYS the response only, please check return status codes!

# 3.6.0

* Move many methods from crud-common to commons to avoid cyclic dependencies. TG- and DH-crud now use all the common methods from common, not from crud-common. Please adapt all your crud-common dependencies!
