package info.textgrid.namespaces.metadata.portalconfig;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import info.textgrid.namespaces.metadata.portalconfig._2020_06_16.Portalconfig;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.UnmarshalException;
import jakarta.xml.bind.Unmarshaller;

class JUnit5ExampleTest {

    @Test
    void testSerialize() throws JAXBException {
        
        JAXBContext context = JAXBContext.newInstance(Portalconfig.class);
        Unmarshaller m = context.createUnmarshaller();

        String path = getClass().getClassLoader().getResource("").getPath();
        var files = listFiles(path);
        
        for (var file : files) {
            Portalconfig tmp = (Portalconfig)m.unmarshal(new File(file));
            //System.out.println(tmp.getName());
        }
    }

    @Test
    void testSerializeFails() throws JAXBException {
        
        JAXBContext context = JAXBContext.newInstance(Portalconfig.class);
        Unmarshaller m = context.createUnmarshaller();

        String path = getClass().getClassLoader().getResource("fail").getPath();
        var files = listFiles(path);
        
        for (var file : files) {
            assertThrows(
                UnmarshalException.class, () -> {
                Portalconfig tmp = (Portalconfig)m.unmarshal(new File(file));
                //System.out.println(tmp.getName());
            });
        }
    }

    @Test
    void testHtml4mime() throws JAXBException{

        JAXBContext context = JAXBContext.newInstance(Portalconfig.class);
        Unmarshaller m = context.createUnmarshaller();

        String path = getClass().getClassLoader().getResource("").getPath();
        Portalconfig tmp = (Portalconfig)m.unmarshal(new File(path +"/portalconfig_xslt4mime.xml"));
        assertEquals(tmp.getXslt().getHtml().get(0).getForMime(), "application/xml;derived=true");

    }

    public Set<String> listFiles(String dir) {
        return Stream.of(new File(dir).listFiles())
          .filter(file -> !file.isDirectory())
          .map(File::getAbsolutePath)
          .collect(Collectors.toSet());
    }
}
