<?xml version='1.0' encoding='UTF-8'?>
<portalconfig 
xmlns="http://textgrid.info/namespaces/metadata/portalconfig/2020-06-16" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://textgrid.info/namespaces/metadata/portalconfig-2020-06-16
  https://textgridlab.org/schema/textgrid-portalconfig_2020-06-16.xsd">

	<!-- Name and description of this project: How should it be listed in textgridrep.org/projects -->
	<name>European Literary Text Collection (ELTeC)</name>
	<description>Distant Reading for European Literary History (COST Action
		CA16204) is a project aiming to create a vibrant and diverse network
		of researchers jointly developing the resources and methods necessary
		to change the way European literary history is written. Grounded in
		the Distant Reading paradigm (i.e. using computational methods of
		analysis for large collections of literary texts), the Action will
		create a shared theoretical and practical framework to enable
		innovative, sophisticated, data-driven, computational methods of
		literary text analysis across at least 10 European languages.
		Fostering insight into cross-national, large-scale patterns and
		evolutions across European literary traditions, the Action will
		facilitate the creation of a broader, more inclusive and
		better-grounded account of European literary history and cultural
		identity.</description>
	<!-- Avatar is a TextGrid URI pointing to a published image, it will be
		shown in 250x250px --> 
	<avatar>textgrid:41mst.0</avatar>
    <!-- project specific facets -->
    <facets>

    	<facet select="work.subject.id.value">
    		<title xml:lang="de">Basisklassifikation</title>
    		<title xml:lang="en">Basic Classification</title>

    		<label for="17.97" xml:lang="de">17.97 Texte eines einzelnen Autors</label>
    		<label for="17.97" xml:lang="en">17.97 Texts by a single author</label>

    		<label for="18.56" xml:lang="de">18.56 Tschechische Sprache und Literatur</label>
    		<label for="18.56" xml:lang="en">18.56 Czech language and literature</label>

    		<label for="18.10" xml:lang="de">18.10 Deutsche Literatur</label>
    		<label for="18.10" xml:lang="en">18.10 German literature</label>

    		<label for="18.05" xml:lang="de">18.05 Englische Literatur</label>
    		<label for="18.05" xml:lang="en">18.05 English literature</label>

    		<label for="18.23" xml:lang="de">18.23 Französische Literatur</label>
    		<label for="18.23" xml:lang="en">18.23 French literature</label>

    		<label for="18.63" xml:lang="de">18.63 Serbokroatische Sprache und Literatur</label>
    		<label for="18.63" xml:lang="en">18.63 Serbo-Croatian language and literature</label>

    		<label for="18.83" xml:lang="de">18.83 Ungarische Sprache und Literatur</label>
    		<label for="18.83" xml:lang="en">18.83 Hungarian language and literature</label>

    		<label for="18.65" xml:lang="de">18.65 Baltische Sprachen und Literaturen</label>
    		<label for="18.65" xml:lang="en">18.65 Baltic Languages and literatures</label>

    		<label for="18.16" xml:lang="de">18.16 Norwegische Sprache und Literatur</label>
    		<label for="18.16" xml:lang="en">18.16 Norwegian language and literature</label>

    		<label for="18.58" xml:lang="de">18.58 Polnische Sprache und Literatur</label>
    		<label for="18.58" xml:lang="en">18.58 Polish language and literature</label>

    		<label for="18.37" xml:lang="de">18.37 Portugiesische Literatur</label>
    		<label for="18.37" xml:lang="en">18.37 Portuguese literature</label>

    		<label for="18.39" xml:lang="de">18.39 Romanische Sprachen und Literaturen: Sonstiges</label>
    		<label for="18.39" xml:lang="en">18.39 Romance languages and literatures: other</label>

    		<label for="18.64" xml:lang="de">18.64 Slowenische Sprache und Literatur</label>
    		<label for="18.64" xml:lang="en">18.64 Slovene language and literature</label>

    		<label for="18.32" xml:lang="de">18.32 Spanische Literatur</label>
    		<label for="18.32" xml:lang="en">18.32 Spanish literature</label>

    		<label for="18.17" xml:lang="de">18.17 Schwedische Sprache und Literatur</label>
    		<label for="18.17" xml:lang="en">18.17 Swedish language and literature</label>

    		<label for="18.54" xml:lang="de">18.54 Ostslawische Sprachen und Literaturen: Sonstiges</label>
    		<label for="18.54" xml:lang="en">18.54 East Slavic languages and literatures: other</label>

    	</facet>

    	<facet select="work.vocab.eltec.timeSlot">
    		<title xml:lang="de">Zeitspanne</title>
    		<title xml:lang="en">Timeslot</title>

    		<label for="T1" xml:lang="en">1840–1859</label>
    		<label for="T1" xml:lang="de">1840–1859</label>

    		<label for="T2" xml:lang="en">1860–1879</label>
    		<label for="T2" xml:lang="de">1860–1879</label>

    		<label for="T3" xml:lang="en">1880–1899</label>
    		<label for="T3" xml:lang="de">1880–1899</label>

    		<label for="T4" xml:lang="en">1900–1920</label>
    		<label for="T4" xml:lang="de">1900–1920</label>
    	</facet>
    	<facet select="work.vocab.eltec.size">
    		<title xml:lang="de">Länge</title>
    		<title xml:lang="en">Size</title>

    		<label for="short" xml:lang="en">Short</label>
    		<label for="short" xml:lang="de">Kurz</label>

    		<label for="medium" xml:lang="en">Medium</label>
    		<label for="medium" xml:lang="de">Mittel</label>

    		<label for="long" xml:lang="en">Long</label>
    		<label for="long" xml:lang="de">Lang</label>
    	</facet>
    	<facet select="work.vocab.eltec.authorGender">
    		<title xml:lang="de">Autor*innengeschlecht</title>
    		<title xml:lang="en">Author's gender</title>

    		<label for="male" xml:lang="en">Male</label>
    		<label for="male" xml:lang="de">Männlich</label>

    		<label for="female" xml:lang="en">Female</label>
    		<label for="female" xml:lang="de">Weiblich</label>

    		<label for="mixed" xml:lang="en">Mixed/diverse/undefined</label>
    		<label for="mixed" xml:lang="de">Gemischt/diverse/undefiniert</label>
    	</facet>

    	<facet select="work.vocab.eltec.reprintCount">
    		<title xml:lang="de">Anzahl Nachdrucke</title>
    		<title xml:lang="en">Reprint count</title>
    		
    		<label for="high" xml:lang="en">High</label>
		    <label for="high" xml:lang="de">Hoch</label>

		    <label for="low" xml:lang="en">Low</label>
		    <label for="low" xml:lang="de">Niedrig</label>

		    <label for="unspecified" xml:lang="en">Unspecified</label>
		    <label for="unspecified" xml:lang="de">Keine Angabe</label>     		

    	</facet>
    	<facet select="work.vocab.eltec.corpusCollection">
    		<title xml:lang="de">Korpus</title>
    		<title xml:lang="en">Corpus</title>
		

    		<label for="cze" xml:lang="en">Czech</label>
		    <label for="cze" xml:lang="de">Tschechisch</label>

		    <label for="deu" xml:lang="en">German</label>
		    <label for="deu" xml:lang="de">Deutsch</label> 

		    <label for="eng" xml:lang="en">English</label>
		    <label for="eng" xml:lang="de">Englisch</label> 

		    <label for="fra" xml:lang="en">French</label>
		    <label for="fra" xml:lang="de">Französisch</label> 

		    <label for="gsw" xml:lang="en">Swiss German</label>
		    <label for="gsw" xml:lang="de">Schweizerdeutsch</label> 

		    <label for="hun" xml:lang="en">Hungarian</label>
		    <label for="hun" xml:lang="de">Ungarisch</label> 

		    <label for="nor" xml:lang="en">Norwegian</label>
		    <label for="nor" xml:lang="de">Norwegisch</label> 

		    <label for="pol" xml:lang="en">Polish</label>
		    <label for="pol" xml:lang="de">Polnisch</label> 

		    <label for="por" xml:lang="en">Portuguese</label>
		    <label for="por" xml:lang="de">Portugiesisch</label> 

		    <label for="rom" xml:lang="en">Romanian</label>
		    <label for="rom" xml:lang="de">Rumänisch</label> 

		    <label for="slv" xml:lang="en">Slovenian</label>
		    <label for="slv" xml:lang="de">Slowenisch</label> 

		    <label for="spa" xml:lang="en">Spanish</label>
		    <label for="spa" xml:lang="de">Spanisch</label> 

		    <label for="srp" xml:lang="en">Serbian</label>
		    <label for="srp" xml:lang="de">Serbisch</label> 

		    <label for="swe" xml:lang="en">Swedish</label>
		    <label for="swe" xml:lang="de">Schwedisch</label> 

		    <label for="ukr" xml:lang="en">Ukrainian</label>
		    <label for="ukr" xml:lang="de">Ukrainisch</label> 
    	</facet>
    </facets>

</portalconfig>