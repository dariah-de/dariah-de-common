# JAXB databinding for the portalconfig

generated with

        trang portalconfig.xml src/main/resources/textgrid-portalconfig_2020-06-16.xsd

and lint it

        xmllint --schema src/main/resources/textgrid-portalconfig_2020-06-16.xsd portalconfig.xml

# Examples & Testing

For examples of a valid portalconfig look into [src/test/resources](src/test/resources).

Please try to check with all existing published portalconfigs from
[textgridrep.org](https://textgridrep.org/search?filter=format%3atext%2ftg.portalconfig%2bxml)
if nothing broke, before changing stuff.
