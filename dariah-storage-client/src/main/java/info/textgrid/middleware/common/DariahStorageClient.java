/**
 * This software is copyright (c) 2024 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright DARIAH-DE Consortium (https://de.dariah.eu)
 * @license Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt GNU)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * @version 2024-09-30
 */

public class DariahStorageClient implements Serializable {

  // **
  // FINALS
  // **

  public static final String AUTH_TOKEN = "Authorization";
  public static final String TRANSACTION_ID = "X-Transaction-ID";
  public static final String BEARER = "bearer ";
  public static final String LOCATION = "Location";

  public static final String LIST_PATH = "list";
  public static final String TOKENINFO_PATH = "auth/info";
  public static final String PUBLISH_PATH = "publish";
  public static final String UNPUBLISH_PATH = "unpublish";
  public static final String CHECKACCESS_PATH = "checkAccess/";
  public static final String CHECKACCESS_OPERATION_READ = "read";
  public static final String CHECKACCESS_OPERATION_WRITE = "write";
  public static final String CHECKACCESS_OPERATION_DELETE = "delete";

  private static final long serialVersionUID = 6732445365684360629L;

  private static final String STORAGE_SAYS = "Storage says: ";
  private static final String ERROR_CREATING = "Error creating file! " + STORAGE_SAYS;
  private static final String ERROR_READING = "Error reading file! " + STORAGE_SAYS;
  private static final String ERROR_UPDATING = "Error updating file! " + STORAGE_SAYS;
  private static final String ERROR_DELETING = "Error deleting file! " + STORAGE_SAYS;
  private static final String ERROR_CHECKING_ACCESS = "Error checking access! " + STORAGE_SAYS;
  private static final String ERROR_PUBLISHING = "Error publishing file! " + STORAGE_SAYS;;
  private static final String ERROR_UNPUBLISING = "Error unpublishing file! " + STORAGE_SAYS;
  private static final String ERROR_LISTING = "Error getting object list! " + STORAGE_SAYS;
  private static final String ERROR_TOKENINFO = "Error getting token info! " + STORAGE_SAYS;
  private static final String UTF8 = "UTF-8";

  private static final Level LOGLEVEL = Level.WARNING;

  // **
  // CLASS
  // **

  private Client client;
  private URI storageUri;
  protected Logger defaultLogger;

  public DariahStorageClient() {
    this.client = ClientBuilder.newClient()
        .property("thread.safe.client", "true")
        // use httpUrlConnection and http1, see
        // https://cwiki.apache.org/confluence/pages/viewpage.action?pageId=49941
        .property("force.urlconnection.http.conduit", "true");
    if (this.defaultLogger == null) {
      this.defaultLogger = Logger.getLogger(DariahStorageClient.class.getName());
      this.defaultLogger.setLevel(LOGLEVEL);
    }
  }

  /**
   * @param is
   * @param mimetype
   * @param token
   * @return
   * @throws IOException
   */
  public String createFile(final InputStream is, final String mimetype, final String token)
      throws IOException {
    return createFile(is, mimetype, token, null);
  }

  /**
   * @param is
   * @param mimetype
   * @param token
   * @param logID
   * @return
   * @throws IOException
   */
  public String createFile(final InputStream is, final String mimetype, final String token,
      final String logID) throws IOException {

    String logString = "[" + logID + "] ";

    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
    headers = addTokenToHeader(token, headers);
    headers = addLogIDToHeader(logID, headers);

    this.defaultLogger.log(Level.INFO, logString + headers);
    this.defaultLogger.log(Level.INFO, logString + mimetype);

    Response response = this.client.target(this.storageUri).request().headers(headers)
        .post(Entity.entity(is, mimetype));

    this.defaultLogger.log(Level.INFO, logString + response.getStatus());
    this.defaultLogger.log(Level.INFO, logString + response.getEntity());
    this.defaultLogger.log(Level.INFO, logString + response.readEntity(String.class));

    if (response.getStatus() != Status.CREATED.getStatusCode()) {
      String message = logString + ERROR_CREATING + response.getStatus() + " "
          + response.getStatusInfo().getReasonPhrase();
      this.defaultLogger.log(Level.SEVERE, message);
      throw new IOException(message);
    }

    String location = response.getHeaderString("Location");

    this.defaultLogger.log(Level.INFO, logString + location);

    return location.substring(location.lastIndexOf("/") + 1);
  }

  /**
   * @param id
   * @return
   * @throws IOException
   */
  public InputStream readFile(final String id) throws IOException {
    return readFile(id, null, null);
  }

  /**
   * @param id
   * @param token
   * @return
   * @throws IOException
   */
  public InputStream readFile(final String id, final String token) throws IOException {
    return readFile(id, token, null);
  }

  /**
   * @param id
   * @param token
   * @param logID
   * @return
   * @throws IOException
   */
  public InputStream readFile(final String id, final String token, final String logID)
      throws IOException {

    String logString = "[" + logID + "] ";

    Response response = readResponse(id, token, logString);

    // Only 200 and 204 are acceptable as response!
    if (response.getStatus() != Status.OK.getStatusCode()
        && response.getStatus() != Status.NO_CONTENT.getStatusCode()) {
      String message = logString + ERROR_READING + response.getStatus() + " "
          + response.getStatusInfo().getReasonPhrase();
      this.defaultLogger.log(Level.SEVERE, message);
      throw new IOException(message);
    }

    return (InputStream) response.getEntity();
  }

  /**
   * @param id
   * @return
   */
  public Response readResponse(final String id) {
    return readResponse(id, null, null);
  }

  /**
   * @param id
   * @param token
   * @return
   */
  public Response readResponse(final String id, final String token) {
    return readResponse(id, token, null);
  }

  /**
   * @param id
   * @param token
   * @param logID
   * @return
   */
  public Response readResponse(final String id, final String token, final String logID) {

    Response result = null;

    String logString = "[" + logID + "] ";

    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
    headers = addTokenToHeader(token, headers);
    headers = addLogIDToHeader(logID, headers);

    result = this.client.target(this.storageUri).path(id).request().headers(headers).get();

    this.defaultLogger.log(Level.INFO, logString + result.getStatus());

    return result;
  }

  /**
   * @param id
   * @param is
   * @param mimeType
   * @param token
   * @return
   * @throws IOException
   */
  public String updateFile(final String id, final InputStream is, final String mimeType,
      final String token) throws IOException {
    return updateFile(id, is, mimeType, token, null);
  }

  /**
   * @param id
   * @param is
   * @param mimeType
   * @param token
   * @param logID
   * @return
   * @throws IOException
   */
  public String updateFile(final String id, final InputStream is, final String mimeType,
      final String token, final String logID) throws IOException {

    String logString = "[" + logID + "] ";

    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
    headers = addTokenToHeader(token, headers);
    headers = addLogIDToHeader(logID, headers);

    Response response = this.client.target(this.storageUri).path(id).request().headers(headers)
        .put(Entity.entity(is, mimeType));

    this.defaultLogger.log(Level.INFO, logString + response.getHeaders());

    if (response.getStatus() != Status.CREATED.getStatusCode()) {
      String message = logString + ERROR_UPDATING + response.getStatus() + " "
          + response.getStatusInfo().getReasonPhrase();
      this.defaultLogger.log(Level.SEVERE, message);
      throw new IOException(message);
    }

    String location = response.getHeaderString(LOCATION);

    this.defaultLogger.log(Level.INFO, logString + location);

    return location.substring(location.lastIndexOf("/") + 1);
  }

  /**
   * @param id
   * @param token
   * @throws IOException
   */
  public void deleteFile(final String id, final String token) throws IOException {
    deleteFile(id, token, null);
  }

  /**
   * @param id
   * @param token
   * @param logID
   * @throws IOException
   */
  public void deleteFile(final String id, final String token, final String logID)
      throws IOException {

    String logString = "[" + logID + "] ";

    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
    headers = addTokenToHeader(token, headers);
    headers = addLogIDToHeader(logID, headers);

    Response response =
        this.client.target(this.storageUri).path(id).request().headers(headers).delete();

    if (response.getStatus() != Status.NO_CONTENT.getStatusCode()) {
      String message = logString + ERROR_DELETING + response.getStatus() + " "
          + response.getStatusInfo().getReasonPhrase();
      this.defaultLogger.log(Level.SEVERE, message);
      throw new IOException(message);
    }
  }

  /**
   * @param id
   * @param token
   * @param logID
   * @param operation
   * @return
   * @throws IOException
   */
  public boolean checkAccess(final String id, final String token, final String logID,
      final String operation) throws IOException {

    boolean result = false;

    String logString = "[" + logID + "] ";

    if (operation != CHECKACCESS_OPERATION_READ && operation != CHECKACCESS_OPERATION_WRITE
        && operation != CHECKACCESS_OPERATION_DELETE) {
      String message = logString + "Operation " + operation + " not supported!";
      this.defaultLogger.log(Level.SEVERE, message);
      throw new IOException(message);
    }

    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
    headers = addTokenToHeader(token, headers);
    headers = addLogIDToHeader(logID, headers);

    String path = id + "/" + CHECKACCESS_PATH + operation;

    this.defaultLogger.log(Level.INFO, logString + "POST " + this.storageUri + path);

    Response response =
        this.client.target(this.storageUri).path(path).request().headers(headers).post(null);

    this.defaultLogger.log(Level.INFO,
        logString + response.getStatus() + " " + response.getStatusInfo().getReasonPhrase());

    if (response.getStatus() == Status.NO_CONTENT.getStatusCode()) {
      result = true;
    } else if (response.getStatus() == Status.UNAUTHORIZED.getStatusCode()) {
      result = false;
    } else if (response.getStatus() == Status.FORBIDDEN.getStatusCode()) {
      result = false;
    } else {
      String message = logString + ERROR_CHECKING_ACCESS + response.getStatus() + " "
          + response.getStatusInfo().getReasonPhrase();
      this.defaultLogger.log(Level.SEVERE, message);
      throw new IOException(message);
    }

    return result;
  }

  /**
   * @param id
   * @param token
   * @param logID
   * @throws IOException
   */
  public void publish(final String id, final String token, final String logID) throws IOException {

    String logString = "[" + logID + "] ";

    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
    headers = addTokenToHeader(token, headers);
    headers = addLogIDToHeader(logID, headers);

    String path = id + "/" + PUBLISH_PATH;

    this.defaultLogger.log(Level.INFO, logString + "POST " + this.storageUri + path);

    Response response =
        this.client.target(this.storageUri).path(path).request().headers(headers).post(null);

    this.defaultLogger.log(Level.INFO,
        logString + response.getStatus() + " " + response.getStatusInfo().getReasonPhrase());

    if (response.getStatus() != Status.NO_CONTENT.getStatusCode()) {
      String message = logString + ERROR_PUBLISHING + response.getStatus() + " "
          + response.getStatusInfo().getReasonPhrase();
      this.defaultLogger.log(Level.SEVERE, message);
      throw new IOException(message);
    }
  }

  /**
   * @param id
   * @param token
   * @param logID
   * @throws IOException
   */
  public void unpublish(final String id, final String token, final String logID)
      throws IOException {

    String logString = "[" + logID + "] ";

    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
    headers = addTokenToHeader(token, headers);
    headers = addLogIDToHeader(logID, headers);

    String path = id + "/" + UNPUBLISH_PATH;

    this.defaultLogger.log(Level.INFO, logString + "POST " + this.storageUri + path);

    Response response =
        this.client.target(this.storageUri).path(path).request().headers(headers).post(null);

    this.defaultLogger.log(Level.INFO,
        logString + response.getStatus() + " " + response.getStatusInfo().getReasonPhrase());

    if (response.getStatus() != Status.NO_CONTENT.getStatusCode()) {
      String message = logString + ERROR_UNPUBLISING + response.getStatus() + " "
          + response.getStatusInfo().getReasonPhrase();
      this.defaultLogger.log(Level.SEVERE, message);
      throw new IOException(message);
    }
  }

  /**
   * <p>
   * Gets all the public storage resources belonging to the given OAuth2 token.
   * </p>
   * 
   * @param token
   * @param logID
   * @return
   * @throws IOException
   */
  public String listPublicResources(final String token, final String logID) throws IOException {
    return listResources(token, logID, true);
  }

  /**
   * <p>
   * Gets all the non-public storage resources belonging to the given OAuth2 token.
   * </p>
   * 
   * @param token
   * @param logID
   * @return
   * @throws IOException
   */
  public String listNonpublicResources(final String token, final String logID) throws IOException {
    return listResources(token, logID, false);
  }

  /**
   * <p>
   * Gets all public AND non-public storage resources belonging to the given OAuth2 token.
   * </p>
   * 
   * @param token
   * @param logID
   * @return
   * @throws IOException
   */
  public String listResources(final String token, final String logID) throws IOException {
    return listResources(token, logID, null);
  }

  /**
   * <p>
   * Gets a list of objects from the DARIAH-DE Storage, public or non-public objects or both/all.
   * </p>
   * 
   * @param token The OAuth token for authentication and authorization.
   * @param logID A log ID.
   * @param publicOPrNotPublic Set to TRUE if only public objects, set to FALSE if only non-public
   *        objects shall be listed. If null, public and non-public objects are being listed.
   * @return A list in JSON format as the following:
   *         <p>
   *         [ { "public": false, "ownStorage": true, "id": "EAEA0-FF14-88F0-9E9E-0" }, { "public":
   *         false, "ownStorage": true, "id": "EAEA0-182E-A43B-FEB9-0" } ]
   *         </p>
   * @throws IOException
   */
  public String listResources(final String token, final String logID,
      final Boolean publicOPrNotPublic) throws IOException {

    String result = "";

    String logString = "[" + logID + "] ";

    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
    headers = addTokenToHeader(token, headers);
    headers = addLogIDToHeader(logID, headers);

    this.defaultLogger.log(Level.INFO, logString + "GET " + this.storageUri + LIST_PATH);

    WebTarget webTarget = this.client.target(this.storageUri).path(LIST_PATH);

    if (publicOPrNotPublic != null) {
      webTarget = webTarget.queryParam("public", publicOPrNotPublic);
    }

    Response response = webTarget.request().headers(headers).get();

    this.defaultLogger.log(Level.INFO,
        logString + response.getStatus() + " " + response.getStatusInfo().getReasonPhrase());

    if (response.getStatus() != Status.OK.getStatusCode()) {
      String message = logString + ERROR_LISTING + response.getStatus() + " "
          + response.getStatusInfo().getReasonPhrase();
      this.defaultLogger.log(Level.SEVERE, message);
      throw new IOException(message);
    }

    result = IOUtils.toString(response.readEntity(InputStream.class), UTF8);

    return result;
  }

  /**
   * <p>
   * Gets an token info of the given OAuth token.
   * </p>
   * 
   * @param token The OAuth token for authentication and authorization.
   * @param logID A log ID.
   * @return A JSON according to the PDP paper including token info.
   * @throws IOException
   */
  public String authInfo(final String token, final String logID) throws IOException {

    String result = "";

    String logString = "[" + logID + "] ";

    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
    headers = addTokenToHeader(token, headers);
    headers = addLogIDToHeader(logID, headers);

    this.defaultLogger.log(Level.INFO, logString + "GET " + this.storageUri + TOKENINFO_PATH);

    WebTarget webTarget = this.client.target(this.storageUri).path(TOKENINFO_PATH);

    Response response = webTarget.request().headers(headers).get();

    this.defaultLogger.log(Level.INFO,
        logString + response.getStatus() + " " + response.getStatusInfo().getReasonPhrase());

    if (response.getStatus() != Status.OK.getStatusCode()) {
      String message = logString + ERROR_TOKENINFO + response.getStatus() + " "
          + response.getStatusInfo().getReasonPhrase();
      this.defaultLogger.log(Level.SEVERE, message);
      throw new IOException(message);
    }

    result = IOUtils.toString(response.readEntity(InputStream.class), UTF8);

    return result;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param logID
   * @param header
   * @return
   */
  protected static MultivaluedMap<String, Object> addLogIDToHeader(String logID,
      MultivaluedMap<String, Object> header) {

    MultivaluedMap<String, Object> result = header;

    if (result == null) {
      result = new MultivaluedHashMap<String, Object>();
    }

    if (logID != null && !logID.isEmpty()) {
      result.add(TRANSACTION_ID, logID);
    }

    return result;
  }

  /**
   * @param theToken
   * @param theHeaders
   * @return
   */
  protected static MultivaluedMap<String, Object> addTokenToHeader(String theToken,
      MultivaluedMap<String, Object> theHeaders) {

    MultivaluedMap<String, Object> result = theHeaders;

    if (result == null) {
      result = new MultivaluedHashMap<String, Object>();
    }

    if (theToken != null && !theToken.isEmpty()) {
      result.add(AUTH_TOKEN, BEARER + theToken);
    }

    return result;
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @return
   */
  public URI getStorageUri() {
    return this.storageUri;
  }

  /**
   * @param storageUri
   * @return
   */
  public DariahStorageClient setStorageUri(URI storageUri) {
    this.storageUri = storageUri;
    return this;
  }

}
