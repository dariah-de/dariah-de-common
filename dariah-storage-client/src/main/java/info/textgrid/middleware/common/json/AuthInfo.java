package info.textgrid.middleware.common.json;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 */
public class AuthInfo {
  public String audience;
  public String scopes[];
  public Principal principal;
  @JsonProperty("expires_in")
  public long expiresIn;

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {

    String result = "";

    result += "audience: " + this.audience + "\nscopes: " + getScopes() + "\nprincipal: "
        + this.principal + "\nexpires_in: " + this.expiresIn;

    return result;
  }

  /**
   * @return
   */
  private String getScopes() {

    String result = "";

    for (String s : this.scopes) {
      result += s + ", ";
    }

    return result;
  }

}
