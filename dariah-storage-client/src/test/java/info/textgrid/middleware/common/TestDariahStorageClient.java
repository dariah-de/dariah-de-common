/*******************************************************************************
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@sub.uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.middleware.common;

import static org.junit.Assert.assertTrue;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/*******************************************************************************
 * TODOLOG
 * 
 *******************************************************************************
 * CHANGELOG
 * 
 * 2019-03-06 - Funk - Added tests for new Storage API V2.0 methods.
 * 
 * 2018-04-23 - Funk - First version.
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * JUnit class for testing the DARIAH Storage Client implementation.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2019-03-18
 * @since 2018-04-23
 ******************************************************************************/

public class TestDariahStorageClient {

  // **
  // STATIC FINALS
  // **

  // **
  // STATICS
  // **

  // **
  // SET UPS
  // **

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  // **
  // TESTS
  // **

  /**
   * 
   */
  @Test
  public void testAddTokenToHeaderNewHeaderMap() {

    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
    String token = "TOKTOKTOK";
    String expToken = "bearer " + token;

    headers = DariahStorageClient.addTokenToHeader(token, headers);

    String tokHeader = (String) headers.get(DariahStorageClient.AUTH_TOKEN).get(0);
    if (!expToken.equals(tokHeader)) {
      System.out.println(tokHeader + " != " + expToken);
      assertTrue(false);
    }
  }

  /**
   * 
   */
  @Test
  public void testAddLogIDToHeaderNewHeaderMap() {

    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
    String expLogID = "LOGLOGLOG";

    headers = DariahStorageClient.addLogIDToHeader(expLogID, headers);

    String logID = (String) headers.get(DariahStorageClient.TRANSACTION_ID).get(0);
    if (!expLogID.equals(logID)) {
      System.out.println(logID + " != " + expLogID);
      assertTrue(false);
    }
  }

  /**
   * 
   */
  @Test
  public void testAddTokenToHeaderExistingHeaderMap() {

    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
    headers.add(DariahStorageClient.TRANSACTION_ID, "LOGLOGLOG");
    String token = "TOKTOKTOK";

    MultivaluedMap<String, Object> expHeaders = new MultivaluedHashMap<String, Object>();
    expHeaders.add(DariahStorageClient.TRANSACTION_ID, "LOGLOGLOG");
    expHeaders.add(DariahStorageClient.AUTH_TOKEN, DariahStorageClient.BEARER + token);

    headers = DariahStorageClient.addTokenToHeader(token, headers);

    if (!expHeaders.equalsIgnoreValueOrder(headers)) {
      System.out.println(headers + " != " + expHeaders);
      assertTrue(false);
    }
  }

  /**
   * 
   */
  @Test
  public void testAddLogIDToHeaderExistingHeaderMap() {

    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
    headers.add(DariahStorageClient.AUTH_TOKEN, DariahStorageClient.BEARER + "TOKTOKTOK");
    String logID = "LOGLOGLOG";

    MultivaluedMap<String, Object> expHeaders = new MultivaluedHashMap<String, Object>();
    expHeaders.add(DariahStorageClient.TRANSACTION_ID, logID);
    expHeaders.add(DariahStorageClient.AUTH_TOKEN, DariahStorageClient.BEARER + "TOKTOKTOK");

    headers = DariahStorageClient.addLogIDToHeader(logID, headers);

    if (!expHeaders.equalsIgnoreValueOrder(headers)) {
      System.out.println(headers + " != " + expHeaders);
      assertTrue(false);
    }
  }

}

