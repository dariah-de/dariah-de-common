/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.common;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import com.fasterxml.jackson.databind.ObjectMapper;
import info.textgrid.middleware.common.json.AuthInfo;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2020-04-30 - Funk - Adapt to non-exception readResponse storage client method.
 * 
 * 2019-04-24 - Funk - More tests added for list resources methods.
 * 
 * 2019-03-06 - Funk - Added online tests for new Storage API V2.0 methods.
 * 
 * 2018-04-23 - Funk - First version.
 */

/**
 * <p>
 * JUnit class for testing the DARIAH Storage Client implementation.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-04-30
 * @since 2018-04-23
 */
@Ignore
public class TestDariahStorageClientONLINE {

  // **
  // STATIC FINALS
  // **

  private static final String LINES = "\n====================";
  private static final String ARR = "  -->  ";
  private static final String OK = "OK";
  private static final String ERROR = "ERROR";

  // private static final String PROPERTIES_FILE = "common.test.properties";
  private static final String PROPERTIES_FILE = "common.pdptest.properties";

  // **
  // STATICS
  // **

  private static DariahStorageClient ownStorageClient = new DariahStorageClient();
  private static DariahStorageClient publicStorageClient = new DariahStorageClient();

  private static String ownStorageEndpoint;
  private static String publicStorageEndpoint;
  private static String testPNG;
  private static String testJPG;
  private static String invalidToken;
  private static String token;
  private static String logID;
  private static String eppn;

  private static ObjectMapper jsonObjectMapper = new ObjectMapper();

  // **
  // SET UPS
  // **

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {

    Properties p = new Properties();
    p.load(new FileInputStream(getResource(PROPERTIES_FILE)));

    ownStorageEndpoint = p.getProperty("OWN_STORAGE_ENDPOINT");
    publicStorageEndpoint = p.getProperty("PUBLIC_STORAGE_ENDPOINT");
    testPNG = p.getProperty("TEST_PNG");
    testJPG = p.getProperty("TEST_JPG");
    invalidToken = p.getProperty("INVALID_TOKEN");
    token = p.getProperty("TOKEN");
    logID = p.getProperty("LOG_ID");
    eppn = p.getProperty("EPPN");

    ownStorageClient.setStorageUri(URI.create(ownStorageEndpoint));
    publicStorageClient.setStorageUri(URI.create(publicStorageEndpoint));

    ownStorageClient.defaultLogger.setLevel(Level.INFO);
    publicStorageClient.defaultLogger.setLevel(Level.INFO);
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  // **
  // ONLINE TESTS
  // **

  /**
   * <p>
   * DARIAH Storage Client seems to be streaming already!
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testDariahPublicStorageRead() throws IOException {

    System.out.println(LINES + " testDariahPublicStorageRead()");

    String id = "EAEA0-0532-5674-E876-0";
    Response response = publicStorageClient.readResponse(id);

    int statusCode = response.getStatus();
    String reasonPhrase = response.getStatusInfo().getReasonPhrase();

    System.out
        .println("\t#READ" + ARR + statusCode + " " + reasonPhrase + ARR + response.getLength());
  }

  /**
   * <p>
   * Testing basic methods of DARIAH-DE OwnStorage service online.
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testOwnStorageUnauthorisedCreate() throws IOException {

    System.out.println(LINES + " testOwnStorageUnauthorisedCreate()");

    try {
      System.out.print("\t#CREATE" + ARR);

      String t = String.valueOf(System.currentTimeMillis());
      ownStorageClient.createFile(new FileInputStream(testPNG), TextGridMimetypes.PNG, invalidToken,
          logID + t);
    } catch (IOException e) {
      System.out.println("Expected " + e.getClass().getName());
    }

  }

  /**
   * <p>
   * Testing basic methods of DARIAH-DE OwnStorage service online.
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testOwnStorageOnlineBasic() throws IOException {

    System.out.println(LINES + " testOwnStorageOnlineBasic()");

    // TEST #CREATE
    System.out.print("\t#CREATE" + ARR);

    String t = String.valueOf(System.currentTimeMillis());
    String id = ownStorageClient.createFile(new FileInputStream(testPNG), TextGridMimetypes.PNG,
        token, logID + t);

    System.out.println(ownStorageClient.getStorageUri() + id);

    // TEST #READ
    System.out.print("\t#READ (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    InputStream i = ownStorageClient.readFile(id, token, logID + t);

    System.out.println(org.apache.cxf.helpers.IOUtils.readStringFromStream(i).length());

    // TEST #READ (no token)
    System.out.print("\t#READ NOTOK (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    try {
      ownStorageClient.readFile(id, null, logID + t);
      assertTrue(false);
    } catch (IOException e) {
      System.out.println(e.getMessage() + ARR + OK);
    }

    // TEST #READ (invalid token)
    System.out.print("\t#READ INVTOK (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    try {
      ownStorageClient.readFile(id, invalidToken, logID + t);
      assertTrue(false);
    } catch (IOException e) {
      System.out.println(e.getMessage() + ARR + OK);
    }

    // TEST #UPDATE (no token)
    System.out.print("\t#UPDATE NOTOK (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    try {
      ownStorageClient.updateFile(id, new FileInputStream(testJPG), TextGridMimetypes.PNG, null,
          logID + t);
      assertTrue(false);
    } catch (IOException e) {
      System.out.println(e.getMessage() + ARR + OK);
    }

    // TEST #UPDATE (invalid token)
    System.out.print("\t#UPDATE INVTOK (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    try {
      ownStorageClient.updateFile(id, new FileInputStream(testJPG), TextGridMimetypes.PNG,
          invalidToken, logID + t);
      assertTrue(false);
    } catch (IOException e) {
      System.out.println(e.getMessage() + ARR + OK);
    }

    // TEST #UPDATE
    System.out.print("\t#UPDATE (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    String updateID = ownStorageClient.updateFile(id, new FileInputStream(testJPG),
        TextGridMimetypes.PNG, token, logID + t);

    System.out.println(ownStorageClient.getStorageUri() + updateID);

    // TEST #READ (no token)
    System.out.print("\t#READ NOTOK (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    try {
      ownStorageClient.readFile(id, null, logID + t);
      assertTrue(false);
    } catch (IOException e) {
      System.out.println(e.getMessage() + ARR + OK);
    }

    // TEST #READ (invalid token)
    System.out.print("\t#READ INVTOK (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    try {
      ownStorageClient.readFile(id, invalidToken, logID + t);
      assertTrue(false);
    } catch (IOException e) {
      System.out.println(e.getMessage() + ARR + OK);
    }

    // TEST #READ
    System.out.print("\t#READ (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    i = ownStorageClient.readFile(id, token, logID + t);

    System.out.println(org.apache.cxf.helpers.IOUtils.readStringFromStream(i).length());

    // TEST #DELETE (no token)
    System.out.print("\t#DELETE NOTOK (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    try {
      ownStorageClient.deleteFile(updateID, null, logID + t);
      assertTrue(false);
    } catch (IOException e) {
      System.out.println(e.getMessage() + ARR + OK);
    }

    // TEST #DELETE (invalid token)
    System.out.print("\t#DELETE INVTOK (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    try {
      ownStorageClient.deleteFile(updateID, invalidToken, logID + t);
      assertTrue(false);
    } catch (IOException e) {
      System.out.println(e.getMessage() + ARR + OK);
    }

    // TEST #DELETE
    System.out.print("\t#DELETE (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    ownStorageClient.deleteFile(updateID, token, logID + t);

    System.out.println(OK);

    // TEST #READ
    System.out.print("\t#READ (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    Response response = ownStorageClient.readResponse(id, token, logID + t);

    int statusCode = response.getStatus();
    if (statusCode != Status.NOT_FOUND.getStatusCode()) {
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Testing basic methods of DARIAH-DE PublicStorage service online.
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testPublicStorageOnlineBasic() throws IOException {

    System.out.println(LINES + " testPublicStorageOnlineBasic()");

    // TEST #CREATE
    System.out.print("\t#CREATE" + ARR);

    String t = String.valueOf(System.currentTimeMillis());
    String id = publicStorageClient.createFile(new FileInputStream(testPNG), TextGridMimetypes.PNG,
        token, logID + t);

    System.out.println(publicStorageClient.getStorageUri() + id);

    // TEST #READ
    System.out.print("\t#READ (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    InputStream i = publicStorageClient.readFile(id, token, logID + t);

    System.out.println(org.apache.cxf.helpers.IOUtils.readStringFromStream(i).length());

    // TEST #READ (no token)
    System.out.print("\t#READ NOTOK (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    try {
      publicStorageClient.readFile(id, null, logID + t);
      System.out.println(OK);
    } catch (IOException e) {
      assertTrue(false);
    }

    // TEST #READ (invalid token)
    System.out.print("\t#READ INVTOK (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    try {
      publicStorageClient.readFile(id, invalidToken, logID + t);
      System.out.println(OK);
    } catch (IOException e) {
      assertTrue(false);
    }

    // TEST #UPDATE (no token)
    System.out.print("\t#UPDATE NOTOK (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    try {
      publicStorageClient.updateFile(id, new FileInputStream(testJPG), TextGridMimetypes.PNG, null,
          logID + t);
      assertTrue(false);
    } catch (IOException e) {
      System.out.println(e.getMessage() + ARR + OK);
    }

    // TEST #UPDATE (invalid token)
    System.out.print("\t#UPDATE INVTOK (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    try {
      publicStorageClient.updateFile(id, new FileInputStream(testJPG), TextGridMimetypes.PNG,
          invalidToken, logID + t);
      assertTrue(false);
    } catch (IOException e) {
      System.out.println(e.getMessage() + ARR + OK);
    }

    // TEST #UPDATE
    System.out.print("\t#UPDATE (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    try {
      publicStorageClient.updateFile(id, new FileInputStream(testJPG), TextGridMimetypes.PNG, token,
          logID + t);
      assertTrue(false);
    } catch (IOException e) {
      System.out.println(e.getMessage() + ARR + OK);
    }

    // TEST #DELETE (no token)
    System.out.print("\t#DELETE NOTOK (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    try {
      publicStorageClient.deleteFile(id, null, logID + t);
      assertTrue(false);
    } catch (IOException e) {
      System.out.println(e.getMessage() + ARR + OK);
    }

    // TEST #DELETE (invalid token)
    System.out.print("\t#DELETE INVTOK (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    try {
      publicStorageClient.deleteFile(id, invalidToken, logID + t);
      assertTrue(false);
    } catch (IOException e) {
      System.out.println(e.getMessage() + ARR + OK);
    }

    // TEST #DELETE
    System.out.print("\t#DELETE (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    try {
      publicStorageClient.deleteFile(id, token, logID + t);
      assertTrue(false);
    } catch (IOException e) {
      System.out.println(e.getMessage() + ARR + OK);
    }
  }

  /**
   * <p>
   * Testing publish and unpublish methods.
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testOwnStoragePublishUnpublish() throws IOException {

    System.out.println(LINES + " testOwnStoragePublishUnpublish()");

    String log = logID + String.valueOf(System.currentTimeMillis());

    // TEST #CREATE
    System.out.print("\t#CREATE" + ARR);

    String id = ownStorageClient.createFile(new FileInputStream(testPNG), TextGridMimetypes.PNG,
        token, log);

    System.out.println(OK);

    // TEST #CHECKACCESS/READ (no token)
    System.out.print("\t#CHECKACCESS/" + DariahStorageClient.CHECKACCESS_OPERATION_READ + " NOTOK ("
        + id + ")" + ARR);

    String t = String.valueOf(System.currentTimeMillis());
    boolean access = ownStorageClient.checkAccess(id, invalidToken, logID + t,
        DariahStorageClient.CHECKACCESS_OPERATION_READ);

    if (access) {
      assertTrue(false);
    }

    System.out.println(access);

    // TEST #CHECKACCESS/READ (invalid token)
    System.out.print("\t#CHECKACCESS/" + DariahStorageClient.CHECKACCESS_OPERATION_READ
        + " INVTOK (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    access = ownStorageClient.checkAccess(id, invalidToken, logID + t,
        DariahStorageClient.CHECKACCESS_OPERATION_READ);

    if (access) {
      assertTrue(false);
    }

    System.out.println(access);

    // Test #READ (no token)
    System.out.print("\t#READ NOTOK (" + id + ")" + ARR);

    Response response = ownStorageClient.readResponse(id, null, log);
    String responseString = response.getStatus() + " " + response.getStatusInfo().getReasonPhrase();
    if (response.getStatus() == Status.OK.getStatusCode()) {
      System.out.println(OK + ": " + responseString);
    } else {
      System.out.println(ERROR + ": " + responseString);
    }

    // Test #READ (invalid token)
    System.out.print("\t#READ INVTOK (" + id + ")" + ARR);

    response = ownStorageClient.readResponse(id, invalidToken, log);
    responseString = response.getStatus() + " " + response.getStatusInfo().getReasonPhrase();
    if (response.getStatus() == Status.OK.getStatusCode()) {
      System.out.println(OK + ": " + responseString);
    } else {
      System.out.println(ERROR + ": " + responseString);
    }

    // TEST #PUBLISH
    System.out.print("\t#PUBLISH (" + id + ")" + ARR);

    ownStorageClient.publish(id, token, log);

    System.out.println(OK);

    // TEST #CHECKACCESS/READ (no token)
    System.out.print("\t#CHECKACCESS/" + DariahStorageClient.CHECKACCESS_OPERATION_READ + " NOTOK ("
        + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    access = ownStorageClient.checkAccess(id, null, logID + t,
        DariahStorageClient.CHECKACCESS_OPERATION_READ);

    if (!access) {
      assertTrue(false);
    }

    System.out.println(access);

    // TEST #CHECKACCESS/READ (invalid token)
    System.out.print("\t#CHECKACCESS/" + DariahStorageClient.CHECKACCESS_OPERATION_READ
        + " INVTOK (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    access = ownStorageClient.checkAccess(id, invalidToken, logID + t,
        DariahStorageClient.CHECKACCESS_OPERATION_READ);

    if (!access) {
      assertTrue(false);
    }

    System.out.println(access);

    // Test #READ (no token)
    System.out.print("\t#READ NOTOK (" + id + ")" + ARR);

    response = ownStorageClient.readResponse(id, null, log);
    responseString = response.getStatus() + " " + response.getStatusInfo().getReasonPhrase();
    if (response.getStatus() == Status.OK.getStatusCode()) {
      System.out.println(OK + ": " + responseString);
    } else {
      System.out.println(ERROR + ": " + responseString);
      assertTrue(false);
    }

    // Test #READ (invalid token)
    System.out.print("\t#READ INVTOK (" + id + ")" + ARR);

    response = ownStorageClient.readResponse(id, invalidToken, log);
    responseString = response.getStatus() + " " + response.getStatusInfo().getReasonPhrase();
    if (response.getStatus() == Status.OK.getStatusCode()) {
      System.out.println(OK + ": " + responseString);
    } else {
      System.out.println(ERROR + ": " + responseString);
      assertTrue(false);
    }

    // TEST #CHECKACCESS/DELETE
    System.out.print("\t#CHECKACCESS/" + DariahStorageClient.CHECKACCESS_OPERATION_DELETE + " ("
        + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    access = ownStorageClient.checkAccess(id, token, logID + t,
        DariahStorageClient.CHECKACCESS_OPERATION_DELETE);

    if (access) {
      assertTrue(false);
    }

    System.out.println(access);

    // Test #DELETE (must NOT be possible!)
    try {
      System.out.print("\t#DELETE (" + id + ")" + ARR);

      ownStorageClient.deleteFile(id, token, log);
    } catch (IOException e) {
      System.out.println(OK + ": " + e.getMessage());
    }

    // TEST #UNPUBLISH
    System.out.print("\t#UNPUBLISH (" + id + ")" + ARR);

    ownStorageClient.unpublish(id, token, log);

    System.out.println(OK);

    // Test #READ (again with no token)
    System.out.print("\t#READ NOTOK (" + id + ")" + ARR);

    response = ownStorageClient.readResponse(id, null, log);
    if (response.getStatus() != Status.OK.getStatusCode()) {
      System.out.println(
          OK + ": " + response.getStatus() + " " + response.getStatusInfo().getReasonPhrase());
    }

    // Test #READ (again with invalid token)
    System.out.print("\t#READ INVTOK (" + id + ")" + ARR);

    response = ownStorageClient.readResponse(id, invalidToken, log);
    if (response.getStatus() != Status.OK.getStatusCode()) {
      System.out.println(
          OK + ": " + response.getStatus() + " " + response.getStatusInfo().getReasonPhrase());
    }

    // TEST #CHECKACCESS/DELETE
    System.out.print("\t#CHECKACCESS/" + DariahStorageClient.CHECKACCESS_OPERATION_DELETE + " ("
        + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    access = ownStorageClient.checkAccess(id, token, logID + t,
        DariahStorageClient.CHECKACCESS_OPERATION_DELETE);

    if (!access) {
      assertTrue(false);
    }

    System.out.println(access);

    // #DELETE
    System.out.print("\t#DELETE (" + id + ")" + ARR);

    ownStorageClient.deleteFile(id, token, log);

    System.out.println(OK);
  }

  /**
   * <p>
   * Testing #list?public=false method of DARIAH-DE service online.
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testStorageOnlineList() throws IOException {

    System.out.println(LINES + " testStorageOnlineList()");

    // TEST #LIST OwnStorage
    System.out.print("\t#LIST OWNSTORAGE" + ARR);

    String t = String.valueOf(System.currentTimeMillis());
    String ownStorageListALL = ownStorageClient.listResources(token, logID + t);
    String ownStorageListPUBLIC = ownStorageClient.listPublicResources(token, logID + t);
    if (ownStorageListPUBLIC.contains("\"public\":false")) {
      assertTrue(false);
    }
    String ownStorageListNONPUBLIC = ownStorageClient.listNonpublicResources(token, logID + t);
    if (ownStorageListNONPUBLIC.contains("\"public\":true")) {
      assertTrue(false);
    }
    int ownCountALL = ownStorageListALL.split("ownStorage").length - 1;
    int ownCountPUBLIC = ownStorageListPUBLIC.split("ownStorage").length - 1;
    int ownCountNONPUBLIC = ownStorageListNONPUBLIC.split("ownStorage").length - 1;
    int ownCountSUM = ownCountNONPUBLIC + ownCountPUBLIC;

    System.out.println("public=" + ownCountPUBLIC + ", nonpublic=" + ownCountNONPUBLIC + ", all="
        + ownCountALL + ", sum=" + ownCountSUM);

    if (ownCountALL != ownCountSUM) {
      assertTrue(false);
    }

    // TEST #LIST PublicStorage
    System.out.print("\t#LIST PUBLICSTORAGE" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    String publicStorageListALL = publicStorageClient.listResources(token, logID + t);
    String publicStorageListPUBLIC = publicStorageClient.listPublicResources(token, logID + t);
    if (publicStorageListPUBLIC.contains("\"public\":false")) {
      assertTrue(false);
    }
    String publicStorageListNONPUBLIC =
        publicStorageClient.listNonpublicResources(token, logID + t);
    if (publicStorageListNONPUBLIC.contains("\"public\":true")) {
      assertTrue(false);
    }
    int publicCountALL = publicStorageListALL.split("ownStorage").length - 1;
    int publicCountPUBLIC = publicStorageListPUBLIC.split("ownStorage").length - 1;
    int publicCountNONPUBLIC = publicStorageListNONPUBLIC.split("ownStorage").length - 1;
    int publicCountSUM = publicCountNONPUBLIC + publicCountPUBLIC;

    System.out.println("public=" + publicCountPUBLIC + ", nonpublic=" + publicCountNONPUBLIC
        + ", all=" + publicCountALL + ", sum=" + publicCountSUM);

    if (publicCountALL != publicCountSUM) {
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Testing #auth/info method of DARIAH-DE OwnStorage service online.
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testStorageOnlineAuthinfo() throws IOException {

    System.out.println(LINES + " testStorageOnlineAuthinfo()");

    // TEST #AUTHINFO OwnStorage
    String expectedOwnStorageEppn = eppn;

    System.out.print("\t#AUTHINFO OWNSTORAGE" + ARR);

    String t = String.valueOf(System.currentTimeMillis());
    String ownStorageAuthinfo = ownStorageClient.authInfo(token, logID + t);
    AuthInfo authInfo = jsonObjectMapper.readValue(ownStorageAuthinfo, AuthInfo.class);
    String eppn = authInfo.principal.name;

    System.out.println(
        "\tOwnStorage ePPN: " + eppn + " = OwnStorage EXPECTED ePPN: " + expectedOwnStorageEppn);

    System.out.println(authInfo);

    if (!eppn.equals(expectedOwnStorageEppn)) {
      assertTrue(false);
    }

    // TEST #AUTHINFO PublicStorage
    String expectedPublicStorageEppn = eppn;

    System.out.print("\t#AUTHINFO PUBLICSTORAGE" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    String publicStorageAuthinfo = publicStorageClient.authInfo(token, logID + t);
    authInfo = jsonObjectMapper.readValue(publicStorageAuthinfo, AuthInfo.class);
    eppn = authInfo.principal.name;

    System.out.println("\tPublicStorage ePPN: " + eppn + " = PublicStorage EXPECTED ePPN: "
        + expectedPublicStorageEppn);

    if (!eppn.equals(expectedPublicStorageEppn)) {
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Testing #auth/info method of DARIAH-DE OwnStorage service online.
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testStorageTokenXpiration() throws IOException {

    System.out.println(LINES + " testStorageTokenXpiration()");

    // TEST #AUTH/INFO OwnStorage
    System.out.print("\t#AUTHINFO OWNSTORAGE" + ARR);

    String t = String.valueOf(System.currentTimeMillis());
    String ownStorageAuthinfo = ownStorageClient.authInfo(token, logID + t);
    AuthInfo authInfo = jsonObjectMapper.readValue(ownStorageAuthinfo, AuthInfo.class);
    long tokenXpiresIn = authInfo.expiresIn;

    System.out.println("OwnStorage token xpires on " + (new Date(tokenXpiresIn)) + " (in app. "
        + (tokenXpiresIn - System.currentTimeMillis()) / 1000 / 60 / 60 + " h)");

    // TEST #AUTH/INFO PublicStorage
    System.out.print("\t#AUTHINFO PUBLICSTORAGE" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    String publicStorageAuthinfo = publicStorageClient.authInfo(token, logID + t);
    authInfo = jsonObjectMapper.readValue(publicStorageAuthinfo, AuthInfo.class);
    tokenXpiresIn = authInfo.expiresIn;

    System.out.println("PublicStorage token xpires on " + (new Date(tokenXpiresIn)) + " (in app. "
        + (tokenXpiresIn - System.currentTimeMillis()) / 1000 / 60 / 60 + " h)");
  }

  /**
   * <p>
   * Testing #checkAccess methods of DARIAH-DE OwnStorage service online.
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testOwnStorageOnlineCheckAccess() throws IOException {

    System.out.println(LINES + " testOwnStorageOnlineCheckAccess()");

    // TEST #CREATE
    System.out.print("\t#CREATE" + ARR);

    String t = String.valueOf(System.currentTimeMillis());
    String id = ownStorageClient.createFile(new FileInputStream(testPNG), TextGridMimetypes.PNG,
        token, logID + t);

    System.out.println(ownStorageClient.getStorageUri() + id);

    // TEST #CHECKACCESS/READ
    System.out.print("\t#CHECKACCESS/" + DariahStorageClient.CHECKACCESS_OPERATION_READ + " ("
        + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    boolean accessRead = ownStorageClient.checkAccess(id, token, logID + t,
        DariahStorageClient.CHECKACCESS_OPERATION_READ);

    if (!accessRead) {
      assertTrue(false);
    }

    System.out.println(accessRead);

    // TEST #CHECKACCESS/WRITE
    System.out.print("\t#CHECKACCESS/" + DariahStorageClient.CHECKACCESS_OPERATION_WRITE + " ("
        + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    boolean accessWrite = ownStorageClient.checkAccess(id, token, logID + t,
        DariahStorageClient.CHECKACCESS_OPERATION_WRITE);

    if (!accessWrite) {
      assertTrue(false);
    }

    System.out.println(accessWrite);

    // TEST #CHECKACCESS/DELETE
    System.out.print("\t#CHECKACCESS/" + DariahStorageClient.CHECKACCESS_OPERATION_DELETE + " ("
        + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    boolean accessDelete = ownStorageClient.checkAccess(id, token, logID + t,
        DariahStorageClient.CHECKACCESS_OPERATION_DELETE);

    if (!accessDelete) {
      assertTrue(false);
    }

    System.out.println(accessDelete);

    // TEST #CHECKACCESS/READ
    System.out.print("\t#CHECKACCESS/" + DariahStorageClient.CHECKACCESS_OPERATION_READ + " ("
        + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    boolean accessNONONO = ownStorageClient.checkAccess(id, invalidToken, logID + t,
        DariahStorageClient.CHECKACCESS_OPERATION_READ);

    if (accessNONONO) {
      assertTrue(false);
    }

    System.out.println(accessNONONO);


    // TEST #DELETE
    System.out.print("\t#DELETE (" + id + ")" + ARR);

    t = String.valueOf(System.currentTimeMillis());
    ownStorageClient.deleteFile(id, token, logID + t);

    System.out.println(OK);
  }

  /**
   * <p>
   * DARIAH Storage Client seems to be streaming already!
   * </p>
   * 
   * @throws IOException
   */
  @Test
  @Ignore
  public void testDariahStorageClientReadFile() throws IOException {

    DariahStorageClient storageClient = new DariahStorageClient();

    // fischinger.mv4
    URI uri = URI.create("https://cdstar.de.dariah.eu/test/public/EAEA0-6570-FE58-EC1B-0");

    URI endpoint = URI.create(uri.getScheme() + "://" + uri.getHost());
    String path = uri.getPath();

    System.out.println(LINES);
    System.out.println(uri);
    System.out.println(endpoint);
    System.out.println(path);
    System.out.println(LINES);

    storageClient.setStorageUri(endpoint);
    InputStream response = storageClient.readFile(path);

    // WRITE STREAM TO ZIP FILE
    long start = System.currentTimeMillis();

    File f = new File("/Users/fugu/Desktop/movie.zip");
    try (FileOutputStream fos = new FileOutputStream(f)) {
      org.apache.cxf.helpers.IOUtils.transferTo(response, f);
      fos.close();
    }

    System.out.println("duration for " + f.length() + ": " + (System.currentTimeMillis() - start));

    // GET DATA INPUTSTREAM FROM ZIP FILE DATA ENTRY
    InputStream i = getDHCrudDariahObjectDataStream(new ZipFile(f));

    // STORE DATA FILE
    try (FileOutputStream dataFileStream =
        new FileOutputStream(new File("/Users/fugu/Desktop/movie.ogx"))) {
      IOUtils.copyLarge(i, dataFileStream);
      dataFileStream.close();

      System.out.println(LINES);
    }
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param theZipStream
   * @return
   * @throws IOException
   */
  private static InputStream getDHCrudDariahObjectDataStream(ZipFile theZipFile)
      throws IOException {

    // Create piped streams.
    PipedInputStream result = new PipedInputStream();
    final PipedOutputStream out = new PipedOutputStream(result);

    // Go through every ZIP entry, read data entry only!
    Enumeration<? extends ZipEntry> entries = theZipFile.entries();
    while (entries.hasMoreElements()) {
      ZipEntry entry = entries.nextElement();

      if (entry.getName().substring(entry.getName().lastIndexOf(File.separatorChar) + 1)
          .startsWith("data")) {

        System.out.println("reading data: " + entry.getName() + " (" + entry.getSize() + ")");

        // Now do read file from ZIP stream, start a new thread that writes to the Pipe
        // (OutputStream > InputStream)...
        Thread thread = new Thread("ZIPFileOutputInputPipe") {
          @Override
          public void run() {
            try (InputStream in = theZipFile.getInputStream(entry)) {
              IOUtils.copy(in, out);
            } catch (IOException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }
          }
        };

        thread.start();
      }
    }

    return result;
  }

  /**
   * @param resPart
   * @return
   * @throws IOException
   */
  public static File getResource(String resPart) throws IOException {

    File res;

    // If we have an absolute resPart, just return the file.
    if (resPart.startsWith(File.separator)) {
      return new File(resPart);
    }

    URL url = ClassLoader.getSystemClassLoader().getResource(resPart);
    if (url == null) {
      throw new IOException("Resource '" + resPart + "' not found");
    }
    try {
      res = new File(url.toURI());
    } catch (URISyntaxException ue) {
      res = new File(url.getPath());
    }

    return res;
  }

}
