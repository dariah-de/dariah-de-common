/**
 * This software is copyright (c) 2021 by
 *
 * TextGrid Consortium (https://textgrid.de)
 *
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.common;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.stream.XMLStreamException;
import org.apache.jena.rdf.model.LiteralRequiredException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.NsIterator;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFList;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import org.apache.jena.util.iterator.ExtendedIterator;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.LTPUtils;

/**
 * TODOLOG
 *
 **
 * CHANGELOG
 *
 * 2021-03-10 - Funk - Add Exception handling for reading RDF models.
 *
 * 2018-12-14 - Funk - Add method remove with value param, too.
 *
 * 2017-08-29 - Funk - Refactor method addToModel, using Resource type as value now.
 *
 * 2017-08-02 - Funk - Add PREMIS and DCTERMS elements.
 *
 * 2017-07-14 - Funk - Refactor a little bit, generalised methods.
 *
 * 2015-08-12 - Funk - First version.
 */

/**
 * <p>
 * RDF utilities for accessing Jena RDF models.
 * </p>
 *
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-03-16
 * @since 2015-08-12
 */

public class RDFUtils {

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Finds all S for a given P and O (? P O query) in a given model.
   * </p>
   *
   * @param theModel
   * @param thePrefix
   * @param theElement
   * @return
   */
  public static List<String> findAllSubjects(final Model theModel, final String thePrefix,
      final String theElement) {
    return findAllSubjects(theModel, null, thePrefix, theElement);
  }

  /**
   * <p>
   * Finds all S for a given P and O (? P O query) for a given resource in a given model.
   * </p>
   *
   * @param theModel
   * @param theResource
   * @param thePrefix
   * @param theElement
   * @return All subject values, empty list otherwise.
   */
  public static List<String> findAllSubjects(final Model theModel, final String theResource,
      final String thePrefix, final String theElement) {

    List<String> result = new ArrayList<String>();
    Property p = theModel.createProperty(theElement);

    if (!thePrefix.equals("")) {
      p = theModel.createProperty(theModel.getNsPrefixURI(thePrefix), theElement);
    }
    Resource r = (Resource) null;
    if (theResource != null) {
      r = theModel.createResource(theResource);
    }
    StmtIterator iter = theModel.listStatements(null, p, r);
    while (iter.hasNext()) {
      Statement s = iter.next();
      result.add(s.getSubject().toString());
    }

    return result;
  }

  /**
   * <p>
   * Finds the first S for a given P and O (? P O query) for given resource in given model.
   * </p>
   *
   * @param theModel
   * @param theResource
   * @param thePrefix
   * @param theElement
   * @return
   */
  public static String findFirstSubject(final Model theModel, final String theResource,
      final String thePrefix, final String theElement) {

    String result = "";

    List<String> elements = findAllSubjects(theModel, theResource, thePrefix, theElement);
    if (!elements.isEmpty()) {
      result = elements.get(0);
    }

    return result;
  }

  /**
   * <p>
   * Finds all O for the first P ( ? P ? query) in a given model.
   * </p>
   *
   * @param theModel
   * @param thePrefix
   * @param theElement
   * @return All the object values, if existing, an empty list otherwise.
   */
  public static List<String> findAllObjects(final Model theModel, final String thePrefix,
      final String theElement) {
    return findAllObjects(theModel, null, thePrefix, theElement, null);
  }

  /**
   * <p>
   * Finds all O for the first P ( ? P ? query) for a given resource in a given model.
   * </p>
   *
   * @param theModel
   * @param theResource
   * @param thePrefix
   * @param theElement
   * @return All the object values, if existing, an empty list otherwise.
   */
  public static List<String> findAllObjects(final Model theModel, final String theResource,
      final String thePrefix, final String theElement) {
    return findAllObjects(theModel, theResource, thePrefix, theElement, null);
  }

  /**
   * <p>
   * Finds all O for the first P ( ? P ? query) for a given resource in a given model, that starts
   * with theStart (if given).
   * </p>
   *
   * @param theModel
   * @param theResource
   * @param thePrefix
   * @param theElement
   * @param theStart
   * @return All the object values, if existing, an empty list otherwise.
   */
  public static List<String> findAllObjects(final Model theModel, final String theResource,
      final String thePrefix, final String theElement, final String theStart) {

    List<String> result = new ArrayList<String>();

    Property p = theModel.createProperty(theElement);

    if (!thePrefix.equals("")) {
      p = theModel.createProperty(theModel.getNsPrefixURI(thePrefix), theElement);
    }
    Resource r;
    StmtIterator iter;
    if (theResource != null) {
      r = theModel.getResource(theResource);
      iter = r.listProperties(p);
    } else {
      r = (Resource) null;
      iter = theModel.listStatements(null, p, r);
    }
    while (iter.hasNext()) {
      Statement statement = iter.next();
      String s = getLiteralOrString(statement);
      // Try if statement is a literal, get as literal lexical form to get correct escaping of chars
      // such as \"!
      if (theStart == null) {
        result.add(s);
      } else {
        if (s.startsWith(theStart)) {
          result.add(s);
        }
      }
    }

    return result;
  }

  /**
   * <p>
   * Finds O for the first P ( ? P ? query) in a given model.
   * </p>
   *
   * @param theModel
   * @param thePrefix
   * @param theElement
   * @return The first object value, empty string otherwise.
   */
  public static String findFirstObject(final Model theModel, final String thePrefix,
      final String theElement) {
    return findFirstObject(theModel, null, thePrefix, theElement, null);
  }

  /**
   * <p>
   * Extracts the value of the first occurrence of the given prefix and element in the given
   * resource and RDF data string ( S P ? query).
   * </p>
   *
   * @param theData
   * @param thePrefix
   * @param theElement
   * @return The value of the given element and prefix if existing, empty string if not.
   * @throws ParseException
   */
  public static String findFirstObject(final String theData, final String thePrefix,
      final String theElement) throws ParseException {
    return RDFUtils.findFirstObject(RDFUtils.readModel(theData), null, thePrefix, theElement, null);
  }

  /**
   * <p>
   * Extracts the value of the first occurrence of the given prefix and element in the given
   * resource and given model.
   * </p>
   *
   * @param theModel
   * @param theResource
   * @param thePrefix
   * @param theElement
   * @return The value of the given element and prefix that starts with the given starting string if
   *         existing, empty string if not.
   */
  public static String findFirstObject(final Model theModel, final String theResource,
      final String thePrefix, final String theElement) {
    return RDFUtils.findFirstObject(theModel, theResource, thePrefix, theElement, null);
  }

  /**
   * <p>
   * Extracts the value of the first occurrence that starts with "theStart" of the given prefix and
   * element in the given resource and RDF data string.
   * </p>
   *
   * @param theData
   * @param theResource
   * @param thePrefix
   * @param theElement
   * @param theStart
   * @return The value of the given element and prefix that starts with the given starting string if
   *         existing, empty string if not.
   * @throws ParseException
   */
  public static String findFirstObject(final String theData, final String theResource,
      final String thePrefix, final String theElement, final String theStart)
      throws ParseException {
    return RDFUtils.findFirstObject(RDFUtils.readModel(theData), theResource, thePrefix, theElement,
        theStart);
  }

  /**
   * <p>
   * Extracts the value of the first occurrence that starts with "theStart" of the given prefix and
   * element in the given resource and RDF data model.
   * </p>
   *
   * @param theModel
   * @param theResource
   * @param thePrefix
   * @param theElement
   * @param theStart
   * @return The first object value, empty string otherwise.
   */
  public static String findFirstObject(final Model theModel, final String theResource,
      final String thePrefix, final String theElement, final String theStart) {

    String result = "";

    List<String> elements = findAllObjects(theModel, theResource, thePrefix, theElement, theStart);
    if (!elements.isEmpty()) {
      result = elements.get(0);
    }

    return result;
  }

  /**
   * <p>
   * Reads an RDF model from the given input string.
   * </p>
   *
   * @param theData
   * @return The model read from the given string.
   * @throws ParseException
   */
  public static Model readModel(final String theData) throws ParseException {
    return readModel(theData, null);
  }

  /**
   * <p>
   * Reads an RDF model from the given input string.
   * </p>
   *
   * @param theData
   * @param theLang
   * @return The model read from the given string.
   * @throws ParseException
   */
  public static Model readModel(final String theData, final String theLang) throws ParseException {

    Model result = ModelFactory.createDefaultModel();

    try {
      result.read(new StringReader(theData), null, theLang);
    } catch (RuntimeException e) {
      throw new ParseException(e.getMessage());
    }

    return result;
  }

  /**
   * <p>
   * Copies a model to a new one for further use, setting the new value for the rdf:about attribute.
   * </p>
   *
   * @param theSourceModel
   * @param theResource
   * @return The newly copied model.
   */
  public static Model copyModel(final Model theSourceModel, final String theResource) {

    // Create destination model.
    Model result = ModelFactory.createDefaultModel();
    Resource r = result.createResource(theResource);
    result.setNsPrefixes(theSourceModel.getNsPrefixMap());

    // Loop all statements and copy to new model.
    StmtIterator iter =
        theSourceModel.listStatements(new SimpleSelector(null, null, (RDFNode) null));
    while (iter.hasNext()) {
      Statement s = iter.next();
      r.addProperty(s.getPredicate(), s.getObject());
    }

    return result;
  }

  /**
   * @param theModel
   * @param theResource
   * @param thePrefix
   * @param theElement
   * @param theLiteral
   */
  public static void addToModel(Model theModel, final String theResource, final String thePrefix,
      final String theElement, final String theLiteral) {
    addLiteralToModel(theModel, theResource, thePrefix, theElement, theLiteral);
  }

  /**
   * <p>
   * Adds a literal (string) element to the given model, resource, prefix, and element.
   * </p>
   *
   * @param theModel
   * @param theResource
   * @param thePrefix
   * @param theElement
   * @param theLiteral
   */
  public static void addLiteralToModel(Model theModel, final String theResource,
      final String thePrefix, final String theElement, final String theLiteral) {
    theModel.getResource(theResource).addProperty(
        theModel.createProperty(theModel.getNsPrefixURI(thePrefix), theElement), theLiteral);
  }

  /**
   * <p>
   * Adds a resource element to the given model, resource, prefix, and element.
   * </p>
   *
   * @param theModel
   * @param theResource
   * @param thePrefix
   * @param theElement
   * @param theValue
   */
  public static void addResourceToModel(Model theModel, final String theResource,
      final String thePrefix, final String theElement, final Resource theValue) {
    theModel.getResource(theResource).addProperty(
        theModel.createProperty(theModel.getNsPrefixURI(thePrefix), theElement), theValue);
  }

  /**
   * <p>
   * Removes ALL values from the given model, resource, prefix, and element.
   * </p>
   *
   * @param theModel
   * @param theResource
   * @param thePrefix
   * @param theElement
   */
  public static void removeFromModel(Model theModel, final String theResource,
      final String thePrefix, final String theElement) {

    Resource s = theModel.getResource(theResource);
    Property p = theModel.createProperty(theModel.getNsPrefixURI(thePrefix), theElement);
    RDFNode o = (RDFNode) null;

    StmtIterator i = theModel.listStatements(s, p, o);

    theModel.remove(i);
  }

  /**
   * <p>
   * Removes ALL values from the given model with the given value, resource, prefix, and element.
   * </p>
   *
   * @param theModel
   * @param theResource
   * @param thePrefix
   * @param theElement
   */
  public static void removeFromModel(Model theModel, final String theResource,
      final String thePrefix, final String theElement, final String theValue) {

    Resource s = theModel.getResource(theResource);
    Property p = theModel.createProperty(theModel.getNsPrefixURI(thePrefix), theElement);
    String o = theValue;

    StmtIterator i = theModel.listStatements(s, p, o);

    theModel.remove(i);
  }

  /**
   * @param theModel
   * @return The model as a RDF/XML string representation.
   */
  public static String getStringFromModel(final Model theModel) {
    return getStringFromModel(theModel, RDFConstants.RDF_XML);
  }

  /**
   * @param theModel
   * @return The model as a Turtle string representation.
   */
  public static String getTTLFromModel(final Model theModel) {
    return getStringFromModel(theModel, RDFConstants.TURTLE);
  }

  /**
   * @param theModel
   * @param theFormat
   * @return The model as a string representation in the given format.
   */
  public static String getStringFromModel(final Model theModel, String theFormat) {

    String result = "";

    try {
      Writer writer = new StringWriter();
      theModel.write(writer, theFormat);
      result = writer.toString();
      writer.close();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return result;
  }

  /**
   * @param theResource
   * @param thePrefix
   * @param theNamespace
   * @return The new model using all the given parameters.
   */
  public static Model newModel(final String theResource, final String thePrefix,
      final String theNamespace) {

    Model result = ModelFactory.createDefaultModel();

    result.createResource(theResource);
    Map<String, String> prefixMap = new HashMap<String, String>();
    prefixMap.put(thePrefix, theNamespace);
    result.setNsPrefixes(prefixMap);

    return result;
  }

  /**
   * @param theModel
   * @param theResource
   * @param thePrefix
   * @param thePredicate
   * @param theStart
   * @return
   * @throws XMLStreamException
   * @throws IOException
   */
  public static String getFirstProperty(Model theModel, String theResource, String thePrefix,
      String thePredicate, String theStart) throws XMLStreamException, IOException {

    String result = "";

    for (String str : getProperties(theModel, theResource, thePrefix, thePredicate)) {
      if (str.toString().startsWith(theStart)) {
        result = str;
        break;
      }
    }

    return result;
  }

  /**
   * @param theModel
   * @param theResource
   * @param thePrefix
   * @param thePredicate
   * @return
   * @throws IOException
   * @throws XMLStreamException
   */
  public static List<String> getProperties(Model theModel, String theResource, String thePrefix,
      String thePredicate) throws XMLStreamException, IOException {
    return getProperties(theModel, theResource, thePrefix, thePredicate, null);
  }

  /**
   * <p>
   * Gets a list of URIs from RDF model.
   * </p>
   *
   * @param theModel
   * @param theResource
   * @param thePrefix
   * @param thePredicate
   * @return A list of URIs if params applicable, empty list otherwise.
   * @throws XMLStreamException
   * @throws IOException
   */
  public static List<String> getProperties(Model theModel, String theResource, String thePrefix,
      String thePredicate, String theStart) throws XMLStreamException, IOException {

    List<String> result = new ArrayList<String>();

    // Get all predicate URIs out of the model.
    Resource res = theModel.getResource(theResource);
    Property p = theModel.createProperty(theModel.getNsPrefixURI(thePrefix), thePredicate);

    // Loop all the predicates.
    StmtIterator iter = res.listProperties(p);
    while (iter.hasNext()) {
      Statement s = iter.next();

      // Get list from RDF node, if we do have a list entry (RDF:Collection). The list will be empty
      // if object is not anonymous.
      List<String> l = getRDFList(s.getObject());
      for (String str : l) {
        if (theStart == null || (theStart != null && str.startsWith(theStart))) {
          result.add(str);
        }
      }
    }

    return result;
  }

  /**
   * <p>
   * Removes a complete list of hasParts from the given model for the given resource. Does remove
   * the hasPart relation, too!
   * </p>
   *
   * @param theModel
   * @param theResource
   */
  public static void removeHasPartListFromModel(Model theModel, String theResource) {

    // Get all hasPart predicate URIs out of the model for the given resource.
    Resource res = theModel.getResource(theResource);
    Property p =
        theModel.createProperty(RDFConstants.DCTERMS_NAMESPACE, RDFConstants.ELEM_DCTERMS_HASPART);

    // Loop all the hasParts.
    StmtIterator iter = res.listProperties(p);
    while (iter.hasNext()) {
      Statement s = iter.next();
      // Get list from RDF node, if we do have a list entry (RDF:Collection) by assuming we have got
      // a list if object is anonymous.
      if (s.getObject().isAnon()) {
        RDFList list = s.getObject().as(RDFList.class);
        list.removeList();
      }
    }

    // Finally also delete hasPart relation.
    removeFromModel(theModel, theResource, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DCTERMS_HASPART);
  }

  /**
   * @param theModel
   */
  public static void removeUnusedNamespacesFromModel(Model theModel) {

    // Create new prefix map.
    Map<String, String> newPrefixMap = new HashMap<String, String>();

    // Iterate all used namespaces and fill new map.
    NsIterator iter = theModel.listNameSpaces();
    while (iter.hasNext()) {
      String namespace = iter.next();
      String prefix = theModel.getNsURIPrefix(namespace);
      if (prefix == null) {
        // If prefix is null, take urglimurgli UUID prefix :-D
        prefix = "urglimurgli." + UUID.randomUUID().toString().replaceAll("-", "");
      }
      newPrefixMap.put(prefix, namespace);
    }

    // Purge old map and set new one with only used prefixes and namespaces.
    theModel.clearNsPrefixMap();
    theModel.setNsPrefixes(newPrefixMap);
  }

  /**
   * @param theModel
   * @param theUri
   * @param thePrefixes
   * @param exchangeNamespaceAndPrefix
   * @return
   */
  public static Model getModelContainingOnlyLiterals(Model theModel, String theUri,
      List<String> thePrefixes, boolean exchangeNamespaceAndPrefix) {
    return getModelContainingOnlyLiterals(theModel, theUri, thePrefixes, exchangeNamespaceAndPrefix,
        null);
  }

  /**
   * <p>
   * Converts RDF resources into literals and exchanges namespaces to prefixes, if needed.
   * </p>
   *
   * @param theModel
   * @param theUri
   * @param thePrefixes
   * @param exchangeNamespaceAndPrefix
   * @param newResourceName
   * @return
   */
  public static Model getModelContainingOnlyLiterals(Model theModel, String theUri,
      List<String> thePrefixes, boolean exchangeNamespaceAndPrefix, String newResourceName) {

    // Create new model for given URIs RDF statements.
    Model result = ModelFactory.createDefaultModel();

    // Add all namespaces from source model.
    result.setNsPrefixes(theModel.getNsPrefixMap());

    // Loop all the statements for URI.
    StmtIterator iter = theModel.listStatements(theModel.getResource(theUri), null, (RDFNode) null);

    // Create new resource name for result model, if given.
    Resource r;
    if (newResourceName != null && !newResourceName.isEmpty()) {
      r = result.createResource(newResourceName);
    } else {
      r = result.createResource(theUri);
    }

    // Use all metadata from the given model.
    while (iter.hasNext()) {
      Statement s = iter.next();
      String prefix = theModel.getNsURIPrefix(s.getPredicate().getNameSpace());
      if (thePrefixes.contains(prefix)) {
        String value = getLiteralOrString(s);
        if (exchangeNamespaceAndPrefix) {
          value =
              value.replace(RDFConstants.DARIAH_COLLECTION + "/", RDFConstants.HDL_PREFIX + ":");
          value = value.replace(RDFConstants.HDL_NAMESPACE, RDFConstants.HDL_PREFIX + ":");
          value = value.replace(RDFConstants.DOI_NAMESPACE, RDFConstants.DOI_PREFIX + ":");
        }
        // Add as literals, not resources!
        result.addLiteral(r, s.getPredicate(), result.createLiteral(value));
      }
    }

    // Purge unused namespaces.
    RDFUtils.removeUnusedNamespacesFromModel(result);

    return result;
  }

  /**
   * <p>
   * Checks if an object with given URI is a collection (RDF type must end with "Collection", so we
   * cover "http://de.dariah.eu/rdf/dataobjects/terms/Collection" and "dariah:Collection").
   * </p>
   * 
   * <p>
   * As URI we need either an "hdl:xyz123" or an "http://hdl.handle.net/xyz123" value!
   * </p>
   *
   * @param theModel
   * @param theUri
   * @return
   */
  public static boolean isCollection(Model theModel, String theUri) {
    return (findFirstObject(theModel, theUri, RDFConstants.RDF_PREFIX, RDFConstants.ELEM_DC_TYPE)
        .endsWith(RDFConstants.ELEM_DARIAH_COLLECTION)
        || findFirstObject(theModel, LTPUtils.resolveHandlePid(theUri), RDFConstants.RDF_PREFIX,
            RDFConstants.ELEM_DC_TYPE).endsWith(RDFConstants.ELEM_DARIAH_COLLECTION));
  }

  /**
   * <p>
   * Takes an RDFNode containing (i) a RDF:Collection (anon), or (ii) a single URI.
   * </p>
   *
   * @param theNode
   * @return A LinkedList containing all the given strings in the given RDF node.
   */
  public static List<String> getRDFList(final RDFNode theNode) {

    List<String> result = new ArrayList<String>();

    // If the node is a RDF Blank, we got (hopefully) a RDF:Collection, we treat that as LinkedList,
    // it is ordered!
    if (theNode.isAnon()) {
      RDFList list = theNode.as(RDFList.class);
      // FIXME Use list.asJavaList() here?!
      ExtendedIterator<RDFNode> exit = list.iterator();
      while (exit.hasNext()) {
        result.add(exit.next().toString());
      }
    }
    // If not, we have got a single URI. The list will have a single entry then.
    else {
      result.add(theNode.toString());
    }

    return result;
  }

  /**
   * <p>
   * Check if dc:date is invalid, remove it, if.
   * </p>
   *
   * @param theModel
   * @return
   * @throws IOException
   * @throws XMLStreamException
   * @throws DatatypeConfigurationException
   */
  public static Model removeDateIfNotXSDDateTimeConform(String theNamespaceURI, Model theModel)
      throws XMLStreamException, IOException, DatatypeConfigurationException {

    List<String> dcdateList =
        RDFUtils.getProperties(theModel, theNamespaceURI, RDFConstants.DC_PREFIX,
            RDFConstants.ELEM_DC_DATE);

    DatatypeFactory dtf = DatatypeFactory.newInstance();
    if (!dcdateList.isEmpty()) {
      for (String date : dcdateList) {
        if (date != null && date != "") {
          try {
            dtf.newXMLGregorianCalendar(date);
          } catch (IllegalArgumentException e) {
            RDFUtils.removeFromModel(theModel, theNamespaceURI, RDFConstants.DC_PREFIX,
                RDFConstants.ELEM_DC_DATE, date);
          }
        }
      }
    }

    return theModel;
  }

  /**
   * <p>
   * Prepares the DMD metadata for JSON creation. ElasticSearch doesn't like (i) false formatted
   * dates and (ii) we do need a literal-only RDF model for transforming RDF XML to JSON!
   * </p>
   *
   * @param theURI
   * @param theDMDModel
   * @return
   * @throws XMLStreamException
   * @throws IOException
   * @throws DatatypeConfigurationException
   */
  public static Model prepareDMDforJSONCreation(URI theURI, Model theDMDModel)
      throws XMLStreamException, IOException, DatatypeConfigurationException {

    Model result;

    String namespaceUri = LTPUtils.resolveHandlePid(theURI);

    // Remove invalid (and optional) dc:date fields, ElasticSearch doesn't like it!
    result = removeDateIfNotXSDDateTimeConform(namespaceUri, theDMDModel);

    // Create DMD Model for esutils.
    List<String> dmdPrefixes = new ArrayList<String>();
    dmdPrefixes.add(RDFConstants.DC_PREFIX);
    result = RDFUtils.getModelContainingOnlyLiterals(theDMDModel, namespaceUri, dmdPrefixes, true);

    return result;
  }

  /**
   * @param theURI
   * @param theADMMDModel
   * @return
   * @throws XMLStreamException
   * @throws IOException
   * @throws DatatypeConfigurationException
   */
  public static Model prepareADMMDforJSONCreation(URI theURI, Model theADMMDModel)
      throws XMLStreamException, IOException, DatatypeConfigurationException {

    Model result;

    String namespaceUri = LTPUtils.resolveHandlePid(theURI);

    // Create ADMMD model for esutils.
    List<String> admmdPrefixes = new ArrayList<String>();
    admmdPrefixes.add(RDFConstants.DCTERMS_PREFIX);
    admmdPrefixes.add(RDFConstants.PREMIS_PREFIX);
    result =
        RDFUtils.getModelContainingOnlyLiterals(theADMMDModel, namespaceUri, admmdPrefixes, true);

    return result;
  }


  // **
  // PRIVATE
  // **

  /**
   * <p>
   * Gets a statement as literal, if literal, string otherwise.
   * </p>
   *
   * @param theStatement
   * @return
   */
  private static String getLiteralOrString(Statement theStatement) {
    try {
      return theStatement.getObject().asLiteral().getLexicalForm();
    } catch (LiteralRequiredException e) {
      return theStatement.getObject().toString();
    }
  }

}
