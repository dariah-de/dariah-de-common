/**
 * This software is copyright (c) 2021 by
 *
 * TextGrid Consortium (https://textgrid.de)
 *
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.common;

import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.stream.XMLStreamException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import info.textgrid.middleware.common.RDFConstants;

/**
 * TODOLOG
 *
 **
 * CHANGELOG
 *
 * 2017-07-25 - Funk - Added test for finding DOIs in metadata RDF.
 *
 * 2016-03-23 - Funk - Moved new methods and tests to DHPublishUtils.
 *
 * 2016-01-21 - Funk - Added new tests for new methods.
 *
 * 2015-08-12 - Funk - First version.
 */

/**
 * <p>
 * JUnit class for testing the RDFUtil implementation.
 * </p>
 *
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-03-16
 * @since 2015-09-28
 */

public class TestRDFUtils {

  // **
  // STATIC FINALS
  // **

  private static final String PID = "http://hdl.handle.net/11022/0000-0000-abcd-0";
  private static final String RDF_DATA = "<rdf:RDF"
      + " xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\""
      + " xmlns:dc=\"http://purl.org/dc/elements/1.1/\""
      + " xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\" >"
      + "<rdf:Description rdf:about=\"http://fugu.de/uri/of/resource\">"
      + "<dc:date>2015-08-06T16:02:41.187</dc:date>"
      + "<dc:identifier rdf:resource=\"" + PID + "\"/>\n"
      + "<rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>"
      + "<dc:format>image/png</dc:format>"
      + "<dc:title>southparkfugu</dc:title>"
      + "<dc:relation rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection/11022/0000-0000-wxyz-0\"/>"
      + "</rdf:Description>" + "</rdf:RDF>";
  private static final String RDF_DATA_MULTIPLE_IDENTIFIERS = "<rdf:RDF"
      + " xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\""
      + " xmlns:dc=\"http://purl.org/dc/elements/1.1/\""
      + " xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\" >"
      + "<rdf:Description rdf:about=\"http://fugu.de/uri/of/resource\">"
      + "<dc:date>2015-08-06T16:02:41.187</dc:date>"
      + "<dc:identifier rdf:resource=\"" + PID + "\"/>"
      + "<dc:identifier rdf:resource=\"http://hdl.handle.net/urgl-argl-aua\"/>"
      + "<dc:identifier rdf:resource=\"http://hdl.handle.net/URGL_ARGL_AUA\"/>"
      + "<rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>"
      + "<dc:format>image/png</dc:format>"
      + "<dc:title>southparkfugu</dc:title>"
      + "<dc:relation rdf:resource=\">http://de.dariah.eu/rdf/dataobjects/terms/Collection/11022/0000-0000-wxyz-0\"/>"
      + "</rdf:Description>" + "</rdf:RDF>";
  private static final String RDF_DATA_MULTIPLE = "<rdf:RDF"
      + " xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\""
      + " xmlns:dc=\"http://purl.org/dc/elements/1.1/\""
      + " xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\" >"
      + "<rdf:Description rdf:about=\"http://fugu.de/uri/of/resource\">"
      + "<dc:date>2015-08-06T16:02:41.187</dc:date>"
      + "<dc:identifier rdf:resource=\"" + PID + "\"/>"
      + "<rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>"
      + "<dc:format>image/png</dc:format>"
      + "<dc:title>southparkfugu</dc:title>"
      + "<dc:title>eine kleine dingsda-sammlung</dc:title>"
      + "<dc:creator>fugu</dc:creator>"
      + "<dc:creator>dingsda</dc:creator>"
      + "<dc:creator>stefan e. funk</dc:creator>"
      + "<dc:relation rdf:resource=\">http://de.dariah.eu/rdf/dataobjects/terms/Collection/11022/0000-0000-wxyz-0\"/>"
      + "</rdf:Description>" + "</rdf:RDF>";
  private static final String NEW_LITERAL_MODEL = "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
      + "\n" + "<fu:uri:of:resource>  dcterms:creator  \"fufu\" .";
  private static final String NEW_RESOURCE_MODEL =
      "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "\n"
          + "<http://fugu.de/uri/of/resource>  dcterms:creator  <http://fugu.de/uri/of/fufu/resource> .";
  private static final String DATA =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n" + "\n"
          + "<http://hdl.handle.net/11022/0000-0000-99E5-1>\n"
          + "        dcterms:hasPart  ( <http://hdl.handle.net/11022/0000-0000-99E4-2> <http://hdl.handle.net/11022/0000-0000-99E3-3> <http://hdl.handle.net/11022/0000-0000-99E2-4> <http://hdl.handle.net/11022/0000-0000-99E1-5> ) .";
  private static final String COLLECTION_DATA = "@prefix hdl:   <http://hdl.handle.net/> .\n"
      + "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
      + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
      + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
      + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
      + "<http://geobrowser.de.dariah.eu/storage/335701>\n"
      + "        a                   dariah:Collection ;\n"
      + "        dc:contributor      \"fugu\" ;\n"
      + "        dc:creator          \"fugu\" ;\n"
      + "        dc:description      \"fugu\" ;\n"
      + "        dc:format           \"text/tg.collection+tg.aggregation+xml\" ;\n"
      + "        dc:title            \"huhihufufifu\" ;\n"
      + "        dcterms:format      \"text/tg.collection+tg.aggregation+xml\" ;\n"
      + "        dcterms:hasPart    ( <http://geobrowser.de.dariah.eu/storage/335702> ) ;\n"
      + "        dcterms:identifier  <http://hdl.handle.net/11022/0000-0000-9D45-2> ;\n"
      + "        dcterms:source      \"http://geobrowser.de.dariah.eu/storage/335701\" .";
  private static final String RDF_DATA_SUBCOLLECTIONS =
      "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "dariahstorage:EAEA0-9386-D2E1-226C-0\n"
          + "        a                dariah:Collection ;\n"
          + "        dc:creator       \"fu\" ;\n"
          + "        dc:format        \"text/tg.collection+tg.aggregation+xml\" ;\n"
          + "        dc:rights        \"free\" ;\n"
          + "        dc:title         \"KOLLEKTION\" ;\n"
          + "        dcterms:format   \"text/tg.collection+tg.aggregation+xml\" ;\n"
          + "        dcterms:hasPart  ( dariahstorage:EAEA0-994F-A580-B42A-0 dariahstorage:EAEA0-DB19-ED64-6480-0 dariahstorage:EAEA0-0E7D-B850-1669-0 dariahstorage:EAEA0-17F8-7EBC-675B-0 ) ;\n"
          + "        dcterms:source   \"https://dariah-cdstar.gwdg.de/dariah/EAEA0-9386-D2E1-226C-0\" .\n"
          + "\n" + "dariahstorage:EAEA0-DB19-ED64-6480-0\n"
          + "        a                dariah:Collection ;\n"
          + "        dc:creator       \"fu\" ;\n"
          + "        dc:rights        \"free\" ;\n"
          + "        dc:title         \"UNTERKOLLEKTION\" ;\n"
          + "        dcterms:hasPart  ( dariahstorage:EAEA0-0ED8-B305-675C-0 dariahstorage:EAEA0-19AF-E233-D094-0 dariahstorage:EAEA0-CCE8-CC65-2F8F-0 dariahstorage:EAEA0-2985-1103-3CCC-0 dariahstorage:EAEA0-3E8A-97BC-F098-0 dariahstorage:EAEA0-7AAA-2719-2D9C-0 dariahstorage:EAEA0-1126-D9C9-7833-0 dariahstorage:EAEA0-C7AF-6F18-0F7E-0 dariahstorage:EAEA0-9A4A-D66B-F1E9-0 dariahstorage:EAEA0-8AC8-413C-87C0-0 dariahstorage:EAEA0-04B7-994C-7C80-0 dariahstorage:EAEA0-D89E-B781-28C4-0 ) .\n"
          + "\n" + "dariahstorage:EAEA0-0E7D-B850-1669-0\n"
          + "        a                dariah:Collection ;\n"
          + "        dc:creator       \"fu\" ;\n"
          + "        dc:rights        \"free\" ;\n"
          + "        dc:title         \"NEUNEUNEUNEUNENUENEUNEUNEUENEUEN\" ;\n"
          + "        dcterms:hasPart  ( dariahstorage:EAEA0-C170-16B3-7B7C-0 ) .\n"
          + "\n" + "dariahstorage:EAEA0-A2B2-19C2-5063-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:35\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0133.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-3E8A-97BC-F098-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:43\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0136.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-0ED8-B305-675C-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:55\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0139.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-8BE7-83A6-01ED-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:37\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"ugauga\" ;\n"
          + "        dc:title    \"IMG_0134.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-2985-1103-3CCC-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:55:22\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0143.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-19AF-E233-D094-0\n"
          + "        a                dariah:Collection ;\n"
          + "        dc:creator       \"fu\" ;\n"
          + "        dc:rights        \"free\" ;\n"
          + "        dc:title         \"URGL ARGL UNTERUNTERKOLLEKTION\" ;\n"
          + "        dcterms:hasPart  ( dariahstorage:EAEA0-BD81-30F6-A848-0 ) .\n"
          + "\n" + "dariahstorage:EAEA0-04B7-994C-7C80-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:40\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0135.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-D89E-B781-28C4-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:53\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0138.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-69D0-BA87-9F19-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:55:22\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0143.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-BD81-30F6-A848-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:43\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"ugl\" ;\n"
          + "        dc:title    \"IMG_0136.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-CCE8-CC65-2F8F-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:27\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0132.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-6816-7A1E-6B82-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:40\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0135.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-9A4A-D66B-F1E9-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:55:19\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0142.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-9F7E-0639-46BF-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:37\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0134.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-05AA-52B2-06DB-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:37\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0134.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-C7AF-6F18-0F7E-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:40\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0135.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-1126-D9C9-7833-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:37\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0134.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-994F-A580-B42A-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:40\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0135.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-17F8-7EBC-675B-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:37\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0134.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-8AC8-413C-87C0-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:35\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0133.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-139C-E1B0-2B82-0\n"
          + "        a                dariah:Collection ;\n"
          + "        dc:creator       \"fu\" ;\n"
          + "        dc:rights        \"free\" ;\n"
          + "        dc:title         \"UNTERUNTERUNTERKOLLEKTION\" ;\n"
          + "        dcterms:hasPart  ( dariahstorage:EAEA0-8BE7-83A6-01ED-0 ) .\n"
          + "\n" + "dariahstorage:EAEA0-C170-16B3-7B7C-0\n"
          + "        a                dariah:Collection ;\n"
          + "        dc:creator       \"fu\" ;\n"
          + "        dc:rights        \"free\" ;\n"
          + "        dc:title         \"SUBSUBSUBSUSBSUSBUBSUSBSUBS\" ;\n"
          + "        dcterms:hasPart  ( dariahstorage:EAEA0-A2B2-19C2-5063-0 dariahstorage:EAEA0-69D0-BA87-9F19-0 dariahstorage:EAEA0-9F7E-0639-46BF-0 ) .\n"
          + "\n" + "dariahstorage:EAEA0-7AAA-2719-2D9C-0\n"
          + "        a                dariah:Collection ;\n"
          + "        dc:creator       \"fu\" ;\n"
          + "        dc:rights        \"free\" ;\n"
          + "        dc:title         \"UNTERUNTERKOLLEKTION\" ;\n"
          + "        dcterms:hasPart  ( dariahstorage:EAEA0-139C-E1B0-2B82-0 dariahstorage:EAEA0-6816-7A1E-6B82-0 dariahstorage:EAEA0-05AA-52B2-06DB-0 ) .";
  private static final String METADATA_WITH_DOI = "<rdf:RDF\n"
      + "    xmlns:dcterms=\"http://purl.org/dc/terms/\"\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
      + "    xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\"\n"
      + "    xmlns:hdl=\"http://hdl.handle.net/\" > \n"
      + "  <rdf:Description rdf:about=\"http://hdl.handle.net/11022/0000-0007-8094-0\">\n"
      + "    <dc:identifier rdf:resource=\"http://box.dariah.local/dhcrud/11022/0000-0007-8094-0/index\"/>\n"
      + "    <dc:format>text/tg.collection+tg.aggregation+xml</dc:format>\n"
      + "    <dc:identifier rdf:resource=\"http://hdl.handle.net/11022/0000-0007-8094-0\"/>\n"
      + "    <dc:identifier rdf:resource=\"http://dx.doi.org/10.3249/11022/0000-0007-8094-0\"/>\n"
      + "    <dc:title>Der Fugu</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection\"/>\n"
      + "  </rdf:Description>\n" + "</rdf:RDF>\n";
  private static final String ESUTILS_ADMMD = "<rdf:RDF\n"
      + "    xmlns:dcterms=\"http://purl.org/dc/terms/\"\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "    xmlns:premis=\"http://www.loc.gov/premis/rdf/v1#\">\n"
      + "  <rdf:Description rdf:about=\"http://hdl.handle.net/21.T11998/0000-0002-4653-2\">\n"
      + "    <dcterms:format>image/jpeg</dcterms:format>\n"
      + "    <dcterms:extent>779741</dcterms:extent>\n"
      + "    <dcterms:identifier rdf:resource=\"http://hdl.handle.net/21.T11998/0000-0002-4653-2\"/>\n"
      + "    <dcterms:identifier rdf:resource=\"http://dx.doi.org/10.20375/0000-0002-4653-2\"/>\n"
      + "    <dcterms:modified>2017-09-05T16:44:55.530</dcterms:modified>\n"
      + "    <dcterms:relation rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11998/0000-0002-464B-C\"/>\n"
      + "    <dcterms:created>2017-09-05T16:44:55.530</dcterms:created>\n"
      + "    <dcterms:creator>StefanFunk@dariah.eu</dcterms:creator>\n"
      + "    <premis:hasMessageDigest>4eabbb0453b0a0822e5cb9388a1fdd73</premis:hasMessageDigest>\n"
      + "    <premis:hasMessageDigestType>md5</premis:hasMessageDigestType>\n"
      + "    <premis:hasMessageDigestOriginator>dhcrud-base 7.15.4-SNAPSHOT.201709051639</premis:hasMessageDigestOriginator>\n"
      + "  </rdf:Description>\n" + "</rdf:RDF>";
  private static final String ESUTILS_ADMMD_EXPECTED = "<rdf:RDF\n"
      + "    xmlns:dcterms=\"http://purl.org/dc/terms/\"\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "    xmlns:premis=\"http://www.loc.gov/premis/rdf/v1#\">\n"
      + "  <rdf:Description rdf:about=\"http://hdl.handle.net/21.T11998/0000-0002-4653-2\">\n"
      + "    <dcterms:format>image/jpeg</dcterms:format>\n"
      + "    <dcterms:extent>779741</dcterms:extent>\n"
      + "    <dcterms:identifier>hdl:21.T11998/0000-0002-4653-2</dcterms:identifier>\n"
      + "    <dcterms:identifier>doi:10.20375/0000-0002-4653-2</dcterms:identifier>\n"
      + "    <dcterms:modified>2017-09-05T16:44:55.530</dcterms:modified>\n"
      + "    <dcterms:relation>hdl:21.T11998/0000-0002-464B-C</dcterms:relation>\n"
      + "    <dcterms:created>2017-09-05T16:44:55.530</dcterms:created>\n"
      + "    <dcterms:creator>StefanFunk@dariah.eu</dcterms:creator>\n"
      + "    <premis:hasMessageDigest>4eabbb0453b0a0822e5cb9388a1fdd73</premis:hasMessageDigest>\n"
      + "    <premis:hasMessageDigestType>md5</premis:hasMessageDigestType>\n"
      + "    <premis:hasMessageDigestOriginator>dhcrud-base 7.15.4-SNAPSHOT.201709051639</premis:hasMessageDigestOriginator>\n"
      + "  </rdf:Description>\n" + "</rdf:RDF>";
  private static final String ESUTILS_DMD = "<rdf:RDF\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
      + "    xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\">\n"
      + "  <dariah:DataObject rdf:about=\"http://hdl.handle.net/21.T11998/0000-0002-4653-2\">\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:title>IMG_0133.jpg</dc:title>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <dc:description>dfkgjhsdfkghsf</dc:description>\n"
      + "    <dc:relation rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11998/0000-0002-464B-C\"/>\n"
      + "    <dc:identifier rdf:resource=\"http://hdl.handle.net/21.T11998/0000-0002-4653-2\"/>\n"
      + "    <dc:date>2015-06-20T16:54:35</dc:date>\n"
      + "    <dc:identifier rdf:resource=\"http://dx.doi.org/10.20375/0000-0002-4653-2\"/>\n"
      + "    <dc:identifier rdf:resource=\"http://box.dariah.local/1.0/dhcrud/21.T11998/0000-0002-4653-2/index\"/>\n"
      + "    <dc:rights>free</dc:rights>\n" + "  </dariah:DataObject>\n"
      + "</rdf:RDF>";
  private static final String ESUTILS_DMD_EXPECTED = "<rdf:RDF\n"
      + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n"
      + "  <rdf:Description rdf:about=\"http://hdl.handle.net/21.T11998/0000-0002-4653-2\">\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:title>IMG_0133.jpg</dc:title>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <dc:description>dfkgjhsdfkghsf</dc:description>\n"
      + "    <dc:relation>hdl:21.T11998/0000-0002-464B-C</dc:relation>\n"
      + "    <dc:identifier>hdl:21.T11998/0000-0002-4653-2</dc:identifier>\n"
      + "    <dc:date>2015-06-20T16:54:35</dc:date>\n"
      + "    <dc:identifier>doi:10.20375/0000-0002-4653-2</dc:identifier>\n"
      + "    <dc:identifier>http://box.dariah.local/1.0/dhcrud/21.T11998/0000-0002-4653-2/index</dc:identifier>\n"
      + "    <dc:rights>free</dc:rights>\n" + "  </rdf:Description>\n"
      + "</rdf:RDF>";
  private static final String METADATA_WITH_RDF_LIST =
      "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "@prefix hdl:   <http://hdl.handle.net/> .\n"
          + "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n"
          + "@prefix doi:   <http://dx.doi.org/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11991/0000-0004-A9A3-5>\n"
          + "        a                   dariah:Collection ;\n"
          + "        dc:creator          \"fu\" ;\n"
          + "        dc:format           \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "        dc:identifier       <http://hdl.handle.net/21.T11991/0000-0004-A9A3-5> , <http://dx.doi.org/10.20375/0000-0004-A9A3-5> ;\n"
          + "        dc:rights           \"free\" ;\n"
          + "        dc:title            \"Wochendkollektion\" ;\n"
          + "        dcterms:format      \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "        dcterms:hasPart     ( dariahstorage:EAEA0-761B-37FF-3A24-0 dariahstorage:EAEA0-EA60-B196-B1BD-0 dariahstorage:EAEA0-3BEA-8940-E38B-0 dariahstorage:EAEA0-AB4B-A593-161F-0 dariahstorage:EAEA0-18EA-DF9D-8972-0 dariahstorage:EAEA0-4ECB-95DC-96C6-0 dariahstorage:EAEA0-C4E2-8011-3C80-0 dariahstorage:EAEA0-21A6-557A-8FFE-0 dariahstorage:EAEA0-723A-D4E7-03A5-0 dariahstorage:EAEA0-FDEC-99FB-A542-0 dariahstorage:EAEA0-F4C7-C53C-2414-0 dariahstorage:EAEA0-2705-C4F5-3EBA-0 dariahstorage:EAEA0-75A8-F473-8B20-0 dariahstorage:EAEA0-0BC4-6C3A-43E1-0 dariahstorage:EAEA0-0177-72E6-011E-0 ) ;\n"
          + "        dcterms:identifier  <http://hdl.handle.net/21.T11991/0000-0004-A9A3-5> , <http://dx.doi.org/10.20375/0000-0004-A9A3-5> ;\n"
          + "        dcterms:source      \"https://cdstar.de.dariah.eu/test/dariah/EAEA0-63DF-C71E-840E-0\" .";
  private static final String EXPECTED_METADATA_WITH_RDF_LIST =
      "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "@prefix hdl:   <http://hdl.handle.net/> .\n"
          + "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n"
          + "@prefix doi:   <http://dx.doi.org/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11991/0000-0004-A9A3-5>\n"
          + "        a                   dariah:Collection ;\n"
          + "        dc:creator          \"fu\" ;\n"
          + "        dc:format           \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "        dc:identifier       <http://hdl.handle.net/21.T11991/0000-0004-A9A3-5> , <http://dx.doi.org/10.20375/0000-0004-A9A3-5> ;\n"
          + "        dc:rights           \"free\" ;\n"
          + "        dc:title            \"Wochendkollektion\" ;\n"
          + "        dcterms:format      \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "        dcterms:identifier  <http://hdl.handle.net/21.T11991/0000-0004-A9A3-5> , <http://dx.doi.org/10.20375/0000-0004-A9A3-5> ;\n"
          + "        dcterms:source      \"https://cdstar.de.dariah.eu/test/dariah/EAEA0-63DF-C71E-840E-0\" .";
  private static final String COLLECTION_FAIL_WITH_NEWLINE =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n" +
          "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n" +
          "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" +
          "\n" +
          "<http://hdl.handle.net/21.T11991/0000-001B-4120-5>\n" +
          "        a              dariah:DataObject ;\n" +
          "        dc:creator     \"fu\" ;\n" +
          "        dc:format      \"image/gif\" ;\n" +
          "        dc:identifier  <http://dx.doi.org/10.20375/0000-001B-4120-5> , <http://hdl.handle.net/21.T11991/0000-001B-4120-5> ;\n"
          +
          "        dc:relation    <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11991/0000-001B-411F-8> ;\n"
          +
          "        dc:rights      \"testing only!\" ;\n" +
          "        dc:title       \"DRAGON \n DRAGON\" .\n";
  private static final String COLLECTION_FAIL =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "dariahstorage:EAEA0-17F8-7EBC-675B-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:37\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0134.jpg\" .\n" + "\n"
          + "        a                dariah:Collection ;\n"
          + "        dc:creator       \"fu\" ;\n"
          + "        dc:rights        \"free\" ;\n"
          + "        dc:title         \"KOLLEKTION\" ;\n"
          + "        dcterms:hasPart  ( dariahstorage:EAEA0-17F8-7EBC-675B-0 dariahstorage:EAEA0-DB19-ED64-6480-0 dariahstorage:EAEA0-994F-A580-B42A-0 ) .\n"
          + "\n" + "dariahstorage:EAEA0-994F-A580-B42A-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:40\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0135.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-DB19-ED64-6480-0\n"
          + "        a       dariah:Collection .\n";
  private static final String OK = "...OK";
  private static final String FAILED = "...FAILED";

  // **
  // SET UPS
  // **

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  // **
  // TESTS
  // **

  /**
   * @throws ParseException
   */
  @Test
  public void testFindFirstObject() throws ParseException {

    String resource = "http://fugu.de/uri/of/resource";
    String prefix = "dc";
    String element = "format";

    String lookingFor = "image/png";

    System.out.println("Looking for " + lookingFor + " in " + resource + ", element " + prefix + ":"
        + element + "...");

    String first =
        RDFUtils.findFirstObject(RDFUtils.readModel(RDF_DATA), resource, prefix, element);

    if (first.equals(lookingFor)) {
      System.out.println("\tfound " + first);
      System.out.println(OK);
    } else {
      System.out.println("\tnot found: " + first);
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testFindFirstSubject() throws ParseException {

    String resource = "http://de.dariah.eu/rdf/dataobjects/terms/DataObject";
    String prefix = "rdf";
    String element = "type";

    String lookingFor = "http://fugu.de/uri/of/resource";

    System.out.println("Looking for " + lookingFor + " in " + resource + ", element " + prefix + ":"
        + element + "...");

    String first =
        RDFUtils.findFirstSubject(RDFUtils.readModel(RDF_DATA), resource, prefix, element);

    if (first.equals(lookingFor)) {
      System.out.println("\tfound " + first);
      System.out.println(OK);
    } else {
      System.out.println("\tnot found: " + first);
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testFindFirstSubjectAboutAttribute() throws ParseException {

    String resource = "http://de.dariah.eu/rdf/dataobjects/terms/Collection";
    String prefix = "";
    String element = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";

    String lookingFor = "http://geobrowser.de.dariah.eu/storage/335701";

    System.out.println("Looking for " + lookingFor + " in " + resource + ", element " + prefix
        + (prefix.equals("") ? "" : ":") + element + "...");

    Model m =
        RDFUtils.readModel(COLLECTION_DATA, RDFConstants.TURTLE);
    String first = RDFUtils.findFirstSubject(m, resource, prefix, element);

    if (first.equals(lookingFor)) {
      System.out.println("\tfound " + first);
      System.out.println(OK);
    } else {
      System.out.println("\tnot found: " + first);
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testFindFirstValue() throws ParseException {

    String prefix = "dc";
    String element = "format";

    String lookingFor = "image/png";

    System.out.println("Looking for " + lookingFor + ", element " + prefix + ":" + element + "...");

    String first = RDFUtils.findFirstObject(RDFUtils.readModel(RDF_DATA), prefix, element);

    if (first.equals(lookingFor)) {
      System.out.println("\tfound " + first);
      System.out.println(OK);
    } else {
      System.out.println("\tnot found: " + first);
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testFindAllObjectsTitle() throws ParseException {

    String prefix = "dc";
    String element = "title";

    List<String> lookingFor = new ArrayList<String>();
    lookingFor.add("southparkfugu");
    lookingFor.add("eine kleine dingsda-sammlung");

    System.out.println("Looking for " + lookingFor + ", element " + prefix + ":" + element + "...");

    List<String> all =
        RDFUtils.findAllObjects(RDFUtils.readModel(RDF_DATA_MULTIPLE), prefix, element);

    if (all.containsAll(lookingFor)) {
      System.out.println("\tfound " + lookingFor);
      System.out.println(OK);
    } else {
      System.out.println("\tnot found: " + lookingFor);
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testFindAllObjectsCreator() throws ParseException {

    String prefix = "dc";
    String element = "creator";

    List<String> lookingFor = new ArrayList<String>();
    lookingFor.add("fugu");
    lookingFor.add("dingsda");
    lookingFor.add("stefan e. funk");

    System.out.println("Looking for " + lookingFor + ", element " + prefix + ":" + element + "...");

    List<String> all =
        RDFUtils.findAllObjects(RDFUtils.readModel(RDF_DATA_MULTIPLE), prefix, element);

    if (all.containsAll(lookingFor)) {
      System.out.println("\tfound " + lookingFor);
      System.out.println(OK);
    } else {
      System.out.println("\tnot found: " + lookingFor);
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testFindAllSubjectAboutAttribute() throws ParseException {

    String prefix = "";
    String element = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";

    System.out.println("Looking for rdf:about attributes...");

    Model m = RDFUtils.readModel(RDF_DATA_SUBCOLLECTIONS, RDFConstants.TURTLE);
    List<String> abouts = RDFUtils.findAllSubjects(m, prefix, element);

    System.out.println("\tsize is " + abouts.size() + ": " + abouts);

    if (abouts.size() == 26) {
      System.out.println("\t...size correct");
      System.out.println(OK);
    } else {
      System.out.println("\t...size must be 26, not " + abouts.size() + "!");
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws XMLStreamException
   */
  @Test
  public void testFindFirstObjectStartsWith()
      throws XMLStreamException, IOException, ParseException {

    String resource = "http://fugu.de/uri/of/resource";
    String prefix = RDFConstants.DC_PREFIX;
    String element = RDFConstants.ELEM_DC_RELATION;

    String lookingFor = RDFConstants.DARIAH_NAMESPACE;

    System.out.println("Looking for starting " + lookingFor + " in " + resource + ", element "
        + prefix + ":" + element + "...");

    String relation = RDFUtils.getFirstProperty(RDFUtils.readModel(RDF_DATA), resource, prefix,
        element, lookingFor);

    if (relation.toString().startsWith(lookingFor)) {
      System.out.println("\tfound " + relation);
      System.out.println(OK);
    } else {
      System.out.println("\tnot found: " + relation);
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testAddToModel() throws ParseException {

    String resource = "http://fugu.de/uri/of/resource";
    String prefix = "dc";
    String element = "source";

    String newValue = "http://fugu.de/uri/of/new/resource";

    System.out.println("Looking for not yet existing element " + prefix + ":" + element + " in "
        + resource + "...");

    // Model to read from.
    Model model = RDFUtils.readModel(RDF_DATA);

    // Check for new source element.
    String newElementValue = RDFUtils.findFirstObject(model, resource, prefix, element);
    if (!newElementValue.equals("")) {
      System.out.println("\tfound: " + newElementValue);
      System.out.println(FAILED);
      assertTrue(false);
    } else {
      System.out.println("\tnot existing...");
    }

    System.out.println("\tcreating element...");

    // Add new element dc:source.
    RDFUtils.addToModel(model, resource, prefix, element, newValue);

    // Now find new element.
    newElementValue = RDFUtils.findFirstObject(model, resource, prefix, element);
    if (newElementValue.equals("")) {
      System.out.println("/tfound: " + newElementValue);
      System.out.println(FAILED);
      assertTrue(false);
    } else {
      System.out.println("\tfound: " + newElementValue);
      System.out.println(OK);
    }
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testRemoveFromModel() throws ParseException {

    String resource = "http://fugu.de/uri/of/resource";
    String prefix = RDFConstants.DC_PREFIX;
    String element = RDFConstants.ELEM_DC_FORMAT;

    System.out.println("Deleting all triples with predicate " + prefix + ":" + element + " in "
        + resource + "...");

    // Model to read from.
    Model model = RDFUtils.readModel(RDF_DATA);

    // model.write(System.out);

    // Check for source element.
    String newElementValue = RDFUtils.findFirstObject(model, resource, prefix, element);
    if (!newElementValue.equals("")) {
      System.out.println("\tfound: " + newElementValue + "...");
    } else {
      System.out.println("\tnot existing...");
      System.out.println(FAILED);
    }

    System.out.println("\tremoving element...");

    // Remove element.
    RDFUtils.removeFromModel(model, resource, prefix, element);

    // model.write(System.out);

    // Now find removed element.
    newElementValue = RDFUtils.findFirstObject(model, resource, prefix, element);
    if (newElementValue.equals("")) {
      System.out.println("\tfound nothing");
      System.out.println(OK);
    } else {
      System.out.println("\tfound: " + newElementValue);
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testRemoveFromModelMultipleIdentifiers() throws ParseException {

    String resource = "http://fugu.de/uri/of/resource";
    String prefix = "dc";
    String element = "identifier";

    System.out.println("Deleting all triples with predicate " + prefix + ":" + element + " in "
        + resource + "...");

    // Model to read from.
    Model model = RDFUtils.readModel(RDF_DATA_MULTIPLE_IDENTIFIERS);

    // model.write(System.out);

    // Check for source element.
    List<String> newElementValues = RDFUtils.findAllObjects(model, resource, prefix, element);
    if (newElementValues.size() == 3) {
      System.out.print("\tfound: " + newElementValues + "...");
    } else {
      System.out.print("\tnot existing...");
    }

    System.out.print("\tremoving element...");

    // Remove element.
    RDFUtils.removeFromModel(model, resource, prefix, element);

    // model.write(System.out);

    // Now find removed element.
    newElementValues = RDFUtils.findAllObjects(model, resource, prefix, element);
    if (newElementValues.size() == 0) {
      System.out.print("\tfound nothing");
      System.out.println(OK);
    } else {
      System.out.println("\tfound: " + newElementValues);
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws XMLStreamException
   * @throws ParseException
   */
  @Test
  public void testRemoveHasPartListFromModel()
      throws XMLStreamException, IOException, ParseException {

    String resource = "http://hdl.handle.net/21.T11991/0000-0004-A9A3-5";
    String prefix = "dcterms";
    String element = "hasPart";

    System.out.println("Deleting all triples with predicate " + prefix + ":"
        + element + "in " + resource + "...");

    // Model to read from.
    Model model = RDFUtils.readModel(METADATA_WITH_RDF_LIST,
        RDFConstants.TURTLE);

    // Check for source element.
    List<String> newElementValues = RDFUtils.findAllObjects(model, resource, prefix, element);
    if (newElementValues.size() == 1) {
      System.out.println("\tfound: " + newElementValues + "...");
    } else {
      System.out.println("\tmore than one element found!");
      assertTrue(false);
    }

    System.out.println("\tremoving element...");

    // Remove element.
    RDFUtils.removeHasPartListFromModel(model, resource);

    // Now compare models.
    Model expected = RDFUtils.readModel(EXPECTED_METADATA_WITH_RDF_LIST, RDFConstants.TURTLE);
    if (expected.isIsomorphicWith(model)) {
      System.out.println("\tfound nothing");
      System.out.println(OK);
    } else {
      System.out.println("\tEXPECTED");
      System.out.println(RDFUtils.getStringFromModel(expected, RDFConstants.NTRIPLES));
      System.out.println("\tCREATED");
      System.out.println(RDFUtils.getStringFromModel(model, RDFConstants.NTRIPLES));
      System.out.println();
      System.out.println("\tDIFFERENCES: " + expected.difference(model));
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testCopyModel() throws ParseException {

    String oldResource = "http://fugu.de/uri/of/resource";
    String newResource = "http://fugu.de/uri/of/new/resource";

    System.out.println("Test copying existing model to new model...");

    // Model to read from.
    Model model = RDFUtils.readModel(RDF_DATA);

    // Copy model.
    Model secondModel = RDFUtils.copyModel(model, newResource);

    System.out.println("\tmodel copied...");

    // Copy model back and again.
    Model thirdModel = RDFUtils.copyModel(secondModel, oldResource);

    System.out.println("\tcopying new model back to third model...");

    boolean isoWithSecond = model.isIsomorphicWith(secondModel);
    boolean isoWithThird = model.isIsomorphicWith(thirdModel);

    if (!isoWithSecond && isoWithThird) {
      System.out.println("\tcopy complete");
      System.out.println(OK);
    } else {
      System.out.println("\terror copying models");
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testCopyAndAdd() throws ParseException {

    String newResource = "fu:new:resource";
    String prefix = "dc";
    String element = "source";
    String value = "my:new:source";

    System.out.println("Test copying existing model to new model...");

    // Model to read from.
    Model model = RDFUtils.readModel(RDF_DATA);

    // Copy model.
    model = RDFUtils.copyModel(model, newResource);

    System.out.println("\tmodel updated with new resource " + newResource + "...");

    // Add to model.
    RDFUtils.addToModel(model, newResource, prefix, element, value);

    System.out.println("\tadded new element " + prefix + ":" + element + " with value " + value
        + " to new model...");

    if (RDFUtils.findFirstObject(model, newResource, prefix, element).equals(value)) {
      System.out.println("\tcopy complete");
      System.out.println(OK);
    } else {
      System.out.println("\terror copying models");
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testNewModelWithResources() throws ParseException {

    String resource = "http://fugu.de/uri/of/resource";
    String prefix = "dcterms";
    String namespace = "http://purl.org/dc/terms/";

    System.out.println("Test creating new resource model...");

    Model model = RDFUtils.newModel(resource, prefix, namespace);
    RDFUtils.addResourceToModel(model, resource, prefix, RDFConstants.ELEM_DC_CREATOR,
        model.createResource("http://fugu.de/uri/of/fufu/resource"));

    // Read test model.
    Model testModel = RDFUtils.readModel(NEW_RESOURCE_MODEL, RDFConstants.TURTLE);

    // Compare both.
    if (model.isIsomorphicWith(testModel)) {
      System.out.println(OK);
    } else {
      System.out.println(FAILED);
      System.out.println("CREATED:\n" + RDFUtils.getTTLFromModel(model));
      System.out.println("EXPECTED:\n" + RDFUtils.getTTLFromModel(testModel));
      assertTrue(false);
    }
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testNewModelWithLiterals() throws ParseException {

    String resource = "fu:uri:of:resource";
    String prefix = "dcterms";
    String namespace = "http://purl.org/dc/terms/";

    System.out.println("Test creating new literals model...");

    Model model = RDFUtils.newModel(resource, prefix, namespace);
    RDFUtils.addLiteralToModel(model, resource, prefix, RDFConstants.ELEM_DC_CREATOR, "fufu");

    System.out.println(RDFUtils.getTTLFromModel(model));

    // Read test model.
    Model testModel = RDFUtils.readModel(NEW_LITERAL_MODEL, RDFConstants.TURTLE);

    System.out.println(RDFUtils.getTTLFromModel(testModel));

    // Compare both.
    if (model.isIsomorphicWith(testModel)) {
      System.out.println("\tcopy complete");
      System.out.println(OK);
    } else {
      System.out.println("\terror creating models");
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * @throws XMLStreamException
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testGetListFromProperty()
      throws XMLStreamException, IOException, ParseException {

    String resource = "http://hdl.handle.net/11022/0000-0000-99E5-1";
    String predicate = RDFConstants.ELEM_DCTERMS_HASPART;
    String prefix = RDFConstants.DCTERMS_PREFIX;

    // Model to read from.
    Model model = RDFUtils.readModel(DATA, RDFConstants.TURTLE);

    System.out.println("Test extracting all aggregated URLs from the data model (resource is "
        + resource + ")... ");

    List<String> list = RDFUtils.getProperties(model, resource, prefix, predicate);

    if (list.isEmpty()) {
      System.out.println("\tlist empty");
      System.out.println(FAILED);
      assertTrue(false);
    }

    System.out.print("\t" + list);

    System.out.println();
    System.out.println(OK);
  }

  /**
   * @throws XMLStreamException
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testGetFirstDoi() throws XMLStreamException, IOException, ParseException {

    String resource = "http://hdl.handle.net/11022/0000-0007-8094-0";
    String expectedDoi = "http://dx.doi.org/10.3249/11022/0000-0007-8094-0";

    System.out.println("Test finding DOI " + expectedDoi + " in model... ");

    // Model to read from.
    Model model = RDFUtils.readModel(METADATA_WITH_DOI);

    // Find DOI in DOI list.
    List<String> dois = RDFUtils.getProperties(model, resource, RDFConstants.DC_PREFIX,
        RDFConstants.ELEM_DC_IDENTIFIER, RDFConstants.DOI_NAMESPACE);

    if (!dois.contains(expectedDoi)) {
      System.out.println("\tDOI " + expectedDoi + " not found!");
      System.out.println("\tLIST: " + dois);
      System.out.println(FAILED);
      assertTrue(false);
    }

    // Find first DOI in model.
    String doi = RDFUtils.getFirstProperty(model, resource, RDFConstants.DC_PREFIX,
        RDFConstants.ELEM_DC_IDENTIFIER, RDFConstants.DOI_NAMESPACE);

    System.out.println("\t" + doi);

    if (!doi.equals(expectedDoi)) {
      System.out.println("\tDOI " + expectedDoi + " not found!");
      System.out.println(FAILED);
      assertTrue(false);
    }

    System.out.println(OK);
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testGetModelContainingOnlyLiteralsDMD() throws ParseException {

    System.out.println("Test literalising DMD metadata... ");

    String uri = "http://hdl.handle.net/21.T11998/0000-0002-4653-2";
    List<String> prefixes = new ArrayList<String>();
    prefixes.add(RDFConstants.DC_PREFIX);

    Model dmd = RDFUtils.readModel(ESUTILS_DMD);
    Model dmdExpected = RDFUtils.readModel(ESUTILS_DMD_EXPECTED);
    Model dmdLiteral = RDFUtils.getModelContainingOnlyLiterals(dmd, uri, prefixes, true);

    if (!dmdExpected.isIsomorphicWith(dmdLiteral)) {
      System.out.println(FAILED);
      System.out.println("CREATED:\n" + RDFUtils.getStringFromModel(dmdLiteral));
      System.out.println("EXPECTED:\n" + RDFUtils.getStringFromModel(dmdExpected));
      System.out.println(
          "DIFFERENCE:\n" + RDFUtils.getStringFromModel(dmdExpected.difference(dmdLiteral)));
      assertTrue(false);
    }

    System.out.println(OK);
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testGetModelContainingOnlyLiteralsADMMD() throws ParseException {

    System.out.println("Test literalising ADM_MD metadata... ");

    String uri = "http://hdl.handle.net/21.T11998/0000-0002-4653-2";
    List<String> prefixes = new ArrayList<String>();
    prefixes.add(RDFConstants.DCTERMS_PREFIX);
    prefixes.add(RDFConstants.PREMIS_PREFIX);

    Model admmd = RDFUtils.readModel(ESUTILS_ADMMD);
    Model admmdExpected = RDFUtils.readModel(ESUTILS_ADMMD_EXPECTED);

    Model admmdLiteral = RDFUtils.getModelContainingOnlyLiterals(admmd, uri, prefixes, true);

    if (!admmdExpected.isIsomorphicWith(admmdLiteral)) {
      System.out.println(FAILED);
      System.out.println("CREATED:\n" + RDFUtils.getStringFromModel(admmdLiteral));
      System.out.println("EXPECTED:\n" + RDFUtils.getStringFromModel(admmdExpected));
      System.out.println(
          "DIFFERENCE:\n" + RDFUtils.getStringFromModel(admmdExpected.difference(admmdLiteral)));
      assertTrue(false);
    }

    System.out.println(OK);
  }

  /**
   * @throws DatatypeConfigurationException
   * @throws IOException
   * @throws XMLStreamException
   * @throws ParseException
   */
  @Test
  public void testPrepareDMDforJSONCreation()
      throws XMLStreamException, IOException, DatatypeConfigurationException, ParseException {

    System.out.println("Test preparing DMD2JSON... ");

    String uri = "http://hdl.handle.net/21.T11998/0000-0002-4653-2";
    List<String> prefixes = new ArrayList<String>();
    prefixes.add(RDFConstants.DC_PREFIX);

    Model dmd = RDFUtils.readModel(ESUTILS_DMD);
    Model dmdExpected = RDFUtils.readModel(ESUTILS_DMD_EXPECTED);
    Model dmdLiteral = RDFUtils.prepareDMDforJSONCreation(URI.create(uri), dmd);

    if (!dmdExpected.isIsomorphicWith(dmdLiteral)) {
      System.out.println(FAILED);
      System.out.println("CREATED:\n" + RDFUtils.getStringFromModel(dmdLiteral));
      System.out.println("EXPECTED:\n" + RDFUtils.getStringFromModel(dmdExpected));
      System.out.println(
          "DIFFERENCE:\n" + RDFUtils.getStringFromModel(dmdExpected.difference(dmdLiteral)));
      assertTrue(false);
    }

    System.out.println(OK);
  }

  /**
   * @throws DatatypeConfigurationException
   * @throws IOException
   * @throws XMLStreamException
   * @throws ParseException
   */
  @Test
  public void testPrepareADMMDforJSONCreation()
      throws XMLStreamException, IOException, DatatypeConfigurationException, ParseException {

    System.out.println("Test preparing ADM_MD2JSON metadata... ");

    String uri = "http://hdl.handle.net/21.T11998/0000-0002-4653-2";
    List<String> prefixes = new ArrayList<String>();
    prefixes.add(RDFConstants.DCTERMS_PREFIX);
    prefixes.add(RDFConstants.PREMIS_PREFIX);

    Model admmd = RDFUtils.readModel(ESUTILS_ADMMD);
    Model admmdExpected = RDFUtils.readModel(ESUTILS_ADMMD_EXPECTED);
    Model admmdLiteral = RDFUtils.prepareADMMDforJSONCreation(URI.create(uri), admmd);

    if (!admmdExpected.isIsomorphicWith(admmdLiteral)) {
      System.out.println(FAILED);
      System.out.println("CREATED:\n" + RDFUtils.getStringFromModel(admmdLiteral));
      System.out.println("EXPECTED:\n" + RDFUtils.getStringFromModel(admmdExpected));
      System.out.println(
          "DIFFERENCE:\n" + RDFUtils.getStringFromModel(admmdExpected.difference(admmdLiteral)));
      assertTrue(false);
    }

    System.out.println(OK);
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testgetRDFListAnon() throws ParseException {

    System.out.println("Getting all hasParts out of the model, testing getRDFList...");

    List<String> expected = new ArrayList<String>();
    expected.add("https://de.dariah.eu/storage/EAEA0-994F-A580-B42A-0");
    expected.add("https://de.dariah.eu/storage/EAEA0-DB19-ED64-6480-0");
    expected.add("https://de.dariah.eu/storage/EAEA0-0E7D-B850-1669-0");
    expected.add("https://de.dariah.eu/storage/EAEA0-17F8-7EBC-675B-0");

    String uri = "https://de.dariah.eu/storage/EAEA0-9386-D2E1-226C-0";
    Model m = RDFUtils.readModel(RDF_DATA_SUBCOLLECTIONS, RDFConstants.TURTLE);

    // Get objects contained in collection (hasPart).
    Resource res = m.getResource(uri);
    Property prop =
        m.createProperty(m.getNsPrefixURI(RDFConstants.DCTERMS_PREFIX),
            RDFConstants.ELEM_DCTERMS_HASPART);

    // Loop all the predicates.
    StmtIterator iter = res.listProperties(prop);
    List<String> list = new ArrayList<String>();
    while (iter.hasNext()) {
      Statement s = iter.next();
      list = RDFUtils.getRDFList(s.getObject());
    }

    // Check order of both lists.
    boolean orderCorrect = true;
    int index = 0;
    for (String str : list) {
      if (!expected.get(index).equals(str)) {
        orderCorrect = false;
      }
      index++;
    }

    if (orderCorrect && list.size() == 4) {
      System.out.println("\t...order correct, size correct");
      System.out.println(OK);
    } else {
      System.out.println("\t...order incorrect!");
      System.out.println("LIST:     " + list);
      System.out.println("EXPECTED: " + expected);
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testgetRDFList() throws ParseException {

    System.out.println("Getting all hasParts out of the model, testing getRDFList...");

    List<String> expected = new ArrayList<String>();
    expected.add("https://de.dariah.eu/storage/EAEA0-BD81-30F6-A848-0");

    String uri = "https://de.dariah.eu/storage/EAEA0-19AF-E233-D094-0";
    Model m = RDFUtils.readModel(RDF_DATA_SUBCOLLECTIONS, RDFConstants.TURTLE);

    // Get objects contained in collection (hasPart).
    Resource res = m.getResource(uri);
    Property prop =
        m.createProperty(m.getNsPrefixURI(RDFConstants.DCTERMS_PREFIX),
            RDFConstants.ELEM_DCTERMS_HASPART);

    // Loop all the predicates.
    StmtIterator iter = res.listProperties(prop);
    List<String> list = new ArrayList<String>();
    while (iter.hasNext()) {
      Statement s = iter.next();
      list = RDFUtils.getRDFList(s.getObject());
    }

    // Check order of both lists.
    boolean orderCorrect = true;
    int index = 0;
    for (String str : list) {
      if (!expected.get(index).equals(str)) {
        orderCorrect = false;
      }
      index++;
    }

    if (orderCorrect && list.size() == 1) {
      System.out.println("\t...order correct, size correct");
      System.out.println(OK);
    } else {
      System.out.println("\t...order incorrect!");
      System.out.println("LIST:     " + list);
      System.out.println("EXPECTED: " + expected);
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * @throws ParseException
   */
  @Test(expected = ParseException.class)
  public void testReadCollectionModelsFAILWITHNEWLINE() throws ParseException {

    System.out.println("Testing ReadCollectionModelsFAILWITHNEWLINE... ");

    // Read model.
    try {
      RDFUtils.readModel(COLLECTION_FAIL_WITH_NEWLINE, RDFConstants.TURTLE);
    } catch (Exception e) {
      System.out.println(
          "\tcought expected exception: [" + e.getClass().getName() + "] " + e.getMessage());
      System.out.println(OK);
      throw e;
    }
  }

  /**
   * @throws ParseException
   */
  @Test(expected = ParseException.class)
  public void testReadCollectionModelsFAIL() throws ParseException {

    System.out.println("Testing ReadCollectionModelsFAIL... ");

    // Read collection.
    try {
      RDFUtils.readModel(COLLECTION_FAIL, RDFConstants.TURTLE);
    } catch (Exception e) {
      System.out.println(
          "\tcought expected exception: [" + e.getClass().getName() + "] " + e.getMessage());
      System.out.println(OK);
      throw e;
    }
  }

}
