# DARIAH-DE and TextGrid Common Packages

These Common Packages are used to gather utility and helper classes such as ElasticSearch, RDF, storage, and LTP utilities used by DARIAH-DE and TextGrid Repository Services and maybe other tools.

This packages are deployed as JAR packages to the [GWDG Nexus Servive](https://nexus.gwdg.de).

# Developing and releasing DARIAH-DE-COMMON
For releasing a new version of DARIAH-DE-COMMON libraries, please have a look at the [DARIAH-DE Release Management Page](https://projects.academiccloud.de/projects/dariah-de-intern/wiki/dariah-de-release-management) or see the [Gitlab CI file](.gitlab-ci.yml).

